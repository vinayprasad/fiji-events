//
//  ColorsRGB.h
//  testTab
//
//  Created by Ashivan Ram on 18/07/2018.
//  Copyright © 2018 Ashivan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ColorsRGB : NSObject
@property(nonatomic,strong) NSString *id;
@property(nonatomic,strong) NSString *eventname;
@property(nonatomic,strong) NSString *R;
@property(nonatomic,strong) NSString *G;
@property(nonatomic,strong) NSString *B;
@end
