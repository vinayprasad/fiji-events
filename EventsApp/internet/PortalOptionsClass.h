//
//  PortalOptionsClass.h
//  testTab
//
//  Created by Ashivan Ram on 16/07/2018.
//  Copyright © 2018 Ashivan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PortalOptionsClass : NSObject
@property (nonatomic,strong) NSString *tabname;
@property (nonatomic,strong) NSString *active;
@end
