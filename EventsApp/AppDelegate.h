//
//  AppDelegate.h
//  FIAEventsApp
//
//  Created by temp on 31/01/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>
@import GoogleSignIn;

@interface AppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate,GIDSignInDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *strDeviceToken;


@end

