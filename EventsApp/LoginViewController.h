//
//  LoginViewController.h
//  FIAEventsApp
//
//  Created by temp on 02/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "DBManager.h"


@interface LoginViewController : UIViewController<UIGestureRecognizerDelegate,SWRevealViewControllerDelegate, UITextFieldDelegate,UIAlertViewDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imgLoogo;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;
@property (strong, nonatomic) IBOutlet UIButton *btnGuest;
@property (nonatomic, strong) DBManager *dbManager;

@property (strong, nonatomic) IBOutlet UIView *imgBackground;



@end
