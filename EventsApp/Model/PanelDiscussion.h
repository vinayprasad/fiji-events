//
//  PanelDiscussion.h
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PanelDiscussion : NSObject
@property (strong,nonatomic) NSNumber *panelID;
@property (strong,nonatomic) NSString *panelquestion;
@property (strong,nonatomic) NSNumber *sessionId;
@property (strong,nonatomic) NSNumber *userID;
@property (strong,nonatomic) NSNumber *active;
@property (strong,nonatomic) NSDate *lastmodified;

-(id)initWithID :(NSNumber*)pID andQuestion:(NSString*)pQuestion andSession:(NSNumber*)pSessionID andUser:(NSNumber*)pUserID andStatus:(NSNumber*)pActive andLastModified:(NSString*)Lmodified;

-(NSDate*)ConvertStringToDate :(NSString*)StringDate;
@end
