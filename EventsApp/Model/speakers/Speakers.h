//
//  Speakers.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 15/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Speakers : NSObject
@property (nonatomic,weak) NSNumber *speakerID;
@property (nonatomic,weak) NSString *speakerFirstName;
@property (nonatomic,weak) NSString *speakerLastName;
@property (nonatomic,weak) NSString *speakerPicURL;
@property (nonatomic,weak) NSString *speakerCompany;
@property (nonatomic,weak) NSString *speakerEventID;
@property (nonatomic,weak) NSString *speakerPosition;
@property (nonatomic,weak) NSString *speakerPDFUrl;
@property (nonatomic,weak) NSString *active;
@property (nonatomic,weak) NSString *lastmodified;

-(id)initWithID:(NSNumber*)sID andName: (NSString*)sFirstName andLastName:(NSString*)sLastName andPicURL:(NSString*)sPicURL andCompany:(NSString*)sCompany andEventID:(NSString*)sEventID andPos:(NSString*)sPos andSpeakerPDFURL:(NSString*)sPDFURL;

-(NSDate*)ConvertStringToDate :(NSString*)StringDate;
@end
//"eventname": "FIA Congress",
//"eventid": 11,
//"speaker": {
//    "id": 13,
//    "username": "finaunagera",
//    "password": "",
//    "first_name": "Finau",
//    "last_name": "Seru Nagera",
//    "email": "finau@yahoo.com",
//    "phone": "9997892",
//    "picture": "FIA_PORTAL/web/uploads/user/finaunagera.jpg",
//    "gender": "Female",
//    "company_name": "Auditor-General’s Office",
//    "designation": "Director of Audit",
//    "qualification": null,
//    "active": "",
//    "lastmodified": "2018-02-27 09:03:03"
