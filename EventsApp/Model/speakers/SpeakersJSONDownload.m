//
//  SpeakersJSONDownload.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 15/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "SpeakersJSONDownload.h"
#import "Constants.h"
#import "Speakers.h"

@implementation SpeakersJsonDownload{
    NSDictionary *arrSpeakers;
    NSDictionary *arrJSON;
    NSMutableArray *arrSpeakerObjects;
    NSMutableDictionary *arrGetUserInfo;
}
-(void) OrganizeDataFromJsonToObject{
    
    
    NSDictionary *headers = @{ @"authorization": @"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd",
                               @"cache-control": @"no-cache"
                               };
    Constants *cObj=[[Constants alloc]init];
    NSLog(@"ip adddress is :%@",cObj.endpoint);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/rest/getspeakers",cObj.endpoint]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            
                                                            
                                                            arrJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                            arrSpeakers = [arrJSON objectForKey:@"data"] ;
                                                            NSLog(@"data is %@",arrSpeakers);
                                                            arrSpeakerObjects=[[NSMutableArray alloc]init];
                                                            
                                                            for (NSDictionary *Speaker in arrSpeakers) {
                                                                
                                                                arrGetUserInfo=[Speaker objectForKey:@"speaker"];
                                                              
                                                                
                                                                Speakers *newEvent=[[Speakers alloc]init];
                                                                newEvent.speakerFirstName=[arrGetUserInfo objectForKey:@"first_name"];
                                                              
                                                                newEvent.speakerLastName=[arrGetUserInfo objectForKey:@"last_name"];
                                                                newEvent.speakerPicURL=[arrGetUserInfo objectForKey:@"picture"];
                                                                newEvent.speakerCompany=[arrGetUserInfo objectForKey:@"company_name"];
                                                                newEvent.speakerEventID=[Speaker objectForKey:@"eventid"];
                                                                newEvent.speakerPDFUrl=[arrGetUserInfo objectForKey:@"url"];
                                                             
                                                                newEvent.speakerPosition=[arrGetUserInfo objectForKey:@"designation"];
                                                                newEvent.active=[arrGetUserInfo objectForKey:@"active"];
                                                                newEvent.speakerID=[arrGetUserInfo objectForKey:@"id"];
                                                                [arrSpeakerObjects addObject:newEvent];
                                                                
                                                                NSLog(@"userinfo is %@", arrSpeakerObjects);
                                                                
                                                            }
                                                            //NSLog(@"array json count %@",temp);
                                                            NSLog(@"_________________-__________________________________");
                                                            //NSLog(@"arr events %@",arrSpon);
                                                            //NSLog(@"arr session %@",SessionTimes);
                                                            if (self.delegate) {
                                                                [self.delegate getSpeakersDownload:arrSpeakerObjects];
                                                            }
                                                            
                                                            
                                                        }
                                                        
                                                    });
                                                    
                                                }
                                      
                                      ];
    [dataTask resume];
    
    //    [NSThread sleepForTimeInterval:2.0f];
    NSLog(@"array parse success");
    //    return arrEventDetails;
    
}
@end
