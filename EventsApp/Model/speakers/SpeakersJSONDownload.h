//
//  SpeakersJSONDownload.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 15/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Speakers.h"
@protocol SpeakersJsonDownloadProtocol<NSObject>
-(void)getSpeakersDownload:(NSMutableArray *)speakers;// change
@end

@interface SpeakersJsonDownload : NSObject
@property(nonatomic, weak) id<SpeakersJsonDownloadProtocol> delegate;

-(void)OrganizeDataFromJsonToObject;
@end
