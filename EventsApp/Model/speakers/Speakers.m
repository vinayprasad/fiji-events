//
//  Speakers.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 15/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "Speakers.h"

@implementation Speakers
@synthesize speakerID,speakerPicURL,speakerCompany,speakerEventID,speakerLastName,speakerFirstName,speakerPosition,speakerPDFUrl;
-(id)initWithID:(NSNumber*)sID andName: (NSString*)sFirstName andLastName:(NSString*)sLastName andPicURL:(NSString*)sPicURL andCompany:(NSString*)sCompany andEventID:(NSString*)sEventID andPos:(NSString*)sPos andSpeakerPDFURL:(NSString*) sPDFURL{
    self=[super init];
    if (self){
        speakerID=sID;
        speakerFirstName=sFirstName;
        speakerLastName=sLastName;
        speakerPicURL=sPicURL;
        speakerCompany=sCompany;
        speakerEventID=sEventID;
        speakerPosition=sPos;
        speakerPDFUrl=sPDFURL;
    }
    return self;
    
}

-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy'T'HH:mm:ss.SSSZ"];
    
    return [dateFormatter dateFromString:StringDate];
}

@end
