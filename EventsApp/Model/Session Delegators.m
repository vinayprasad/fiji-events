//
//  Session Delegators.m
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "Session Delegators.h"

@implementation Session_Delegators
@synthesize sessionId,UserID;

-(id)initWithID:(NSNumber*)BsessionID andUser:(NSNumber*)BuserID{
    self=[super init];
    if (self){
        sessionId=BsessionID;
        UserID=BuserID;
    
    }
    return self;
}
@end
