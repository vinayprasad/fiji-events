//
//  Constants.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 12/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject
@property (nonatomic,strong) NSString *endpoint;
@property (nonatomic,strong) NSString *dbName;

@end
