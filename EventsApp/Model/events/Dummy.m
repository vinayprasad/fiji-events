//
//  Dummy.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 28/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "Dummy.h"

@implementation Dummy
@synthesize eventID, eventName,eventDesc,startDate,endDate,active,lastModified,eventImageURL, venueID;
-(id)initWithID:(NSNumber*)eID andName:(NSString*)eName andDesc: (NSString*)eDesc andEventImage:(NSString*)eImgURL andStartDate: (NSString*)sDate andEndDate :(NSString*)eDate andActive: (NSNumber*)active andLastModified: (NSString*)LModified andVenue:(NSString*)vID{
    self=[super init];
    if (self){
        eventID = eID;
        eventName = eName;
        eventDesc = eDesc;
        eventImageURL=eImgURL;
        startDate = [NSString stringWithFormat:@"%@",[self ConvertStringToDate:sDate]];
        endDate = [NSString stringWithFormat:@"%@",[self ConvertStringToDate:eDate]];
        lastModified = [NSString stringWithFormat:@"%@",[self ConvertStringToDate:LModified]];
        venueID = vID;
    }

    return self;
}

-(NSString*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date= [dateFormatter dateFromString:StringDate];
    [dateFormatter setDateFormat:@"dd-MMM-yyyy"];
//    NSDate *finaldate= [dateFormatter date];

    //    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
//    [dateFormatter1 setDateFormat:@"dd-MM-yyy"];
    
    return [dateFormatter stringFromDate:date];
} 
@end
