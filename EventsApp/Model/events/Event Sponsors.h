//
//  Event Sponsors.h
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event_Sponsors : NSObject
@property (strong,nonatomic) NSNumber *eventID;
@property (strong,nonatomic) NSNumber *sponsorID;

-(id)initWithID:(NSNumber*)BEventID andSponsor:(NSNumber*)BsponsorID;
@end
