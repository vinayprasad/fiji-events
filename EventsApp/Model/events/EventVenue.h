//
//  EventVenue.h
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventVenue : NSObject
@property (nonatomic, strong) NSNumber *venueID;
@property (nonatomic, strong) NSString *venueName;
@property (nonatomic, strong) NSString *venueLocation;
@property (nonatomic, strong) NSString *venueAddress;
@property (nonatomic, strong) NSDate *lastModified;

-(id)initWithID:(NSNumber*)vID andName:(NSString*)vName andLocation: (NSString*)vLocation andAddress :(NSString*) vAddress andLastModified: (NSString*)LModified;
-(NSDate*)ConvertStringToDate :(NSString*)StringDate;
@end
