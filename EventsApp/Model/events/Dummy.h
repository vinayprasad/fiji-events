//
//  Dummy.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 28/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Dummy : NSObject
@property (nonatomic, strong) NSNumber *eventID;
@property (nonatomic, strong) NSString *eventName;
@property (nonatomic, strong) NSString *eventDesc;
@property (nonatomic,strong) NSString *eventImageURL;
@property (nonatomic, strong) NSString *startDate;
@property (nonatomic, strong) NSString *endDate;
@property (nonatomic, strong) NSNumber *active;
@property (nonatomic, strong) NSString *lastModified;
@property (nonatomic,strong) NSString *venueID;
@property (nonatomic,strong) NSString *longtitude;
@property (nonatomic,strong) NSString *latitude;

-(id)initWithID:(NSNumber*)eID andName:(NSString*)eName andDesc: (NSString*)eDesc andEventImage:(NSString*)eImgURL andStartDate: (NSString*)sDate andEndDate :(NSString*)eDate andActive: (NSNumber*)active andLastModified: (NSString*)LModified andVenue:(NSString*)venueID;
-(NSString*)ConvertStringToDate :(NSString*)StringDate;
@end
