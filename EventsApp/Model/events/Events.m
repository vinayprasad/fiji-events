//
//  Events.m
//  FIAEventsApp
//
//  Created by temp on 01/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "Events.h"

@implementation Events
@synthesize eventID, eventName,eventDesc,startDate,endDate,active,lastModified,eventImageURL;
-(id)initWithID:(NSNumber*)eID andName:(NSString*)eName andDesc: (NSString*)eDesc andEventImage:(NSString*)eImgURL andStartDate: (NSString*)sDate andEndDate :(NSString*)eDate andActive: (NSNumber*)active andLastModified: (NSString*)LModified{
    self=[super init];
    if (self){
        eventID = eID;
        eventName = eName;
        eventDesc = eDesc;
        eventImageURL=eImgURL;
        startDate = [self ConvertStringToDate:sDate];
        endDate = [self ConvertStringToDate:eDate];;
        lastModified = [self ConvertStringToDate:LModified];
    }
    return self;
}

-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
        
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    return [dateFormatter dateFromString:StringDate];
} 
@end
