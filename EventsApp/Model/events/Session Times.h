//
//  Session Times.h
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Session_Times : NSObject

@property (nonatomic, strong) NSNumber *sessionID;
@property (nonatomic, strong) NSDate *sessionStartTime;
@property (nonatomic,strong) NSDate *sessionEndTime;
@property (nonatomic, strong) NSString *sessionProgramme;
@property (nonatomic, strong) NSNumber *active;
@property (nonatomic, strong) NSDate *lastmodified;
@property (nonatomic, strong) NSNumber *eventID;
@property (nonatomic, strong) NSNumber *venueID;
@property (nonatomic, strong) NSNumber *hasDiscussion;
@property (nonatomic,strong)  NSString *isDebatable;
@property (nonatomic,strong) NSDate *sessionDate;
@property (nonatomic,strong) NSString *sessionSDate;
@property (nonatomic,strong) NSString *sessionSpeaker;
@property (nonatomic,strong) NSNumber *sessionSpeakerID;
@property (nonatomic,strong) NSNumber *SessionOrderNumber;


-(id)initWithID:(NSNumber*)sID andStartTime:(NSString*)sTime andEndTime:(NSString*)eTime andProgramme:(NSString*)sProgramme andStatus:(NSNumber*)sActive andLastModified:(NSString*)LModified andEvent:(NSNumber*) sEventId andVenue:(NSNumber*) sVenueID andHasDiscussion:(NSNumber*) hasDiscuss andSessionDate:(NSString*) sDate andSpeakerID:(NSNumber*)sSpeakerID andOrderNo:(NSNumber*)OrderNumber;

-(NSDate*)ConvertStringToDate :(NSString*)StringDate;
@end
