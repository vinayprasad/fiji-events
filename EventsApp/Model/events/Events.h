//
//  Events.h
//  FIAEventsApp
//
//  Created by temp on 01/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Events : NSObject
@property (nonatomic, strong) NSNumber *eventID;
@property (nonatomic, strong) NSString *eventName;
@property (nonatomic, strong) NSString *eventDesc;
@property (nonatomic,strong) NSString *eventImageURL;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) NSNumber *active;
@property (nonatomic, strong) NSDate *lastModified;

-(id)initWithID:(NSNumber*)eID andName:(NSString*)eName andDesc: (NSString*)eDesc andEventImage:(NSString*)eImgURL andStartDate: (NSString*)sDate andEndDate :(NSString*)eDate andActive: (NSNumber*)active andLastModified: (NSString*)LModified;
-(NSDate*)ConvertStringToDate :(NSString*)StringDate;
@end



