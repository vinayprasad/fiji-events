//
//  EventJsonDownload.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 28/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "EventJsonDownload.h"
#import "Dummy.h"
#import "Constants.h"
#import "Session Times.h"

@implementation EventJsonDownload{
    NSDictionary *arrJSON;
    NSMutableArray *arrEventDetails;
    NSMutableDictionary *SessionTimes;
    NSDictionary *SessionDict;
    NSMutableArray *sessionTimesFinal;
    NSDictionary *tempa;
    BOOL completed;
}

-(void) OrganizeDataFromJsonToObject{
    completed=false;
    
    NSDictionary *headers = @{ @"authorization": @"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd",
                               @"cache-control": @"no-cache"
                               };
    Constants *cObj=[[Constants alloc]init];
    NSLog(@"ip adddress is :%@",cObj.endpoint);
    
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/rest/getallevents",cObj.endpoint]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];

    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            
                                                            
                                                            arrJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                            tempa = [arrJSON objectForKey:@"data"];
                                                            //SessionTimes=[[arrJSON objectForKey:@"data"] objectForKey:@"session"];
                                                            NSLog(@"tempa %@", tempa);
                                                            sessionTimesFinal=[NSMutableArray array];
                                                            NSLog(@"%@", SessionTimes);
                                                            arrEventDetails=[NSMutableArray array];
                                                            for (NSDictionary *temp in tempa) {
                                                                Dummy *newEvent=[[Dummy alloc]init];
                                                                newEvent.active=[temp objectForKey:@"active"];
                                                                newEvent.eventName=[temp objectForKey:@"name"];
                                                                newEvent.eventID = [temp objectForKey:@"id"];
                                                                newEvent.eventDesc = [temp objectForKey:@"description"];
                                                                newEvent.endDate = [temp objectForKey:@"end_date"];
                                                                newEvent.eventImageURL = [temp objectForKey:@"image_cover"];
                                                                newEvent.lastModified = [temp objectForKey:@"lastmodified"];
                                                                newEvent.startDate =[self ConvertStringToDate:[temp objectForKey:@"start_date"]];
                                                                newEvent.longtitude=[temp objectForKey:@"long"];
                                                                newEvent.latitude=[temp objectForKey:@"lat"];
                                                                
                                                                newEvent.venueID=[NSString stringWithFormat:@"%@ ,%@",[temp objectForKey:@"venue"],[temp objectForKey:@"venue_address"]];
                                                                SessionDict =[temp objectForKey:@"session"];
                                                                for (NSDictionary *sessionObj in SessionDict) {
                                                                    NSLog(@"Session chunk is %@ object is %@", SessionDict, sessionObj);
                                                                    Session_Times *newEvent=[[Session_Times alloc]init];
                                                                    newEvent.sessionID=[sessionObj objectForKey:@"id"];
                                                                    newEvent.sessionStartTime=[sessionObj objectForKey:@"start_time"];
                                                                    newEvent.sessionEndTime = [sessionObj objectForKey:@"end_time"];
                                                                    newEvent.sessionProgramme = [sessionObj objectForKey:@"program"];
                                                                    newEvent.active = [sessionObj objectForKey:@"active"];
                                                                    newEvent.lastmodified = [sessionObj objectForKey:@"lastmodified"];
                                                                    newEvent.venueID = [sessionObj objectForKey:@"venue_id"];
                                                                    newEvent.eventID =[sessionObj objectForKey:@"events_id"];
                                                                    newEvent.isDebatable=[sessionObj objectForKey:@"isdebate"];
                                                                    NSLog(@"isdebate = %@",[sessionObj objectForKey:@"isdebate"]);
                                                                    newEvent.sessionDate=[sessionObj objectForKey:@"session_date"];
                                                                    newEvent.sessionSDate =[sessionObj objectForKey:@"session_date"];
                                                                    newEvent.hasDiscussion=[sessionObj objectForKey:@"hasdiscussion"];
                                                                    newEvent.sessionSpeaker = [sessionObj objectForKey:@"speakername"];
                                                                    newEvent.sessionSpeakerID = [sessionObj objectForKey:@"speakerid"];
                                                                    
                                                                    [sessionTimesFinal addObject:newEvent];
                                                                    
                                                                    
                                                                }
                                                            
                                        
                                                                //SessionTimes=SessionDict;
                                                                [arrEventDetails addObject:newEvent];
                                                                
                                                                NSLog(@"array json count %@",temp);
                                                                NSLog(@"_________________-__________________________________");
                                                                NSLog(@"arr session %ld",sessionTimesFinal.count);
                                                            }
                                                            
                                                            
                                                            
                                                                //NSLog(@"arr session %@",SessionTimes);
                                                                if (self.delegate) {
                                                                    [self.delegate getDownloaded:arrEventDetails andSession:sessionTimesFinal];
                                                                }
                                                            
                                                        //} moved it up
                                                        }

                                                    });
                                                    
                                                }

                                      ];
    [dataTask resume];
    
//    [NSThread sleepForTimeInterval:2.0f];
    NSLog(@"array parse success");
//    return arrEventDetails;
    
}

-(NSString*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date= [dateFormatter dateFromString:StringDate];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
     [dateFormatter setDateFormat:@"dd"];
    NSString *day =[dateFormatter stringFromDate:date];
    [dateFormatter setDateFormat:@"MMM"];
    NSString *month =[dateFormatter stringFromDate:date];
     [dateFormatter setDateFormat:@"yyyy"];
    NSString *year =[dateFormatter stringFromDate:date];
    NSString *finaldate =[NSString stringWithFormat:@"%@th %@, %@",day,month,year];
    
    return finaldate ;
}


@end
