//
//  Event Attendees.h
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Event_Attendees : NSObject
@property (strong,nonatomic) NSNumber *eventID;
@property (strong,nonatomic) NSNumber *UserID;

-(id)initWithID:(NSNumber*)BeventID andUser:(NSNumber*)BuserID;
@end
