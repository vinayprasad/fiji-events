//
//  EventJsonDownload.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 28/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol EventJsonDownloadProtocol<NSObject>
-(void)getDownloaded:(NSMutableArray *)events andSession:(NSMutableArray*)session;
@end

@interface EventJsonDownload : NSObject
@property(nonatomic, weak) id<EventJsonDownloadProtocol> delegate;
-(void)OrganizeDataFromJsonToObject;
-(NSString*)ConvertStringToDate :(NSString*)StringDate;
//-(void) getData;
@end
