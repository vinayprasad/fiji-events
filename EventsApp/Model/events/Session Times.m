//
//  Session Times.m
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "Session Times.h"

@implementation Session_Times
@synthesize sessionID,sessionStartTime,sessionEndTime, sessionProgramme,active,lastmodified,eventID,venueID,hasDiscussion,sessionDate,sessionSpeakerID,SessionOrderNumber;

-(id)initWithID:(NSNumber*)sID andStartTime:(NSString*)sTime andEndTime:(NSString*)eTime andProgramme:(NSString*)sProgramme andStatus:(NSNumber*)sActive andLastModified:(NSString*)LModified andEvent:(NSNumber*) sEventId andVenue:(NSNumber*) sVenueID andHasDiscussion:(NSNumber*) hasDiscuss andSessionDate:(NSString*) sDate andSpeakerID:(NSNumber*)sSpeakerID andOrderNo:(NSNumber *)OrderNumber{
    self=[super init];
    if (self){
        sessionID = sID;
        sessionStartTime=[self ConvertStringToDate:sTime];
        sessionEndTime=[self ConvertStringToDate:eTime];
        sessionProgramme = sProgramme;
        active = sActive;
        lastmodified = [self ConvertStringToDate:LModified];
        eventID = sEventId;
        venueID = sVenueID;
        hasDiscussion=hasDiscuss;
        sessionDate=[self ConvertStringToDate:sDate];
        sessionSpeakerID=sSpeakerID;
        SessionOrderNumber=OrderNumber;
    }
    return self;
}

-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy"];
    
    return [dateFormatter dateFromString:StringDate];
}
@end
