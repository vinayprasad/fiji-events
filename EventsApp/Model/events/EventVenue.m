//
//  EventVenue.m
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "EventVenue.h"

@implementation EventVenue
@synthesize venueID,venueName,venueAddress,venueLocation,lastModified;
-(id)initWithID:(NSNumber*)vID andName:(NSString*)vName andLocation: (NSString*)vLocation andAddress :(NSString*) vAddress andLastModified: (NSString*)LModified{

    self=[super init];
    if (self){
        venueID = vID;
        venueName = vName;
        venueAddress = vAddress;
        lastModified = [self ConvertStringToDate:LModified];
        
    }
    return self;
}
-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy'T'HH:mm:ss.SSSZ"];
    
    return [dateFormatter dateFromString:StringDate];
}
@end
