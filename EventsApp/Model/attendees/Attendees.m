//
//  Attendees.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 09/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "Attendees.h"

@implementation Attendees
@synthesize AttendeesPicURL,AttendeesLastName,AttendeesFirstName,AttendeeCompany,AttendeeEventID;

-(id)initWithID: (NSString*)AFirstName andLastName:(NSString*)ALastName andPicURL:(NSString*)APicURL andCompany:(NSString*)ACompany andEventID:(NSString*)aEventID{
    self=[super init];
    if (self){
        AttendeesFirstName=AFirstName;
        AttendeesLastName=ALastName;
        AttendeesPicURL=APicURL;
        AttendeeCompany=ACompany;
        AttendeeEventID=aEventID;
    }
    return self;
    
}

-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy'T'HH:mm:ss.SSSZ"];
    
    return [dateFormatter dateFromString:StringDate];
}
@end
