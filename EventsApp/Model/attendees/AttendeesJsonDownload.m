//
//  AttendeesJsonDownload.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 09/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "AttendeesJsonDownload.h"
#import "Attendees.h"
#import "Constants.h"

@implementation AttendeesJsonDownload{
    NSDictionary *arrAttendees;
    NSDictionary *arrJSON;
    NSMutableArray *arrAttendeesObjects;
    NSMutableDictionary *arrGetUserInfo;

}
-(void) OrganizeDataFromJsonToObject{
    
    
    NSDictionary *headers = @{ @"authorization": @"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd",
                               @"cache-control": @"no-cache"
                               };
    Constants *cObj=[[Constants alloc]init];
    NSLog(@"ip adddress is :%@",cObj.endpoint);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/rest/getattendees",cObj.endpoint]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            
                                                            
                                                            arrJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                            arrAttendees = [arrJSON objectForKey:@"data"] ;
                                                            
                                                            //NSLog(@"--->>>=%@",arrAttendees);
                                                            //SessionTimes=[tempa objectForKey:@"session"];
                                                            //NSLog(@"%@", arrJSON);
                                                            arrAttendeesObjects=[NSMutableArray array];
                                                            
                                                            for (NSDictionary *attendee in arrAttendees) {
                                                                
                                                                arrGetUserInfo=[attendee objectForKey:@"userinfo"];

                                                                
                                                                Attendees *newEvent=[[Attendees alloc]init];
                                                               
                                                                newEvent.attendeeID = [arrGetUserInfo objectForKey:@"id"]; newEvent.AttendeesFirstName=[arrGetUserInfo objectForKey:@"first_name"];

                                                                newEvent.AttendeesLastName=[arrGetUserInfo objectForKey:@"last_name"];
                                                                newEvent.AttendeesPicURL=[arrGetUserInfo objectForKey:@"picture"];
                                                                newEvent.AttendeeCompany=[arrGetUserInfo objectForKey:@"company_name"];
                                                                newEvent.AttendeeEventID=[attendee objectForKey:@"events_id"];
                                                                newEvent.active=[attendee objectForKey:@"active"];
                                                                newEvent.attendeeLastModified=[attendee objectForKey:@"lastmodified"];
                                                                [arrAttendeesObjects addObject:newEvent];
                                                               

                                                                
                                                            }

                                                            if (self.delegate) {
                                                                [self.delegate getAttendeesDownload:arrAttendeesObjects];
                                                            }
                                                            
                                                            
                                                        }
                                                        
                                                    });
                                                    
                                                }
                                      
                                      ];
    [dataTask resume];
    
    //    [NSThread sleepForTimeInterval:2.0f];
//    NSLog(@"array parse success");
    //    return arrEventDetails;
    
}
@end
