//
//  Attendees.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 09/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Attendees : NSObject
@property (nonatomic,weak) NSNumber *attendeeID;
@property (nonatomic,weak) NSString *AttendeesFirstName;
@property (nonatomic,weak) NSString *AttendeesLastName;
@property (nonatomic,weak) NSString *AttendeesPicURL;
@property (nonatomic,weak) NSString *AttendeeCompany;
@property (nonatomic,weak) NSString *AttendeeEventID;
@property (nonatomic,weak) NSNumber *active;
@property (nonatomic,weak) NSString *attendeeLastModified;

-(id)initWithID: (NSString*)AFirstName andLastName:(NSString*)ALastName andPicURL:(NSString*)APicURL andCompany:(NSString*)ACompany andEventID:(NSString*)aEventID;

-(NSDate*)ConvertStringToDate :(NSString*)StringDate;
@end
