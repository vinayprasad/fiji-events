//
//  AttendeesJsonDownload.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 09/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Attendees.h"
@protocol AttendeesJsonDownloadProtocol<NSObject>
-(void)getAttendeesDownload:(NSMutableArray *)attendees;// change
@end

@interface AttendeesJsonDownload : NSObject
@property(nonatomic, weak) id<AttendeesJsonDownloadProtocol> delegate;

-(void)OrganizeDataFromJsonToObject;
@end
