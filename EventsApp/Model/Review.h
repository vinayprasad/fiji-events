//
//  Review.h
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Review : NSObject
@property (strong,nonatomic) NSNumber *reviewID;
@property (strong,nonatomic) NSNumber *reviewUserID;
@property (strong,nonatomic) NSNumber *reviewRating;
@property (strong,nonatomic) NSString *reviewMessage;
@property (strong,nonatomic) NSNumber *Active;
@property (strong,nonatomic) NSDate *Lastmodified;

-(id)initWithID: (NSNumber*) rID andUser: (NSNumber*)rUserID andRating: (NSNumber*)rRating andMesage:(NSString*)rMessage andStatus:(NSNumber*)rActive andLastModified:(NSString*)Lmodified;

-(NSDate*)ConvertStringToDate :(NSString*)StringDate;


@end
