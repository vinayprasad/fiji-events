//
//  UserInfoDownload.m
//  FIAEventsApp
//
//  Created by Ashivan on 4/11/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import "UserInfoDownload.h"
#import "Constants.h"

@implementation UserInfoDownload

-(void)OrganizeDataFromJsonToObject:(NSString *)username andPassword:(NSString *)password{
    //[self LoginForUser:username andpassword:password];
    Constants *cObj=[[Constants alloc]init];
    NSLog(@"ip adddress is :%@",cObj.endpoint);
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/rest/checkuser",cObj.endpoint]]];
    
    NSString *userUpdate =[NSString stringWithFormat:@"username=%@&password=%@", username,password];
    
    //create the Method "GET" or "POST"
    [urlRequest setHTTPMethod:@"POST"];
    
    [urlRequest setValue:@"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd" forHTTPHeaderField:@"Authorization"];
    //Convert the String to Data
    NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    
    //Apply the data to the body
    [urlRequest setHTTPBody:data1];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSLog(@"Response =%@",response);
        if(httpResponse.statusCode == 200)
        {
            NSError *parseError = nil;
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
            NSLog(@"The response is - %@",responseDictionary);
            NSDictionary *success = [responseDictionary objectForKey:@"data"];
            NSLog(@"The success is - %@",success);
            if(success.count>0)
            {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"%@",success);
                    for(NSDictionary *successObj in success){
                        Users *obj=[[Users alloc]init];
                        [[NSUserDefaults standardUserDefaults] setValue:[successObj objectForKey:@"id"] forKey:@"UserID"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        obj.UserID=[successObj objectForKey:@"id"];
                        obj.FirstName=[successObj objectForKey:@"first_name"];
                        obj.LastName=[successObj objectForKey:@"last_name"];
                        obj.Email=[successObj objectForKey:@"email"];
                        obj.Phone=[successObj objectForKey:@"phone"];
                        obj.Picture=[successObj objectForKey:@"picture"];
                        obj.Gender=[successObj objectForKey:@"gender"];
                        obj.CompanyName=[successObj objectForKey:@"company_name"];
                        obj.Designation=[successObj objectForKey:@"designation"];
                        obj.Qualificaton=[successObj objectForKey:@"qualification"];
                        obj.Active=[successObj objectForKey:@"active"];
                        obj.LastModified=[successObj objectForKey:@"lastmodified"];
                        
                        if (self.delegate) {
                            [self.delegate getDownloaded:obj];
                        }
                    }
                    NSLog(@"Login SUCCESS");
                    //                    [self performSegueWithIdentifier:@"MainPage" sender:self];
                });
                
                //                check=true;
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //                    [self showsUIAlertWithMessage:@"Authentication Error" andTitle:@"Username or Password not found"];
                });
                
                NSLog(@"Login FAILURE");
                
            }
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"Error response is %@", response);
                //                [self showsUIAlertWithMessage:@"Error Message" andTitle:@"host not found. Please try again"];
                //[self performSegueWithIdentifier:@"MainPage" sender:self];
            });
            
        }
    }];
    [dataTask resume];
}

-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy'T'HH:mm:ss.SSSZ"];
    
    return [dateFormatter dateFromString:StringDate];
}
@end
