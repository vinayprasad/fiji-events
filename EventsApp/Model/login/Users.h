//
//  Users.h
//  FIAEventsApp
//
//  Created by temp on 06/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>




@interface Users : NSObject
@property (strong,nonatomic) NSNumber *UserID;
@property (strong,nonatomic) NSString *Username;
@property (strong,nonatomic) NSString *Password;
@property (strong,nonatomic) NSString *FirstName;
@property (strong,nonatomic) NSString *LastName;
@property (strong,nonatomic) NSString *Email;
@property (strong,nonatomic) NSString *Phone;
@property (strong,nonatomic) NSString *Picture;
@property (strong,nonatomic) NSString *Gender;
@property (strong,nonatomic) NSString *CompanyName;
@property (strong,nonatomic) NSString *Designation;
@property (strong,nonatomic) NSString *Qualificaton;
@property (strong,nonatomic) NSNumber *Active;
@property (strong,nonatomic) NSDate *LastModified;


-(NSString*)ConvertStringToDate :(NSString*)StringDate;

-(id)initWithID:(NSNumber*)uID andName:(NSString*)Uname andPassword:(NSString*)uPassword andFirstname:(NSString*)Fname andLastname:(NSString*)LName andEmail:(NSString*)uEmail andPhone:(NSString*)uPhone andPicture:(NSString*)uPicture andGender:(NSString*)uGender andCompany:(NSString*)CompanyName andDesignation:(NSString*)uDesignation andQualification:(NSString*)uQual andStatus:(NSNumber*)uActive andlastmodified:(NSDate*)LModified;

@end
