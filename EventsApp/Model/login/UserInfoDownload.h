//
//  UserInfoDownload.h
//  FIAEventsApp
//
//  Created by Ashivan on 4/11/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Users.h"
@protocol UserInfoDownloadProtocol<NSObject>
-(void)getDownloaded:(Users *)userinfo;
@end
@interface UserInfoDownload : NSObject
@property(nonatomic, weak) id<UserInfoDownloadProtocol> delegate;
-(void)OrganizeDataFromJsonToObject:(NSString*)username andPassword:(NSString*)password;
-(NSString*)ConvertStringToDate :(NSString*)StringDate;
@end
