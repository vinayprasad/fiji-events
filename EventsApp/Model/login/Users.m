//
//  Users.m
//  FIAEventsApp
//
//  Created by temp on 06/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "Users.h"
#import "Constants.h"

@implementation Users{
}
@synthesize UserID,Username,Password,FirstName,LastName,Email,Phone,Picture,Gender,CompanyName,Designation,Qualificaton,Active,LastModified;

-(id)initWithID:(NSNumber*)uID andName:(NSString*)Uname andPassword:(NSString*)uPassword andFirstname:(NSString*)Fname andLastname:(NSString*)LName andEmail:(NSString*)uEmail andPhone:(NSString*)uPhone andPicture:(NSString*)uPicture andGender:(NSString*)uGender andCompany:(NSString*)CompName andDesignation:(NSString*)uDesignation andQualification:(NSString*)uQual andStatus:(NSNumber*)uActive andlastmodified:(NSString*)LModified{
    
    self=[super init];
    if (self){
        
        UserID=uID;
        Username=Uname;
        Password=uPassword;
        FirstName=Fname;
        LastName=LName;
        Email=uEmail;
        Phone=uPhone;
        Picture=uPicture;
        Gender=uGender;
        CompanyName=CompName;
        Designation=uDesignation;
        Qualificaton=uQual;
        Active=uActive;
        LastModified = [self ConvertStringToDate:LModified];
        
    }
    return self;
}

-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy'T'HH:mm:ss.SSSZ"];
    
    return [dateFormatter dateFromString:StringDate];
}


@end
