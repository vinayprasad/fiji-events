//
//  Review.m
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "Review.h"

@implementation Review
@synthesize reviewID,reviewUserID,reviewRating,reviewMessage,Active,Lastmodified;
-(id)initWithID: (NSNumber*) rID andUser: (NSNumber*)rUserID andRating: (NSNumber*)rRating andMesage:(NSString*)rMessage andStatus:(NSNumber*)rActive andLastModified:(NSString*)Lmodified{
    self=[super init];
    if (self){
        reviewID=rID;
        reviewUserID = rUserID;
        reviewRating = rRating;
        reviewMessage = rMessage;
        Active = rActive;
        Lastmodified = [self ConvertStringToDate:Lmodified];
        
        
    }
    return self;
    
}

-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy'T'HH:mm:ss.SSSZ"];
    
    return [dateFormatter dateFromString:StringDate];
}
@end
