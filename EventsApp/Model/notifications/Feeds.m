//
//  Feeds.m
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "Feeds.h"

@implementation Feeds
//@synthesize feedsID,feedsMsg,lastmodified,userID,active,session_id;
//-(id)initWithID:(NSNumber*)fID andMessage:(NSString*)fmsg andUser:(NSNumber*)fuserid andSession:(NSNumber*)fsessionID andLastModified:(NSString*)LModified andStatus:(NSNumber*)factive{
//    self=[super init];
//    if (self){
//        feedsID = fID;
//        feedsMsg = fmsg;
//        lastmodified = LModified;
//        userID=fuserid;
//        active=factive;
//        session_id=fsessionID;
//
//    }
//    return self;
//}

-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy'T'HH:mm:ss.SSSZ"];
    
    return [dateFormatter dateFromString:StringDate];
}

@end
