//
//  NotificationDownload.h
//  FIAEventsApp
//
//  Created by Ashivan on 5/10/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol NotificationDownloadProtocol<NSObject>
-(void)getNotification:(NSMutableArray *)notify;
@end

@interface NotificationDownload : NSObject
@property(nonatomic, weak) id<NotificationDownloadProtocol> delegate;
-(void)OrganizeDataFromJsonToObject;
-(NSString*)ConvertStringToDate :(NSString*)StringDate;
//-(void) getData;
@end
