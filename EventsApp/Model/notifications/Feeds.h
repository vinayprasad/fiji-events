//
//  Feeds.h
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Feeds : NSObject
@property (nonatomic, strong) NSNumber *notifyID;
@property (nonatomic, strong) NSString *notifyTitle;
@property (nonatomic, strong) NSString *notifyBody;
@property (nonatomic, strong) NSString *lastmodified;
@property (nonatomic, strong) NSNumber *active;

//-(id)initWithID:(NSNumber*)fID andMessage:(NSString*)fmsg andUser:(NSNumber*)fuserid andSession:(NSNumber*)fsessionID andLastModified:(NSDate*)LModified andStatus:(NSNumber*)factive;

-(NSDate*)ConvertStringToDate :(NSString*)StringDate;
@end
