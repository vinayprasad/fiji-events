//
//  NotificationDownload.m
//  FIAEventsApp
//
//  Created by Ashivan on 5/10/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import "NotificationDownload.h"
#import "Constants.h"
#import "Feeds.h"

@implementation NotificationDownload{
NSDictionary *arrJSON;
NSDictionary *notifyJSON;
NSMutableArray *arrNotification;
    NSMutableArray *arrNotificationCut;
BOOL completed;
}

-(void) OrganizeDataFromJsonToObject{
    completed=false;
    
    NSDictionary *headers = @{ @"authorization": @"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd",
                               @"cache-control": @"no-cache"
                               };
    Constants *cObj=[[Constants alloc]init];
    NSLog(@"ip adddress is :%@",cObj.endpoint);
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/rest/getnotifications",cObj.endpoint]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                            // load offline data if available
                                                            //else alert that no data was found offline
                                                        } else {
                                                            @try {
                                                               
                                                                arrJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                                notifyJSON = [arrJSON objectForKey:@"data"];
                                                                NSString *Check=[NSString stringWithFormat:@"%@",[arrJSON objectForKey:@"status"]];
                                                                arrNotification=[[NSMutableArray alloc]init];
                                                                //                                                            NSLog(@"arrjson %@",notifyJSON);
                                                                if(![Check isEqualToString:@"0"]){
                                                                    for(NSDictionary *objNotify in notifyJSON){
                                                                        Feeds *obj =[[Feeds alloc]init];
                                                                        obj.notifyID=[objNotify objectForKey:@"id"];
                                                                        obj.notifyTitle=[objNotify objectForKey:@"title"];
                                                                        obj.notifyBody=[objNotify objectForKey:@"body"];
                                                                        obj.active=[objNotify objectForKey:@"sent"];
                                                                        obj.lastmodified=[objNotify objectForKey:@"lastmodified"];
                                                                        //                                                                NSLog(@"%@",obj.lastmodified);
                                                                        
                                                                        [arrNotification addObject:obj];
                                                                        
                                                                    }
                                                                    
                                                                    arrNotificationCut=[[NSMutableArray alloc]init];
                                                                    for(Feeds *feedsobj in arrNotification){
                                                                        if([feedsobj.active intValue]==1){
                                                                            [arrNotificationCut addObject:feedsobj];
                                                                        }
                                                                        
                                                                        
                                                                    }
                                                                    
                                                                }
                                                                if (self.delegate){
                                                                    [self.delegate getNotification:arrNotificationCut];
                                                                }
                                                            } @catch (NSException *e) {
                                                                NSLog(@"Could not load notificstions");
                                                            }
                                                            
                                                            

                                                    
                                                        }
                                                        
                                                    });
                                                    
                                                }
                                      
                                      ];
    [dataTask resume];
    
    //    [NSThread sleepForTimeInterval:2.0f];
    NSLog(@"array parse success");
    //    return arrEventDetails;
    
}

-(NSString*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date= [dateFormatter dateFromString:StringDate];
    [dateFormatter setDateFormat:@"dd MMM yyyy"];
    [dateFormatter setDateFormat:@"dd"];
    NSString *day =[dateFormatter stringFromDate:date];
    [dateFormatter setDateFormat:@"MMM"];
    NSString *month =[dateFormatter stringFromDate:date];
    [dateFormatter setDateFormat:@"yyyy"];
    NSString *year =[dateFormatter stringFromDate:date];
    NSString *finaldate =[NSString stringWithFormat:@"%@th %@, %@",day,month,year];
    
    return finaldate ;
}

@end
