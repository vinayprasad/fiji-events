//
//  Sponsors.m
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "Sponsors.h"

@implementation Sponsors
@synthesize SponsorID,SponsorActive,SponsorTypeID,SponsorWebURL,SponsorLogoURL,SponsorCompanyName,lastmodified,SponsorEventID;

-(id)initWithID: (NSNumber*)sID andCompanyName:(NSString*) SCompName andLogo:(NSString*)SLogoURL andlastmodified:(NSString*)Lmodified andSponsorType:(NSString*)STypeID andActive:(NSNumber*)active andSpsonsorPage:(NSString*) SWebURL andEventID: (NSString*)sEventID;{
    self=[super init];
    if (self){
        SponsorID=sID;
        SponsorCompanyName=SCompName;
        SponsorLogoURL = SLogoURL;
        lastmodified=[self ConvertStringToDate:Lmodified];
        SponsorTypeID=STypeID;
        SponsorActive=active;
        SponsorWebURL=SWebURL;
        SponsorEventID=sEventID;
        
    }
    return self;

}

-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy'T'HH:mm:ss.SSSZ"];
    
    return [dateFormatter dateFromString:StringDate];
}
@end
