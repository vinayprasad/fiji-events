//
//  Sponsors.h
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Sponsors : NSObject
@property (nonatomic,weak ) NSNumber *SponsorID;
@property (nonatomic,weak) NSString *SponsorCompanyName;
@property (nonatomic,weak) NSString *SponsorLogoURL;
@property (nonatomic,weak) NSDate *lastmodified;
@property (nonatomic,weak) NSString *SponsorTypeID;
@property (nonatomic,weak) NSNumber *SponsorActive;
@property (nonatomic,weak) NSString *SponsorWebURL;
@property (nonatomic,weak) NSString *SponsorEventID;

-(id)initWithID: (NSNumber*)sID andCompanyName:(NSString*) SCompName andLogo:(NSString*)SLogoURL andlastmodified:(NSString*)Lmodified andSponsorType:(NSString*)STypeID andActive:(NSNumber*)active andSpsonsorPage:(NSString*) SWebURL andEventID: (NSString*)sEventID;

-(NSDate*)ConvertStringToDate :(NSString*)StringDate;
@end
