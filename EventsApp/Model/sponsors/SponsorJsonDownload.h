//
//  SponsorJsonDownload.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 08/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol SponsorJsonDownloadProtocol<NSObject>
-(void)getSponsorDownload:(NSMutableArray *)sponsors;// change
@end

@interface SponsorJsonDownload : NSObject
@property(nonatomic, weak) id<SponsorJsonDownloadProtocol> delegate;

-(void)OrganizeDataFromJsonToObject;
@end
