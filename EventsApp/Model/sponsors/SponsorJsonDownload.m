//
//  SponsorJsonDownload.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 08/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "SponsorJsonDownload.h"
#import "Sponsors.h"
#import "Constants.h"
@implementation SponsorJsonDownload{
    NSDictionary *arrSpon;
    NSDictionary *arrJSON;
    NSMutableArray *arrSponsorObjects;
    NSDictionary *arrSponsorMain;
    
}
-(void) OrganizeDataFromJsonToObject{
   
    
    NSDictionary *headers = @{ @"authorization": @"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd",
                               @"cache-control": @"no-cache"
                               };
    
    Constants *cObj=[[Constants alloc]init];
    NSLog(@"ip adddress is :%@",cObj.endpoint);
    
  
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/rest/getsponsors",cObj.endpoint]]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:120.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                        if (error) {
                                                            NSLog(@"%@", error);
                                                        } else {
                                                            
                                                            
                                                            arrJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                            arrSponsorMain = [arrJSON objectForKey:@"data"];
                                                            NSLog(@"%@",arrSponsorMain);
                                                            arrSponsorObjects=[[NSMutableArray alloc]init];
                                                            
                                                           
                                                            for(NSDictionary *SponObj in arrSponsorMain){
                                                                 Sponsors *newEvent=[[Sponsors alloc]init];
                                                                 newEvent.SponsorEventID=[SponObj objectForKey:@"eventid"];
                                                                
                                                                arrSpon=[SponObj objectForKey:@"sponsor"];
                                                                NSLog(@"%@",arrSpon);
                                                                    newEvent.SponsorID=[arrSpon objectForKey:@"id"];
                                                                    newEvent.SponsorCompanyName=[arrSpon objectForKey:@"companyname"];
                                                                    newEvent.SponsorLogoURL=[arrSpon objectForKey:@"logo"];
                                                                    newEvent.lastmodified=[arrSpon objectForKey:@"lastmodified"];
                                                                    newEvent.SponsorTypeID=[arrSpon objectForKey:@"sponsortype_id"];
                                                                    newEvent.SponsorActive=[arrSpon objectForKey:@"active"];
                                                                    newEvent.SponsorWebURL=[arrSpon objectForKey:@"url"];
                                                                    [arrSponsorObjects addObject:newEvent];
                                                                
                                                            }
                                                            
                                                            NSLog(@"Sponsor Objects =%lu and Event count =%lu",(unsigned long)arrSponsorMain.count,(unsigned long)arrSponsorObjects.count);
                                                           
                                                            
                                                            
                                                            
                                                                //NSLog(@"array json count %@",temp);
                                                                NSLog(@"_________________-__________________________________");
                                                                //NSLog(@"arr events %@",arrSpon);
                                                                //NSLog(@"arr session %@",SessionTimes);
                                                                if (self.delegate) {
                                                                    [self.delegate getSponsorDownload:arrSponsorObjects];
                                                                }
                                                            
                                                            
                                                        }
                                                        
                                                    });
                                                    
                                                }
                                      
                                      ];
    [dataTask resume];
    
    //    [NSThread sleepForTimeInterval:2.0f];
    NSLog(@"array parse success");
    //    return arrEventDetails;
    
}
@end
