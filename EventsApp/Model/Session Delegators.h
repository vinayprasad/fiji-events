//
//  Session Delegators.h
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Session_Delegators : NSObject
@property (strong,nonatomic) NSNumber *sessionId;
@property (strong,nonatomic) NSNumber *UserID;

-(id)initWithID:(NSNumber*)BsessionID andUser:(NSNumber*)BuserID;
@end
