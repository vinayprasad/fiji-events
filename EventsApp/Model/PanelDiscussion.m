//
//  PanelDiscussion.m
//  FIAEventsApp
//
//  Created by temp on 05/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "PanelDiscussion.h"

@implementation PanelDiscussion
@synthesize panelID,panelquestion,sessionId,userID,active,lastmodified;

-(id)initWithID :(NSNumber*)pID andQuestion:(NSString*)pQuestion andSession:(NSNumber*)pSessionID andUser:(NSNumber*)pUserID andStatus:(NSNumber*)pActive andLastModified:(NSString*)Lmodified{
    self=[super init];
    if (self){
        panelID=pID;
        panelquestion=pQuestion;
        sessionId=pSessionID;
        userID=pUserID;
        active=pActive;
        lastmodified = [self ConvertStringToDate:Lmodified];
        
        
    }
    return self;
}

-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy'T'HH:mm:ss.SSSZ"];
    
    return [dateFormatter dateFromString:StringDate];
}
@end
