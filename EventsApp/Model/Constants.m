//
//  Constants.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 12/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "Constants.h"

@implementation Constants
-(id)init{
    
    if (self=[super init]) {
        _endpoint=@"ec2-54-66-220-147.ap-southeast-2.compute.amazonaws.com";
//        _endpoint=@"192.168.10.74";
        _dbName=@"FIAOfflineDataV12345678.sqlite";
    }
    return self;
}
@end
