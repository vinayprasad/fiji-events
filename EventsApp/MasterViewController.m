//
//  MasterViewController.m
//  FIAEventsApp
//
//  Created by temp on 31/01/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "MasterViewController.h"
#import "DetailViewController.h"
#import "EventJsonDownload.h"
#import "Dummy.h"
#import "EventListTableViewCell.h"
#import "EventViewController.h"
#import "UserInfoDownload.h"
#import "Users.h"
#import "UserProTableViewCell.h"
#import "MyProfileViewController.h"
#import "Constants.h"
#import "Haneke.h"
#import "DBManager.h"

@interface MasterViewController (){
    EventJsonDownload *EventList;
    UserInfoDownload *userinfoObj;
    Users *currentUserInfo;
    NSMutableArray *arrEvents;
    long eventID;
    UIView *overlayview;
    
    
}


@property NSMutableArray *objects;
@property (nonatomic, strong) DBManager *dbManager;
@end

@implementation MasterViewController

- (void)viewDidLoad {
    [super viewDidLoad];

  
    overlayview =[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x
                                                         ,self.view.frame.origin.y
                                                         , self.view.frame.size.width, self.view.frame.size.height)];
    

    Constants *obj=[[Constants alloc]init];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:obj.dbName];
  
    [self setUserInfo];
      [self getDataFromDB];
    
   
    // Do any additional setup after loading the view, typically from a nib.

}

-(void)getDownloaded:(Users *)userinfo{
    if(userinfo==NULL){
        currentUserInfo.FirstName=@"Guest";
        currentUserInfo.LastName=@"";
        currentUserInfo.Picture=@"FIA Logo HD.png";
    }else{
    currentUserInfo=userinfo;
    }
    [_tblMenu reloadData];
}

-(void)getDataFromDB{
    NSString *query = [NSString stringWithFormat:@"select * from tblEvents"];
    
    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
    NSLog(@"db elements %@",dbresults);
    
    NSMutableArray * arrGetEvent=[NSMutableArray array];
    //        NSMutableArray * arrGetEvent;
    for(int i=0;i<dbresults.count;i++){
        Dummy *obj =[[Dummy alloc]init];
        
        
        obj.eventID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"id"]];
        obj.eventName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"name"]];
        obj.eventDesc=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"description"]];
        obj.startDate=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"start_date"]];
        obj.endDate=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"end_date"]];
        obj.active=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"active"]];
        obj.lastModified=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
        obj.eventImageURL=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"image_cover"]];
        obj.venueID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"venue_address"]];
        obj.latitude=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lat"]];
        obj.longtitude=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"long"]];
        
        [arrGetEvent addObject:obj];
    }
    arrEvents=arrGetEvent;
    
    [_tblMenu reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    [_tblMenu selectRowAtIndexPath:indexPath
                          animated:YES
                    scrollPosition:UITableViewScrollPositionNone];
    [self tableView:_tblMenu didSelectRowAtIndexPath:indexPath ];
}

-(void)getDownloaded:(NSMutableArray *)events andSession:(NSMutableArray *)session{
    NSLog(@"stuff is %@",events);
    NSLog(@"session is %@",session);
    arrEvents=[NSMutableArray array];
    arrEvents = events;
    NSLog(@"events array=%@",arrEvents);
    
    [_tblMenu reloadData];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    [_tblMenu selectRowAtIndexPath:indexPath
                          animated:YES
                    scrollPosition:UITableViewScrollPositionNone];
    [self tableView:_tblMenu didSelectRowAtIndexPath:indexPath ];
  
    
}


- (void)viewDidAppear:(BOOL)animated {
[super viewDidAppear:animated=NO];
       [self.revealViewController.frontViewController.view addSubview:overlayview];
         [overlayview addGestureRecognizer:self.revealViewController.panGestureRecognizer];
         [overlayview addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    
    
   
  
    long num=[[NSUserDefaults standardUserDefaults] integerForKey:@"EventID"];
  
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:num inSection:1];
        [_tblMenu selectRowAtIndexPath:indexPath
                              animated:YES
                        scrollPosition:UITableViewScrollPositionNone];
        [self tableView:_tblMenu didSelectRowAtIndexPath:indexPath ];
    
   
    
    
    
}

-(void)viewWillDisappear:(BOOL)animated{

    [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    overlayview.removeFromSuperview;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"profile"])
    {
        // Get reference to the destination view controller
         MyProfileViewController *vc = [segue destinationViewController];
        
        
      
        // Pass any objects to the view controller here, like...
        [vc setUserObject:currentUserInfo];
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0){
        return 1;
    }
    if(section==1){
        NSLog(@"number of evenst in table %lu",(unsigned long)arrEvents.count);
        return arrEvents.count;
    }
    if(section==2){
        return 1;
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Constants *cObj=[[Constants alloc]init];
    if (indexPath.section==0){
        UserProTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserProCell" forIndexPath:indexPath];
        
        cell.lblUserWelcome.text=[NSString stringWithFormat:@"Welcome %@ %@",currentUserInfo.FirstName,currentUserInfo.LastName];
        NSString *urlpath=[NSString stringWithFormat:@"http://%@/%@",cObj.endpoint,currentUserInfo.Picture];
//        NSLog(@"%@",currentUserInfo.Picture);
        NSURL *url = [NSURL URLWithString:urlpath];
        //            NSData *data = [NSData dataWithContentsOfURL:url];
        
        //                UIImage *img = [[UIImage alloc] initWithData:data];
        NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
        if(![Username isEqualToString:@"Guest"]){
        [cell.ImgUserProPic hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"LoadingImage.jpg"]];
        }
        else{
            cell.ImgUserProPic.image=[UIImage imageNamed:currentUserInfo.Picture];
        }
          return cell;
    }

//    if(indexPath.row==1){
//    cell= [tableView dequeueReusableCellWithIdentifier:@"UserProCell" forIndexPath:indexPath];
//    }
    if(indexPath.section==1){
       EventListTableViewCell  *cell= [tableView dequeueReusableCellWithIdentifier:@"EventListCell" forIndexPath:indexPath];
        Dummy *obj =[[Dummy alloc]init];
        obj=[arrEvents objectAtIndex:indexPath.row];
        cell.lblEventID.text=[[NSString alloc]initWithFormat:@"%@",obj.eventName];
        cell.lblEventDate.text=[[NSString alloc]initWithFormat:@"%@",obj.startDate];
        
        return cell;
        
    }
    if(indexPath.section==2){
        UITableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:@"LogOutCell" forIndexPath:indexPath];
          return cell;
    }
    
    return 0;
}

-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy'T'HH:mm:ss.SSSZ"];
    
    return [dateFormatter dateFromString:StringDate];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row==0 && indexPath.section==0){
        NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
        if(![Username isEqualToString:@"Guest"]){
        [self performSegueWithIdentifier:@"profile" sender:self];
        }
        else{
            [self showsUIAlertWithMessage:@"Sorry. Please login to access this feature." andTitle:@"Alert-Restricted Access"];
//            [self performSelector:@selector(MoveToEvents) withObject:nil afterDelay:1];
        }
    }
    eventID=(long)indexPath.row;
    NSLog(@"eventId ==>> %ld",eventID);
    [[NSUserDefaults standardUserDefaults] setInteger:eventID forKey:@"EventChange"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==1){
        return 70;
    }
    
    return UITableViewAutomaticDimension;
}


- (IBAction)btnLogOut:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"EventName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"EventID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"EventChange"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"LoginToken"];
    [self performSegueWithIdentifier:@"LogOut" sender:self];
}

-(void)setUserInfo{
    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    if([Username isEqualToString:@"Guest"]){
    
        Users *obj =[[Users alloc]init];
        obj.FirstName=@"Guest";
        obj.LastName=@"User";
        obj.Picture=@"FIA Logo HD.png";
        currentUserInfo=obj;
        
    }
    else{
    NSString *query = [NSString stringWithFormat:@"select * from tblCurrentUser where active=='1'"];
    
    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
    NSLog(@"db elements %@",dbresults);
    
    
    if(dbresults.count>0){
    Users *obj =[[Users alloc]init];
    int i=0;
    obj.FirstName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"first_name"]];
    obj.LastName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"last_name"]];
    obj.Active=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"active"]];
    obj.LastModified=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
    obj.CompanyName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"company_name"]];
    obj.Designation=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"designation"]];
    obj.Picture=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"picture"]];
    obj.Email=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"email"]];
    obj.Phone=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"phone"]];
    obj.Gender=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"gender"]];
    obj.Qualificaton=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"qualification"]];
    obj.UserID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"id"]];
    
    currentUserInfo=obj;
            }
    }
}
- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}



@end
