//
//  main.m
//  EventsApp
//
//  Created by Vinay Prasad on 19/9/18.
//  Copyright © 2018 iTvTi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
