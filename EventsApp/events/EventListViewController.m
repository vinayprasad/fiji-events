//
//  EventListViewController.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 02/08/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "EventListViewController.h"
#import "EventJsonDownload.h"
#import "Dummy.h"
#import "EventsTableViewCell.h"
#import "Constants.h"
#import <Haneke.h>
#import "EnterCodeViewController.h"
#import "EventsLoginViewController.h"


@interface EventListViewController (){
    NSMutableArray *arrevents;
    UIRefreshControl *refreshControl;
}


@end

@implementation EventListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    EventJsonDownload *eventDownloaded = [[EventJsonDownload alloc]init];
    [eventDownloaded setDelegate:(id)self];
    
    [eventDownloaded OrganizeDataFromJsonToObject];
    
    Constants *obj=[[Constants alloc]init];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:obj.dbName];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [_tblEventList addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    [self setData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refreshTable{
    [_tblEventList reloadData];
    [refreshControl endRefreshing];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return arrevents.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    Dummy *obj=[[Dummy alloc]init];
    Constants *cObj=[[Constants alloc]init];
    obj=[arrevents objectAtIndex:indexPath.row];
    EventsTableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell"];
    cell.tag=indexPath.row;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
                          if (cell.tag ==indexPath.row) {
            
            NSString *urlpath=[NSString stringWithFormat:@"http://%@/%@",cObj.endpoint,obj.eventImageURL];
            NSURL *url = [NSURL URLWithString:urlpath];
            //            NSData *data = [NSData dataWithContentsOfURL:url];
            
            //                UIImage *img = [[UIImage alloc] initWithData:data];
            [cell.imgEventBanner hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"LoadingImage.jpg"]];
                        }
        });
    });
//    cell.imgEventBanner.image
    
//    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
//    dispatch_async(queue, ^{
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//                          if (cell.tag ==indexPath.row) {
//
//                        NSString *urlpath=[NSString stringWithFormat:@"http://%@/%@",cObj.endpoint,obj.eventImageURL];
//
//                        NSURL *url = [NSURL URLWithString:urlpath];
//                        NSData *data = [NSData dataWithContentsOfURL:url];
//
//                        cell.imgEventBanner.image = [[UIImage alloc] initWithData:data];
//
//                        }
//        });
//    });
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.view.frame.size.height/3;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)getDownloaded:(NSMutableArray *)events andSession:(NSMutableArray *)session{
      
          
            //        [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"EventChange"]
            //        NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1' and id==%ld",(long)[eventNum integerValue]];
    
}

-(void)setData{
    NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1'"];
    NSArray *dbresults =[[NSArray alloc]init];
    dbresults =[self.dbManager loadDataFromDB:query];
    NSLog(@"db elements %@",dbresults);
    
    NSMutableArray * arrGetEvent=[[NSMutableArray alloc]init];
    arrevents=[[NSMutableArray alloc]init];
    if(dbresults.count>0){
        for(int i=0;i<dbresults.count;i++){
            Dummy *obj =[[Dummy alloc]init];
            obj.eventID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"id"]];
            obj.eventName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"name"]];
            obj.eventDesc=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"description"]];
            obj.startDate=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"start_date"]];
            obj.endDate=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"end_date"]];
            obj.active=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"active"]];
            obj.lastModified=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
            obj.eventImageURL=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"image_cover"]];
            obj.venueID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"venue_address"]];
            obj.latitude=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lat"]];
            obj.longtitude=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"long"]];
            
            [arrGetEvent addObject:obj];
        }
    }
    
    arrevents=arrGetEvent;
    
    
    [_tblEventList reloadData];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Row selected is : %ld",(long)indexPath.row+1);
//    Dummy *eObj=[arrevents objectAtIndex:indexPath.row];
    
    [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"EventChange"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self performSegueWithIdentifier:@"newstart" sender:self];
}
- (IBAction)btnEnterCode:(id)sender {
//    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"test"];
////    vc.preferredContentSize=CGSizeMake(367, 344);
//    vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
////    vc.modalTransitionStyle = UIModalPresentationNone;
//    [self presentViewController:vc animated:YES completion:nil];
    
    [self performSegueWithIdentifier:@"LoginScreen" sender:nil];
    
}

-(void)viewWillAppear:(BOOL)animated{
//    [_tblEventList reloadData];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
//    if(section==0){
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 0)];
//        return view;
//    }
    if(section==0){
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
        view.backgroundColor=[UIColor whiteColor];
        /* Create custom view to display section header... */
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 20)];
        [label setFont:[UIFont boldSystemFontOfSize:17]];
        label.textColor=[UIColor blueColor];
                 [label setTextAlignment:NSTextAlignmentCenter];
//                label.textAlignment = NSTextAlignmentCenter;
        NSString *string =@"Public Events";
        /* Section header is in 0th index... */
        [label setText:string];
        [view addSubview:label];
//        [view setBackgroundColor:[UIColor colorWithRed:45/255.0 green:107/255.0 blue:139/255.0 alpha:1.0]]; //your background color...
        return view;
    }
    if(section==2){
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
        /* Create custom view to display section header... */
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 20)];
        [label setFont:[UIFont boldSystemFontOfSize:17]];
        label.textColor=[UIColor whiteColor];
        //         label.textAlignment = NSTextAlignmentCenter;
        NSString *string =@"Notifications";
        /* Section header is in 0th index... */
        [label setText:string];
        [view addSubview:label];
        [view setBackgroundColor:[UIColor colorWithRed:45/255.0 green:107/255.0 blue:139/255.0 alpha:1.0]];//your background color...
        return view;
    }
    return 0;
}





@end
