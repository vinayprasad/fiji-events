//
//  EventsTableViewCell.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 03/08/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgEventBanner;

@end
