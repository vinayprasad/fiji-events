//
//  SectionCollectionReusableView.h
//  FIAEventsApp
//
//  Created by Ashivan on 8/28/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionCollectionReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *lblHeadername;

@end
