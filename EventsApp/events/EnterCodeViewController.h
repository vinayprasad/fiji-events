//
//  EnterCodeViewController.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 15/08/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface EnterCodeViewController : UIViewController<UIAlertViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) DBManager *dbManager;
@property (strong,nonatomic) NSString *datapath;
@property (nonatomic) sqlite3 *DB;
@property (weak, nonatomic) IBOutlet UICollectionView *collEventBanner;

@end
