//
//  BannerCollectionViewCell.h
//  FIAEventsApp
//
//  Created by Ashivan on 8/28/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannerCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgBanner;
@property (weak, nonatomic) IBOutlet UILabel *lblEventBannerName;

@end
