//
//  EventsLoginViewController.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 02/08/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "EventsLoginViewController.h"
#import "Constants.h"
#import "Users.h"
#import "UserInfoDownload.h"
#import "Attendees.h"
#import "Events.h"
#import "EventJsonDownload.h"
#import "AttendeesJsonDownload.h"
#import "Speakers.h"
#import "SpeakersJSONDownload.h"
#import "Users.h"
#import "UserInfoDownload.h"
#import "Sponsors.h"
#import "SponsorJsonDownload.h"
#import "DBManager.h"
#import "Users.h"
#import "Session Times.h"
#import "Dummy.h"
#import "Reachability.h"

@interface EventsLoginViewController ()
    {
        NSMutableArray *jsonArray;
        NSDictionary *JSON;
        BOOL check;
        NSString *username;
        NSString *password;
        BOOL checkAttendees;
        float frameSizeOriginal;
        float frameKeyboardDistance;
        float keyboardheightOut;
        BOOL checkFirstResponder;
        
        NSString *checkLastModifiedAttendees;
        NSString *checkLastModifiedUser;
        NSString *checkLastModifiedEvent;
        NSString *checkLastModifiedSession;
        NSString *checkLastModifiedSponsor;
        NSString *checkLastModifiedSpeaker;
        
        bool checkBool;
        bool checkFirstLogin;
        bool MovedToEvents;
        
        //counter for timeout of api
        int CountApiCalls;
        
        //Arrays For Delegates Event, Attendees, Users, Speakers,Sponsors
        NSMutableArray *arrAttendeesSQL;
        NSMutableArray *arrEventsSQL;
        NSMutableArray *arrSessionsSQL;
        NSMutableArray *arrSponsorsSQL;
        NSMutableArray *arrSpeakersSQL;
        NSMutableArray *arrUsersInfo;
        
        //CheckdownlaodedData
        bool isEventDownloaded;
        bool isAttendeesDownloaded;
        bool isSpeakersDownloaded;
        bool isSponsorsDownloaded;
        
        //data set
        bool isEventDownloadedDB;
        bool isAttendeesDownloadedDB;
        bool isSpeakersDownloadedDB;
        bool isSponsorsDownloadedDB;
    }

@property (strong, nonatomic) IBOutlet UIView *overlayView;
@property (strong, nonatomic) IBOutlet UIImageView *imgitvtiLogo;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;

@end

@implementation EventsLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // copy start
    checkFirstLogin=false;
    
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected)];
    singleTap.numberOfTapsRequired = 1;
    [_imgitvtiLogo setUserInteractionEnabled:YES];
    [_imgitvtiLogo addGestureRecognizer:singleTap];
    
    //bool to check if downlaod was completed
    isEventDownloaded=false;
    isAttendeesDownloaded=false;
    isSpeakersDownloaded=false;
    isSponsorsDownloaded=false;
    
    isEventDownloadedDB=false;
    isAttendeesDownloadedDB=false;
    isSpeakersDownloadedDB=false;
    isSponsorsDownloadedDB=false;
    
    //initalise array components
    //    arrAttendees=[[NSMutableArray alloc]init];
    //    arrEvents=[[NSMutableArray alloc]init];
    //    arrSessions=[[NSMutableArray alloc]init];
    //    arrSponsors=[[NSMutableArray alloc]init];
    //    arrSpeakers=[[NSMutableArray alloc]init];
    //    arrUsersInfo=[[NSMutableArray alloc]init];
    
    //initialse timout counter
    CountApiCalls=0;
    
    NSString * nothing;
    [[NSUserDefaults standardUserDefaults] setValue:nothing forKey:@"LastModified-Attendee-v1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:nothing forKey:@"LastModified-Userv1"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:nothing forKey:@"LastModified-Session"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:nothing forKey:@"LastModified-Event"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:nothing forKey:@"LastModified-Sponsor"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setValue:nothing forKey:@"LastModified-Speaker"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    
    
    // Do any additional setup after loading the view.
    //database components
    Constants *obj=[[Constants alloc]init];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:obj.dbName];
    //database end
    
    _overlayView.hidden=YES;
    _actIndicator.hidden=YES;
    frameSizeOriginal=self.view.frame.size.height;
    frameKeyboardDistance=self.view.frame.size.height-110;
    
    //    checkAttendees=FALSE;
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"EventName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"EventID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"EventChange"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //     [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"LoginToken"];
    [self someMethodWhereYouSetUpYourObserver];
    
    //    [self testInternetConnection];
    [self isLoggedIn];
    
//    [self startOperation];
    // copy end
    // Do any additional setup after loading the view.
    [_txtUsername.layer setCornerRadius:20.0f];
    
    // border
    [_txtUsername.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_txtUsername.layer setBorderWidth:0.9f];
    _txtUsername.clipsToBounds=YES;
    
    [_txtPassword.layer setCornerRadius:20.0f];
    
    // border
    [_txtPassword.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_txtPassword.layer setBorderWidth:0.9f];
    _txtPassword.clipsToBounds=YES;
    
    [_btnSignIN.layer setCornerRadius:20.0f];
    
    // border
    [_btnSignIN.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_btnSignIN.layer setBorderWidth:0.9f];
    _btnSignIN.clipsToBounds=YES;
    
    [GIDSignIn sharedInstance].uiDelegate = self;
    [GIDSignIn sharedInstance].delegate=self;
    
//    [[GIDSignIn sharedInstance] signInSilently];
//    _overlayView.hidden=NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark-Google Sign In
- (IBAction)btnGoogleSignIn:(id)sender {
    _overlayView.hidden=NO;
    [[GIDSignIn sharedInstance] signIn];
    
    
}

-(void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error{
//    _actIndicator.hidden=NO;
    //    NSLog(@"User is already signed in %@",user);
    @try{
    if(!error){
        if([[GIDSignIn sharedInstance]currentUser]!=nil){
//            [self showsUIAlertWithMessage:@"Welcome To The Jobs App" andTitle:[NSString stringWithFormat:@"Hello!! %@",user.profile.email]];
            _overlayView.hidden=YES;
            [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"DidGoogleSignIn"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self showAlertLogin:[NSString stringWithFormat:@"You Are Signed in as %@. Would You Like to Continue? . Press Cancel to Log In With New Account.",user.profile.name] andTitle:@"Information"];
        }
        
        
        else{
            
            [self showsUIAlertWithMessage:@"Please Login in and try again" andTitle:@"Hmmn!! Wonder What Went Wrong?"];
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"DidGoogleSignIn"];
            [[NSUserDefaults standardUserDefaults] synchronize];
           _overlayView.hidden=YES;
                    [[GIDSignIn sharedInstance]signOut];
        }
    }
    else{
        [self showsUIAlertWithMessage:@"Ooops!! Error While Login in.Please try again" andTitle:@"Oh No !!  Something Happended"];
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"DidGoogleSignIn"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        _overlayView.hidden=YES;
    }
    
    } @catch (NSException *e) {
        NSLog(@"error is %@",e);
    }
    
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnLogOut:(id)sender {
    [[GIDSignIn sharedInstance]signOut];
}

#pragma mark-Set Keyboard height
-(void)tapDetected{
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        [self showsUIAlertWithMessageSettings:@"Oops!! No Internet Connection. Please connect and try again." andTitle:@"😎- WhatsThis??"];
    }
    else{
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.itvti.com.fj"]];
    }
    
}
- (void)someMethodWhereYouSetUpYourObserver
{
    // This could be in an init method.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myNotificationMethod:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
}

- (void)myNotificationMethod:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    float keyboardHeight=keyboardFrameBeginRect.size.height;
    keyboardheightOut=keyboardHeight;
    frameKeyboardDistance=frameSizeOriginal-keyboardHeight;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    [self.view.layer removeAllAnimations];
    
    [self.view setFrame:CGRectMake(0,-100,self.view.frame.size.width,self.view.frame.size.height)];
    
    return YES;
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if([_txtPassword isFirstResponder] && ([touch view] != _txtPassword)){
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,frameSizeOriginal)];
        [_txtPassword resignFirstResponder];
        checkFirstResponder=TRUE;
        
    }
    if([_txtUsername isFirstResponder] &&([touch view] != _txtUsername)){
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,frameSizeOriginal)];
        [_txtUsername resignFirstResponder];
        checkFirstResponder=TRUE;
        
    }
    
    [super touchesBegan:touches withEvent:event];
}

#pragma mark - Show Alert Box

- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}

- (void)showsUIAlertWithMessageOKSeque:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Continue",nil];
    alert.tag=2;
    [alert show];
    
}

- (void)showsUIAlertWithMessageOKSeque2:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Continue",nil];
    alert.tag=6;
    [alert show];
    
}

- (void)showsUIAlertWithMessageGuest:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings", nil];
    [alert show];
    
}

- (void)showsUIAlertWithMessageSettings:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings", nil];
    alert.tag=3;
    [alert show];
    
}

- (void)showAlertRetry:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry", nil];
    alert.tag=5;
    [alert show];
    
}

- (void)showAlertLogin:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Continue", nil];
    alert.tag=7;
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    @try{
        if (buttonIndex == 0){
            NSLog(@"Cancel button clicked");
            if(alertView.tag==7){
                [[GIDSignIn sharedInstance] signOut];
            }
        }
        if (buttonIndex == 1){
            if(alertView.tag==2){
                _overlayView.hidden=NO;
                //            [self testInternetConnection];
                //            [self MoveToEvents];
                if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                {
                    NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1'"];
                    
                    //    //    // Execute the query.
                    
                    NSArray *dbresultsEvents =[self.dbManager loadDataFromDB:query];
                    
                    if(dbresultsEvents.count>0){
                        _overlayView.hidden=YES;
                        [self performSegueWithIdentifier:@"MainPage" sender:self];
                    }
                    else{
                        [self showsUIAlertWithMessageGuest:@"Your Internet Just Dropped. Please connect and try again." andTitle:@"Information"];
                    }
                }
                else{
                    Reachability* reachability = [Reachability reachabilityWithHostName: @"http://ec2-54-66-220-147.ap-southeast-2.compute.amazonaws.com"];
                    NetworkStatus netStatus = [reachability currentReachabilityStatus];
                    NSLog(@"the nertwok is %ld", (long)netStatus);
                    if(netStatus==0){
                        NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1'"];
                        
                        //    //    // Execute the query.
                        
                        NSArray *dbresultsEvents =[self.dbManager loadDataFromDB:query];
                        
                        if(dbresultsEvents.count>0){
                            _overlayView.hidden=YES;
                            [self performSegueWithIdentifier:@"MainPage" sender:self];
                        }
                    }
                    else{
                        [self testInternetConnection];
                    }
                }
                
                
                
            }
            
            if (alertView.tag==3){
                NSLog(@"settings button clicked");
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-prefs:"]];
            }
            
            if(alertView.tag==5){
                _overlayView.hidden=NO;
                [self LoadDelegates];
            }
            
            if(alertView.tag==6){
                if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                {
                    NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1'"];
                    
                    //    //    // Execute the query.
                    
                    NSArray *dbresultsEvents =[self.dbManager loadDataFromDB:query];
                    
                    if(dbresultsEvents.count>0){
                        _overlayView.hidden=YES;
                        [self performSegueWithIdentifier:@"MainPage" sender:self];
                    }
                    else{
                        [self showsUIAlertWithMessage:@"Content Could not be loaded. Please connect and try again." andTitle:@"Information"];
                    }
                }
                else
                {
                    Reachability* reachability = [Reachability reachabilityWithHostName: @"www.google.com"];
                    NetworkStatus netStatus = [reachability currentReachabilityStatus];
                    NSLog(@"the nertwok is %ld", (long)netStatus);
                    
                    NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1'"];
                    
                    //    //    // Execute the query.
                    
                    NSArray *dbresultsEvents =[self.dbManager loadDataFromDB:query];
                    
                    if(dbresultsEvents.count>0){
                        _overlayView.hidden=YES;
                        [self performSegueWithIdentifier:@"MainPage" sender:self];
                    }
                    else{
                        [self showsUIAlertWithMessage:@"Content Could not be loaded. Please connect and try again." andTitle:@"Information"];
                    }
                    
                    
                }
                
            }
            
            if(alertView.tag==7){
                [self StartDownload];
            }
        }
    }
    @catch(NSException *e){
        _overlayView.hidden=YES;
        [self showsUIAlertWithMessage:@"Oops!! Something Went Wrong. Please Check Internet Connection and try again." andTitle:@"Alert"];
    }
}

#pragma mark -View Settings
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear: animated];
    isEventDownloaded=false;
    isAttendeesDownloaded=false;
    isSpeakersDownloaded=false;
    isSponsorsDownloaded=false;
}

#pragma mark- Navigation Functions
- (IBAction)LogIn:(id)sender {
    
    @try {
        if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        {
            [self showsUIAlertWithMessageSettings:@"There is no internet connection. Please connect and try again or continue as Guest" andTitle:@"Error"];
        }
        else
        {
            //            [self testInternetConnection];
            NSLog(@"login button clicked");
            _overlayView.hidden=NO;
            _actIndicator.hidden=NO;
            [_actIndicator startAnimating];
            MovedToEvents=false;
            isAttendeesDownloaded=false;
            isEventDownloaded=false;
            isSponsorsDownloaded=false;
            isSpeakersDownloaded=false;
            username= [_txtUsername.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            password= [_txtPassword.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            //[self LoginForUser:username andpassword:password];
            Constants *cObj=[[Constants alloc]init];
            //            NSLog(@"ip adddress is :%@",cObj.endpoint);
            
            NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/rest/checkuser",cObj.endpoint]]];
            
            NSString *userUpdate =[NSString stringWithFormat:@"username=%@&password=%@", username,password];
            [[NSUserDefaults standardUserDefaults] setValue:username forKey:@"Username"];
            [[NSUserDefaults standardUserDefaults] setValue:password forKey:@"Password"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            //create the Method "GET" or "POST"
            [urlRequest setHTTPMethod:@"POST"];
            
            [urlRequest setValue:@"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd" forHTTPHeaderField:@"Authorization"];
            //Convert the String to Data
            NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
            
            //Apply the data to the body
            [urlRequest setHTTPBody:data1];
            
            NSURLSession *session = [NSURLSession sharedSession];
            NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                //                NSLog(@"Response =%@",response);
                if(httpResponse.statusCode == 200)
                {
                    NSError *parseError = nil;
                    NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                    //                    NSLog(@"The response is - %@",responseDictionary);
                    NSDictionary *success = [responseDictionary objectForKey:@"data"];
                    NSString *isSuccess = [responseDictionary objectForKey:@"status"];
                    //                    NSLog(@"The success is - %@",success);
                    if([isSuccess isEqualToString:@"true"])
                    {
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_actIndicator stopAnimating];
                            //                            _overLayView.hidden=YES;
                            for(NSDictionary *successObj in success){
                                Users *obj=[[Users alloc]init];
                                [[NSUserDefaults standardUserDefaults] setValue:[successObj objectForKey:@"id"] forKey:@"UserID"];
                                [[NSUserDefaults standardUserDefaults] synchronize];
                                
                                NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
                                dayComponent.day = 1;
                                
                                NSCalendar *theCalendar = [NSCalendar currentCalendar];
                                NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:[NSDate date] options:0];
                                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                                [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm aa"];
                                NSString *SessionDate=[dateFormatter stringFromDate:nextDate];
                                
                                //session token
                                [[NSUserDefaults standardUserDefaults] setValue:SessionDate forKey:@"LoginToken-Date"];
                                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"LoginToken"];
                                
                                obj.UserID=[successObj objectForKey:@"id"];
                                obj.FirstName=[successObj objectForKey:@"first_name"];
                                obj.LastName=[successObj objectForKey:@"last_name"];
                                obj.Email=[successObj objectForKey:@"email"];
                                obj.Phone=[successObj objectForKey:@"phone"];
                                obj.Picture=[successObj objectForKey:@"picture"];
                                obj.Gender=[successObj objectForKey:@"gender"];
                                obj.CompanyName=[successObj objectForKey:@"company_name"];
                                obj.Designation=[successObj objectForKey:@"designation"];
                                obj.Qualificaton=[successObj objectForKey:@"qualification"];
                                obj.Active=[successObj objectForKey:@"active"];
                                obj.LastModified=[successObj objectForKey:@"lastmodified"];
                                
                                @try {
                                    checkLastModifiedUser=[[NSUserDefaults standardUserDefaults] stringForKey:@"LastModified-Userv1"];
                                    
                                    NSString *clearquery = [NSString stringWithFormat:@"delete from tblCurrentUser"];
                                    //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
                                    
                                    [self.dbManager executeQuery:clearquery];
                                    
                                    
                                    //    // If the query was successfully executed then pop the view controller.
                                    if (self.dbManager.affectedRows != 0) {
                                        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                                    }
                                    else{
                                        NSLog(@"Could not execute the query.");
                                    }
                                    if(![obj.FirstName isEqualToString:@""]){
                                        NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblCurrentUser (first_name,last_name,email,phone,picture,gender,company_name,designation,qualification,active,lastmodified,id) Values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",obj.FirstName,obj.LastName,obj.Email,obj.Phone,obj.Picture,obj.Gender,obj.CompanyName,obj.Designation,obj.Qualificaton,obj.Active,obj.LastModified,obj.UserID];
                                        
                                        
                                        [self.dbManager executeQuery:query];
                                    }
                                    
                                    //    // If the query was successfully executed then pop the view controller.
                                    if (self.dbManager.affectedRows != 0) {
                                        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                                    }
                                    else{
                                        NSLog(@"Could not execute the query.");
                                    }
                                    
                                    NSString *newquery = [NSString stringWithFormat:@"select * from tblCurrentUser where active=='1' order by lastmodified"];
                                    
                                    //    //    // Execute the query.
                                    
                                    NSArray *dbresults =[self.dbManager loadDataFromDB:newquery];
                                    if(dbresults.count>0){
                                        //    NSLog(@"db elements %@",dbresults);
                                        
                                        
                                        //            checkLastModifiedUser=[[dbresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
                                        //            [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedUser forKey:@"LastModified-Userv1"];
                                        //            [[NSUserDefaults standardUserDefaults] synchronize];
                                    }
                                    
                                    //    else{
                                    //        NSString *compDateLastModifiedGlobal=checkLastModifiedUser;
                                    //        NSString *compDateLastModifiedDB;
                                    //        NSString *query = [NSString stringWithFormat:@"select * from tblCurrentUser where active=='1' order by lastmodified DESC"];
                                    //
                                    //        //    //    // Execute the query.
                                    //
                                    //        NSArray *dbresults =[self.dbManager loadDataFromDB:query];
                                    //        if(dbresults.count>0){
                                    //            //    NSLog(@"db elements %@",dbresults);
                                    //
                                    //            compDateLastModifiedDB=[[dbresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
                                    //
                                    //            if([self compareDateAndTimeIsDecending:compDateLastModifiedDB:compDateLastModifiedGlobal]==TRUE)
                                    //            {
                                    //                //    NSLog(@"db elements %@",dbresults);
                                    //
                                    //                NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblCurrentUser (first_name,last_name,email,phone,picture,gender,company_name,designation,qualification,active,lastmodified,id) Values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",userData.FirstName,userData.LastName,userData.Email,userData.Phone,userData.Picture,userData.Gender,userData.CompanyName,userData.Designation,userData.Qualificaton,userData.Active,userData.LastModified,userData.UserID];
                                    //
                                    //
                                    //                [self.dbManager executeQuery:query];
                                    //
                                    //                //    // If the query was successfully executed then pop the view controller.
                                    //                if (self.dbManager.affectedRows != 0) {
                                    //                    NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                                    //                }
                                    //                else{
                                    //                    NSLog(@"Could not execute the query.");
                                    //                }
                                    //
                                    ////                checkLastModifiedUser=compDateLastModifiedDB;
                                    ////                [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedUser forKey:@"LastModified-Userv1"];
                                    ////                [[NSUserDefaults standardUserDefaults] synchronize];
                                    //
                                    //
                                    //            }
                                    //
                                    //
                                    //        }
                                    //    }
                                } @catch (NSException *e) {
                                    _overlayView.hidden=YES;
                                    [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
                                }
                                [self getUserData:obj];
                            }
                            [self LoadDelegates];
                            checkBool=true;
                            checkFirstLogin=true;
                            check=true;
                        });
                    }
                    else
                    {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [_actIndicator stopAnimating];
                            _overlayView.hidden=YES;
                            [self showsUIAlertWithMessage:@"Your Username or Password did not match. Please try again" andTitle:@"Oops!! Login Failure "];
                            
                            
                        });
                    }
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //                        NSLog(@"Error response is %@", response);
                        [_actIndicator stopAnimating];
                        _overlayView.hidden=YES;
                        if([username length]==0 || [password length]==0){
                            [self showsUIAlertWithMessageSettings:@"Username or Password was not entered.Please Enter and Try Again" andTitle:@"Error"];
                        }
                        else{
                            [self showsUIAlertWithMessageSettings:@"There is no internet connection. Please connect and try again or continue as Guest" andTitle:@"Error"];
                        }
                        //[self performSegueWithIdentifier:@"MainPage" sender:self];
                    });
                    
                }
            }];
            [dataTask resume];
        }
    }@catch (NSException *e) {
        NSLog(@"DO something %@",e);
        _overlayView.hidden=YES;
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"LoginToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
    }
    
    
}

-(void)StartDownload{
    {
        _overlayView.hidden=NO;
        //            [self testInternetConnection];
        //            [self MoveToEvents];
        if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        {
            NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1'"];
            
            //    //    // Execute the query.
            
            NSArray *dbresultsEvents =[self.dbManager loadDataFromDB:query];
            
            if(dbresultsEvents.count>0){
                _overlayView.hidden=YES;
//                [self performSegueWithIdentifier:@"MainPage" sender:self];
                [self performSegueWithIdentifier:@"EventListView" sender:self];
            }
            else{
                [self showsUIAlertWithMessageGuest:@"Your Internet Just Dropped. Please connect and try again." andTitle:@"Information"];
            }
        }
        else{
            Reachability* reachability = [Reachability reachabilityWithHostName: @"http://ec2-54-66-220-147.ap-southeast-2.compute.amazonaws.com"];
            NetworkStatus netStatus = [reachability currentReachabilityStatus];
            NSLog(@"the nertwok is %ld", (long)netStatus);
            if(netStatus==0){
                NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1'"];
                
                //    //    // Execute the query.
                
                NSArray *dbresultsEvents =[self.dbManager loadDataFromDB:query];
                
                if(dbresultsEvents.count>0){
                    _overlayView.hidden=YES;
//                    [self performSegueWithIdentifier:@"MainPage" sender:self];
                    [self performSegueWithIdentifier:@"EventListView" sender:self];
                }
            }
            else{
                [self testInternetConnection];
            }
        }
    }
    
    
}


- (IBAction)LogInGuest:(id)sender {
//    NSDictionary *headers = @{ @"Authorization": @"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd",
//                               @"Cache-Control": @"no-cache",
//                               @"Postman-Token": @"2ef76440-1094-4b81-96d8-e4f7887c4a32" };
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://192.168.10.74/FIA_PORTAL/api/v1/rest/getallevents"]
//                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
//                                                       timeoutInterval:10.0];
//    [request setHTTPMethod:@"GET"];
//    [request setAllHTTPHeaderFields:headers];
//
//    NSURLSession *session = [NSURLSession sharedSession];
//    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
//                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                    if (error) {
//                                                        NSLog(@"%@", error);
//                                                    } else {
//                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//                                                        NSLog(@"%@", httpResponse);
//                                                    }
//                                                }];
//    [dataTask resume];
    [self StartDownload];
}

-(void)MoveToEvents{
   
    @try{
        if(isEventDownloadedDB==true&&isSpeakersDownloadedDB==true&&isSponsorsDownloadedDB==true&&isAttendeesDownloadedDB==true){
//        if(isEventDownloadedDB==true&&isSpeakersDownloadedDB==true){
            Reachability* reachability = [Reachability reachabilityWithHostName: @"www.google.com"];
            NetworkStatus netStatus = [reachability currentReachabilityStatus];
            NSLog(@"the nertwok is %ld", (long)netStatus);
            if((long)netStatus>0){
                if(MovedToEvents!=true){
//                    [self performSegueWithIdentifier:@"MainPage" sender:self];
                    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"TabsLoadedSucessfully"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
//                    [self performSegueWithIdentifier:@"EventListView" sender:self];
                    [self performSegueWithIdentifier:@"LoggedIn" sender:self];
                }
            }
            else{
                _overlayView.hidden=YES;
                [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"LoginToken"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                [self showsUIAlertWithMessage:@"Oh!No.Your internet connection is not stable. Please Check Internet connection and Try Again." andTitle:@"Alert"];
            }
        }
        
    }
    @catch(NSException *e){
        _overlayView.hidden=YES;
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"LoginToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self showsUIAlertWithMessage:@"Oh!No.Your internet connection is not stable. Please Check Internet connection and Try Again." andTitle:@"Alert"];
    }
}

-(void)CheckForAPIFailure{
    if(isEventDownloadedDB!=true||isSpeakersDownloadedDB!=true||isSponsorsDownloadedDB!=true||isAttendeesDownloadedDB!=true){
//        if(isEventDownloadedDB!=true||isSpeakersDownloadedDB!=true){
        MovedToEvents=true;
        _overlayView.hidden=YES;
        NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1'"];
        
        //    //    // Execute the query.
        
        NSArray *dbresultsEvents =[self.dbManager loadDataFromDB:query];
        
        if(dbresultsEvents.count>0){
            _overlayView.hidden=YES;
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"LoginToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self showsUIAlertWithMessageOKSeque2:@"Your Internet Connection might have dropped. Would you like to continue offline?" andTitle:@"Information"];
            
        }
        else{
            _overlayView.hidden=YES;
            [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"LoginToken"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self showsUIAlertWithMessageSettings:@"Your Internet Connection might have dropped.Please connect to the internet and try again" andTitle:@"Information"];
        }
        
    }
}

#pragma mark-Test Internet Connection
-(void)testInternetConnection{
    @try {
        checkBool=false;
        
        if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        {
            NSString * FirstTime=[[NSUserDefaults standardUserDefaults] stringForKey:@"FirstLogin"];
            if(![FirstTime isEqualToString:@"YES"]){
                [self showsUIAlertWithMessageGuest:@"Content Not Loaded.Please Connect To Internet and try again"andTitle:@"Alert"];
            }
            else{
                [self showsUIAlertWithMessage:@"No internet Connection was found. Content May Not be Updated "andTitle:@"Alert"];
            }
        }
        else
        {
            [self LoadDelegates];
            
        }
    } @catch (NSException *e) {
        _overlayView.hidden=YES;
        [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
    }
    
    
    
}

-(void)testInternetConnectionLogin{
    @try {
        checkBool=false;
        
        if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
        {
            
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self LoadDelegates];
            });
            
        }
    } @catch (NSException *e) {
        _overlayView.hidden=YES;
        [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
    }
    
    
    
}

-(void)LoadDelegates{
    @try {
        checkBool=true;
        //        checkFirstLogin=true;
        EventJsonDownload *eventDownloaded = [[EventJsonDownload alloc]init];
        [eventDownloaded setDelegate:(id)self];
        
        [eventDownloaded OrganizeDataFromJsonToObject];
        
        //    //Speakers
        SpeakersJsonDownload *SpeakersDownloads = [[SpeakersJsonDownload alloc]init];
        [SpeakersDownloads setDelegate:(id)self];
        
        [SpeakersDownloads OrganizeDataFromJsonToObject];
        //
        //    //Sponsors
        SponsorJsonDownload *SponsorsDownloads = [[SponsorJsonDownload alloc]init];
        [SponsorsDownloads setDelegate:(id)self];

        [SponsorsDownloads OrganizeDataFromJsonToObject];

        AttendeesJsonDownload *AttendeesDownloads = [[AttendeesJsonDownload alloc]init];
        [AttendeesDownloads setDelegate:(id)self];

        [AttendeesDownloads OrganizeDataFromJsonToObject];
        
        
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"FirstLogin"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [self performSelector:@selector(CheckForAPIFailure) withObject:nil afterDelay:30];
        
    } @catch (NSException *e) {
        NSLog(@"Exception caught %@",e);
        checkBool=false;
        _overlayView.hidden=YES;
        [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
    }
    
    
}

-(void)isLoggedIn{
    @try {
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *LoginToken =[[NSUserDefaults standardUserDefaults] stringForKey:@"LoginToken"];
            
            if([LoginToken isEqualToString:@"YES"]){
                
                NSString *LoginTokenDate =[[NSUserDefaults standardUserDefaults] stringForKey:@"LoginToken-Date"];
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd hh:mm aa"];
                
                NSString *currentDate= [formatter stringFromDate:[NSDate date]];
                
                if([self compareDateAndTimeIsDecending2:currentDate:LoginTokenDate]==false){
                    //        _overLayView.hidden=true;
                    
                    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                    {
                        _overlayView.hidden=NO;
                        NSString *query = [NSString stringWithFormat:@"select * from tblEvents"];
                        
                        NSArray *dbresults =[self.dbManager loadDataFromDB:query];
                        if(dbresults>0){
                            [self performSegueWithIdentifier:@"MainPage" sender:self];
                        }
                        
                        
                    }
                    else
                    {
                        _overlayView.hidden=NO;
                        NSString *query = [NSString stringWithFormat:@"select * from tblEvents"];
                        
                        NSArray *dbresults =[self.dbManager loadDataFromDB:query];
                        if(dbresults>0){
                            
                            [self performSegueWithIdentifier:@"MainPage" sender:self];
                        }
                        else{
                            [self showsUIAlertWithMessage:@"No Events Were Found.Please Try Again" andTitle:@"Alert"];
                            //                            [self performSegueWithIdentifier:@"MainPage" sender:self];
                        }
                    }
                    
                    
                    
                }
                else{
                    //         _overLayView.hidden=true;
                    [self showsUIAlertWithMessage:@"Session has expired. Please Login." andTitle:@"Information"];
                }
                
                
                
            }
        });
        
    } @catch (NSException *e) {
        NSLog(@"Ecveption caught%@",e);
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"LoginToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        _overlayView.hidden=YES;
        [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
    }
    
}

#pragma mark-Other Functions
-(void)SetAllDataInDB{
    [self MoveToEvents];
    
    
}


-(BOOL)compareDateAndTimeIsDecending:(NSString*) stringDate1 : (NSString*) stringDate2{
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    
    NSDate *date1= [formatter dateFromString:stringDate1];
    //    NSLog(@"%@",date1);
    NSDate *date2 = [formatter dateFromString:stringDate2];
    //                NSLog(@"%@",date2);
    NSComparisonResult result = [date1 compare:date2];
    if(result == NSOrderedDescending)
    {
        return TRUE;
        
    }
    else if(result == NSOrderedAscending)
    {
        //        NSLog(@"assending session ID => %@currenttime :%@  - session%@",sessionObj.sessionID, date1, date2);
        //        checkasc=i;
        
    }
    else
    {
        //        NSLog(@"session ID => %@currenttime :%@  - session%@",sessionObj.sessionID, date1, date2);
        
    }
    
    return FALSE;
}

-(BOOL)compareDateAndTimeIsDecending2:(NSString*) stringDate1 : (NSString*) stringDate2{
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm aa"];
    
    NSDate *date1= [formatter dateFromString:stringDate1];
    //    NSLog(@"%@",date1);
    NSDate *date2 = [formatter dateFromString:stringDate2];
    //                NSLog(@"%@",date2);
    NSComparisonResult result = [date1 compare:date2];
    if(result == NSOrderedDescending)
    {
        return TRUE;
        //date1>date2
        
    }
    else if(result == NSOrderedAscending)
    {
        //date2>date1
        
    }
    else
    {
        //date1=date2
        
    }
    
    return FALSE;
}

#pragma mark-Data Retrieval and Adding to DB
-(void)getAttendeesDownload:(NSMutableArray *)attendees{
    
    arrAttendeesSQL=[[NSMutableArray alloc]init];
    arrAttendeesSQL=attendees;
    isAttendeesDownloaded=true;
    //    [self SetAllDataInDB];
    
    if(isAttendeesDownloaded==true){
        @try {
            checkLastModifiedAttendees=[[NSUserDefaults standardUserDefaults] stringForKey:@"LastModified-Attendee-v1"];
            if([checkLastModifiedAttendees length]==0 && (checkBool==true)){
                NSString *clearquery = [NSString stringWithFormat:@"delete from tblAttendees"];
                //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
                
                [self.dbManager executeQuery:clearquery];
                
                
                //    // If the query was successfully executed then pop the view controller.
                if (self.dbManager.affectedRows != 0) {
                    NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                }
                else{
                    NSLog(@"Could not execute the query.");
                }
                if(arrAttendeesSQL.count>0){
                    for(Attendees *dbAttendee in arrAttendeesSQL){
                        dbAttendee.AttendeeCompany=[ dbAttendee.AttendeeCompany stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                        dbAttendee.AttendeesFirstName=[ dbAttendee.AttendeesFirstName stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                        NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblAttendees (events_id,user_id,active,lastmodified,first_name,last_name,company_name,picture) Values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",dbAttendee.AttendeeEventID,dbAttendee.attendeeID,dbAttendee.active,dbAttendee.attendeeLastModified,dbAttendee.AttendeesFirstName,dbAttendee.AttendeesLastName,dbAttendee.AttendeeCompany,dbAttendee.AttendeesPicURL];
                        //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
                        
                        [self.dbManager executeQuery:query];
                        
                        
                        //    // If the query was successfully executed then pop the view controller.
                        if (self.dbManager.affectedRows != 0) {
                            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                        }
                        else{
                            NSLog(@"Could not execute the query.");
                        }
                    }
                    
                }
                isAttendeesDownloadedDB=true;
                
                
                NSString *query = [NSString stringWithFormat:@"select * from tblAttendees where active=='1' order by lastmodified DESC"];
                
                //    //    // Execute the query.
                
                NSArray *dbresults =[self.dbManager loadDataFromDB:query];
                if(dbresults.count>0){
                    //    NSLog(@"db elements %@",dbresults);
                    
                    
                    //        checkLastModifiedAttendees=[[dbresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
                    //        [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedAttendees forKey:@"LastModified-Attendee-v1"];
                    //        [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }
            //    else{
            //        NSString *compDateLastModifiedGlobal=checkLastModifiedAttendees;
            //        NSString *compDateLastModifiedDB;
            //        NSString *query = [NSString stringWithFormat:@"select * from tblAttendees where active=='1' order by lastmodified DESC"];
            //
            //        //    //    // Execute the query.
            //
            //        NSArray *dbresults =[self.dbManager loadDataFromDB:query];
            //        if(dbresults.count>0){
            //            //    NSLog(@"db elements %@",dbresults);
            //
            //        compDateLastModifiedDB=[[dbresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
            //
            //        if([self compareDateAndTimeIsDecending:compDateLastModifiedDB:compDateLastModifiedGlobal]==TRUE)
            //            {
            //                    //    NSLog(@"db elements %@",dbresults);
            //                for(Attendees *dbAttendee in attendees){
            //                    NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblAttendees (events_id,user_id,active,lastmodified,first_name,last_name,company_name,picture) Values ('%@','%@','%@','%@','%@','%@','%@','%@')",dbAttendee.AttendeeEventID,dbAttendee.attendeeID,dbAttendee.active,dbAttendee.attendeeLastModified,dbAttendee.AttendeesFirstName,dbAttendee.AttendeesLastName,dbAttendee.AttendeeCompany,dbAttendee.AttendeesPicURL];
            //                    //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
            //
            //                    [self.dbManager executeQuery:query];
            //
            //
            //                    //    // If the query was successfully executed then pop the view controller.
            //                    if (self.dbManager.affectedRows != 0) {
            //                        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
            //                    }
            //                    else{
            //                        NSLog(@"Could not execute the query.");
            //                    }
            //                }
            ////                checkLastModifiedAttendees=compDateLastModifiedDB;
            ////                    [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedAttendees forKey:@"LastModified-Attendee-v1"];
            ////                    [[NSUserDefaults standardUserDefaults] synchronize];
            //
            //
            //            }
            //
            //
            //        }
            //    }
        } @catch (NSException *e) {
            _overlayView.hidden=YES;
            [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
        }
    }
    [self SetAllDataInDB];
    
    
}

-(void)getUserData:(Users*)userData{
    
    
}


-(void)getDownloaded:(NSMutableArray *)events andSession:(NSMutableArray *)session{
    arrSessionsSQL=[[NSMutableArray alloc]init];
    arrEventsSQL=[[NSMutableArray alloc]init];
    arrEventsSQL=events;
    arrSessionsSQL=session;
    isEventDownloaded=true;
    //    [self SetAllDataInDB];
    
    if(isEventDownloaded==true){
        @try {
            checkLastModifiedEvent=[[NSUserDefaults standardUserDefaults] stringForKey:@"LastModified-Event"];
            checkLastModifiedSession=[[NSUserDefaults standardUserDefaults] stringForKey:@"LastModified-Session"];
            if([checkLastModifiedEvent length]==0&&[checkLastModifiedSession length]==0 && (checkBool==true)){
                NSString *clearquery = [NSString stringWithFormat:@"delete from tblEvents"];
                //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
                
                [self.dbManager executeQuery:clearquery];
                
                
                //    // If the query was successfully executed then pop the view controller.
                if (self.dbManager.affectedRows != 0) {
                    NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                }
                else{
                    NSLog(@"Could not execute the query.");
                }
                if(arrEventsSQL.count>0){
                    for( Dummy *dbEvents in arrEventsSQL){
                        dbEvents.eventName=[dbEvents.eventName stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                        dbEvents.eventDesc=[ dbEvents.eventDesc stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                        NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblEvents (active,name,id,description,end_date,image_cover,lastmodified,start_date,long,lat,venue_address) Values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",dbEvents.active,dbEvents.eventName,dbEvents.eventID,dbEvents.eventDesc,dbEvents.endDate,dbEvents.eventImageURL,dbEvents.lastModified,dbEvents.startDate,dbEvents.longtitude,dbEvents.latitude,dbEvents.venueID];
                        
                        
                        [self.dbManager executeQuery:query];
                        
                        
                        //    // If the query was successfully executed then pop the view controller.
                        if (self.dbManager.affectedRows != 0) {
                            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                        }
                        else{
                            NSLog(@"Could not execute the query.");
                        }
                    }
                    
                }
                
                
                NSString *clearquery2 = [NSString stringWithFormat:@"delete from tblSessions"];
                //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
                
                [self.dbManager executeQuery:clearquery2];
                
                
                //    // If the query was successfully executed then pop the view controller.
                if (self.dbManager.affectedRows != 0) {
                    NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                }
                else{
                    NSLog(@"Could not execute the query.");
                }
                NSLog(@"Session count %ld",arrSessionsSQL.count);
                if(arrSessionsSQL.count>0){
                    for(Session_Times *dbSession in arrSessionsSQL){
                        dbSession.sessionProgramme=[dbSession.sessionProgramme stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                        dbSession.sessionSpeaker=[dbSession.sessionSpeaker stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                        NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblSessions (sessionid,start_time,end_time,program,active,lastmodified,venue_id,events_id,session_date,hasdiscussion,speakername,speakerid,isdebate) Values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",dbSession.sessionID, dbSession.sessionStartTime,dbSession.sessionEndTime,dbSession.sessionProgramme,dbSession.active,dbSession.lastmodified,dbSession.venueID,dbSession.eventID,dbSession.sessionSDate,dbSession.hasDiscussion,dbSession.sessionSpeaker,dbSession.sessionSpeakerID,dbSession.isDebatable];
                        
                        //                if([dbSession.eventID intValue]==13){
                        //                    NSLog(@"");
                        //                }
                        [self.dbManager executeQuery:query];
                        
                        //    // If the query was successfully executed then pop the view controller.
                        if (self.dbManager.affectedRows != 0) {
                            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                        }
                        else{
                            NSLog(@"Could not execute the query.");
                        }
                    }
                }
                isEventDownloadedDB=true;
                
                NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1' order by lastmodified DESC"];
                
                //    //    // Execute the query.
                
                NSArray *dbresults =[self.dbManager loadDataFromDB:query];
                if(dbresults.count>0){
                    //    NSLog(@"db elements %@",dbresults);
                    checkLastModifiedEvent=[[dbresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
                    [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedEvent forKey:@"LastModified-Event"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }
                
                //        NSString *Squery = [NSString stringWithFormat:@"select * from tblSession where active=='1' order by lastmodified DESC"];
                NSString *Squery = [NSString stringWithFormat:@"select * from tblSessions order by lastmodified DESC"];
                
                //    //    // Execute the query.
                
                NSArray *dbSresults =[self.dbManager loadDataFromDB:Squery];
                NSLog(@"dbSresults %ld",dbSresults.count);
                if(dbSresults.count>0){
                    
                    //                checkLastModifiedSession=[[dbSresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
                    //                [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedSession forKey:@"LastModified-Session"];
                    //                [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }
            
            //    else{
            //        NSString *compDateLastModifiedGlobal=checkLastModifiedEvent;
            //        NSString *compDateLastModifiedDB;
            //        NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1' order by lastmodified DESC"];
            //
            //        //    //    // Execute the query.
            //
            //        NSArray *dbresults =[self.dbManager loadDataFromDB:query];
            //        if(dbresults.count>0){
            //
            //            compDateLastModifiedDB=[[dbresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
            //
            //            if([self compareDateAndTimeIsDecending:compDateLastModifiedDB:compDateLastModifiedGlobal]==TRUE)
            //            {
            //                //    NSLog(@"db elements %@",dbresults);
            //                for( Dummy *dbEvents in events){
            //                    NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblSessions (active,name,id,description,end_date,image_cover,lastmodified,start_date,long,lat,venue_address) Values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",dbEvents.active,dbEvents.eventName,dbEvents.eventID,dbEvents.eventDesc,dbEvents.endDate,dbEvents.eventImageURL,dbEvents.lastModified,dbEvents.startDate,dbEvents.longtitude,dbEvents.latitude,dbEvents.venueID];
            //
            //
            //                    [self.dbManager executeQuery:query];
            //
            //
            //                    //    // If the query was successfully executed then pop the view controller.
            //                    if (self.dbManager.affectedRows != 0) {
            //                        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
            //                    }
            //                    else{
            //                        NSLog(@"Could not execute the query.");
            //                    }
            //                }
            //
            ////                checkLastModifiedEvent=compDateLastModifiedDB;
            ////                [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedEvent forKey:@"LastModified-Event"];
            ////                [[NSUserDefaults standardUserDefaults] synchronize];
            //
            //
            //            }
            //
            //        }
            //
            //        //Session Check
            //        compDateLastModifiedGlobal=checkLastModifiedSession;
            ////        NSString *Squery = [NSString stringWithFormat:@"select * from tblSessions where active=='1' order by lastmodified DESC"];
            //        NSString *Squery = [NSString stringWithFormat:@"select * from tblSessions order by lastmodified DESC"];
            //
            //        //    //    // Execute the query.
            //
            //        NSArray *dbSresults =[self.dbManager loadDataFromDB:Squery];
            //        if(dbSresults.count>0){
            //
            //            compDateLastModifiedDB=[[dbSresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
            //
            //            if([self compareDateAndTimeIsDecending:compDateLastModifiedDB:compDateLastModifiedGlobal]==TRUE)
            //            {
            //                //    NSLog(@"db elements %@",dbresults);
            //                for(Session_Times *dbSession in session){
            //                    NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblEvents (start_time,end_time,program,active,lastmodified,venue_id,events_id,session_date,hasdiscussion,speakername,speakerid) Values ('%@','%@','%@','%@','%@','%@','%@','%@','%@','%@','%@')",dbSession.sessionStartTime,dbSession.sessionEndTime,dbSession.sessionProgramme,dbSession.active,dbSession.lastmodified,dbSession.venueID,dbSession.eventID,dbSession.sessionDate,dbSession.hasDiscussion,dbSession.sessionSpeaker,dbSession.sessionSpeakerID];
            //
            //                    [self.dbManager executeQuery:query];
            //
            //
            //                    //    // If the query was successfully executed then pop the view controller.
            //                    if (self.dbManager.affectedRows != 0) {
            //                        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
            //                    }
            //                    else{
            //                        NSLog(@"Could not execute the query.");
            //                    }
            //                }
            //
            ////                checkLastModifiedSession=compDateLastModifiedDB;
            ////                [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedSession forKey:@"LastModified-Session"];
            ////                [[NSUserDefaults standardUserDefaults] synchronize];
            //
            //            }
            //
            //        }
            //    }
        } @catch (NSException *e) {
            _overlayView.hidden=YES;
            [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
        }
    }
    [self SetAllDataInDB];
}



-(void)getSponsorDownload:(NSMutableArray *)sponsors{
    //    arrSponsorsSQL=[[NSMutableArray alloc]init];
    arrSponsorsSQL=[[NSMutableArray alloc]initWithArray:sponsors];
    isSponsorsDownloaded=true;
    //   [self SetAllDataInDB];
    if(isSponsorsDownloaded==true){
        @try {
            checkLastModifiedSponsor=[[NSUserDefaults standardUserDefaults] stringForKey:@"LastModified-Sponsor"];
            if([checkLastModifiedSponsor length]==0&& (checkBool==true)){
                NSString *clearquery = [NSString stringWithFormat:@"delete from tblSponsors"];
                //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
                
                [self.dbManager executeQuery:clearquery];
                
                
                //    // If the query was successfully executed then pop the view controller.
                if (self.dbManager.affectedRows != 0) {
                    NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                }
                else{
                    NSLog(@"Could not execute the query.");
                }
                if(arrSponsorsSQL.count>0){
                    for(Sponsors *dbSponsor in arrSponsorsSQL){
                        dbSponsor.SponsorCompanyName=[dbSponsor.SponsorCompanyName stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                        NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblSponsors (id,companyname,logo,lastmodified,sponsortype_id,active,url,event_id) Values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",dbSponsor.SponsorID,dbSponsor.SponsorCompanyName,dbSponsor.SponsorLogoURL,dbSponsor.lastmodified,dbSponsor.SponsorTypeID,dbSponsor.SponsorActive,dbSponsor.SponsorWebURL,dbSponsor.SponsorEventID];
                        
                        
                        [self.dbManager executeQuery:query];
                        
                        
                        //    // If the query was successfully executed then pop the view controller.
                        if (self.dbManager.affectedRows != 0) {
                            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                        }
                        else{
                            NSLog(@"Could not execute the query.");
                        }
                    }
                }
                isSponsorsDownloadedDB=true;
                
                NSString *query = [NSString stringWithFormat:@"select * from tblSponsors where active=='1' order by lastmodified DESC"];
                
                //    //    // Execute the query.
                
                NSArray *dbresults =[self.dbManager loadDataFromDB:query];
                if(dbresults.count>0){
                    //    NSLog(@"db elements %@",dbresults);
                    
                    
                    //            checkLastModifiedSponsor=[[dbresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
                    //            [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedAttendees forKey:@"LastModified-Sponsor"];
                    //            [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }
            //    else{
            //        NSString *compDateLastModifiedGlobal=checkLastModifiedSponsor;
            //        NSString *compDateLastModifiedDB;
            //        NSString *query = [NSString stringWithFormat:@"select * from tblSponsors where active=='1' order by lastmodified DESC"];
            //
            //        //    //    // Execute the query.
            //
            //        NSArray *dbresults =[self.dbManager loadDataFromDB:query];
            //        if(dbresults.count>0){
            //            //    NSLog(@"db elements %@",dbresults);
            //
            //            compDateLastModifiedDB=[[dbresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
            //
            //            if([self compareDateAndTimeIsDecending:compDateLastModifiedDB:compDateLastModifiedGlobal]==TRUE)
            //            {
            //                //    NSLog(@"db elements %@",dbresults);
            //                for(Sponsors *dbSponsor in sponsors){
            //                   NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblSponsors (id,companyname,logo,lastmodified,sponsortype_id,active,url,event_id) Values ('%@','%@','%@','%@','%@','%@','%@','%@')",dbSponsor.SponsorID,dbSponsor.SponsorCompanyName,dbSponsor.SponsorLogoURL,dbSponsor.lastmodified,dbSponsor.SponsorTypeID,dbSponsor.SponsorActive,dbSponsor.SponsorWebURL,dbSponsor.SponsorEventID];
            //
            //
            //                    [self.dbManager executeQuery:query];
            //
            //
            //                    //    // If the query was successfully executed then pop the view controller.
            //                    if (self.dbManager.affectedRows != 0) {
            //                        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
            //                    }
            //                    else{
            //                        NSLog(@"Could not execute the query.");
            //                    }
            //                }
            ////                checkLastModifiedSponsor=compDateLastModifiedDB;
            ////                [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedSponsor forKey:@"LastModified-Sponsor"];
            ////                [[NSUserDefaults standardUserDefaults] synchronize];
            //
            //
            //            }
            //
            //
            //        }
            //    }
            
            
        } @catch (NSException *e) {
            _overlayView.hidden=YES;
            [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
        }
    }
    [self SetAllDataInDB];
    
}



-(void)getSpeakersDownload:(NSMutableArray *)speakers{
    arrSpeakersSQL=[[NSMutableArray alloc]init];
    arrSpeakersSQL=speakers;
    isSpeakersDownloaded=true;
    
    if(isSpeakersDownloaded==true){
        
        @try {
            checkLastModifiedSpeaker=[[NSUserDefaults standardUserDefaults] stringForKey:@"LastModified-Speaker"];
            if([checkLastModifiedSpeaker length]==0&& (checkBool==true)){
                NSString *clearquery = [NSString stringWithFormat:@"delete from tblSpeakers"];
                //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
                
                [self.dbManager executeQuery:clearquery];
                
                
                //    // If the query was successfully executed then pop the view controller.
                if (self.dbManager.affectedRows != 0) {
                    NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                }
                else{
                    NSLog(@"Could not execute the query.");
                }
                if(arrSpeakersSQL.count>0){
                    //                    dbSpeaker;
                    for(Speakers *dbSpeaker in arrSpeakersSQL){
                        dbSpeaker.speakerFirstName=[ dbSpeaker.speakerFirstName stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                        dbSpeaker.speakerCompany=[ dbSpeaker.speakerCompany stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
                        NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblSpeakers (first_name,last_name,picture,company_name,eventid,url,designation,active,lastmodified,speakerid) Values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",dbSpeaker.speakerFirstName,dbSpeaker.speakerLastName,dbSpeaker.speakerPicURL,dbSpeaker.speakerCompany,dbSpeaker.speakerEventID,dbSpeaker.speakerPDFUrl,dbSpeaker.speakerPosition,dbSpeaker.active,dbSpeaker.lastmodified, dbSpeaker.speakerID];
                        
                        [self.dbManager executeQuery:query];
                        
                        
                        //    // If the query was successfully executed then pop the view controller.
                        if (self.dbManager.affectedRows != 0) {
                            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
                        }
                        else{
                            NSLog(@"Could not execute the query.");
                        }
                    }
                }
                isSpeakersDownloadedDB=true;
                
                //        NSString *query = [NSString stringWithFormat:@"select * from tblSpeakers where active=='1' order by lastmodified DESC"];
                NSString *query = [NSString stringWithFormat:@"select * from tblSpeakers order by lastmodified DESC"];
                
                //    //    // Execute the query.
                
                NSArray *dbresults =[self.dbManager loadDataFromDB:query];
                if(dbresults.count>0){
                    //    NSLog(@"db elements %@",dbresults);
                    
                    
                    //            checkLastModifiedSpeaker=[[dbresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
                    //            [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedSpeaker forKey:@"LastModified-Speaker"];
                    //            [[NSUserDefaults standardUserDefaults] synchronize];
                }
            }
            //    else{
            //        NSString *compDateLastModifiedGlobal=checkLastModifiedSpeaker;
            //        NSString *compDateLastModifiedDB;
            ////        NSString *query = [NSString stringWithFormat:@"select * from tblSpeakers where active=='1' order by lastmodified DESC"];
            //          NSString *query = [NSString stringWithFormat:@"select * from tblSpeakers order by lastmodified DESC"];
            //
            //        //    //    // Execute the query.
            //
            //        NSArray *dbresults =[self.dbManager loadDataFromDB:query];
            //        if(dbresults.count>0){
            //            //    NSLog(@"db elements %@",dbresults);
            //
            //            compDateLastModifiedDB=[[dbresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
            //
            //            if([self compareDateAndTimeIsDecending:compDateLastModifiedDB:compDateLastModifiedGlobal]==TRUE)
            //            {
            //                //    NSLog(@"db elements %@",dbresults);
            //                for(Speakers *dbSpeaker in speakers){
            //                    NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblSpeakers (first_name,last_name,picture,company_name,eventid,url,designation,active,lastmodified,speakerid) Values ('%@','%@','%@','%@','%@','%@','%@','%@')",dbSpeaker.speakerFirstName,dbSpeaker.speakerLastName,dbSpeaker.speakerPicURL,dbSpeaker.speakerCompany,dbSpeaker.speakerEventID,dbSpeaker.speakerPDFUrl,dbSpeaker.active,dbSpeaker.speakerID];
            //
            //                    [self.dbManager executeQuery:query];
            //
            //
            //                    //    // If the query was successfully executed then pop the view controller.
            //                    if (self.dbManager.affectedRows != 0) {
            //                        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
            //                    }
            //                    else{
            //                        NSLog(@"Could not execute the query.");
            //                    }
            //                }
            ////                checkLastModifiedSpeaker=compDateLastModifiedDB;
            ////                [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedSpeaker forKey:@"LastModified-Speaker"];
            ////                [[NSUserDefaults standardUserDefaults] synchronize];
            //
            //
            //            }
            //
            //
            //        }
            //    }
        } @catch (NSException *e) {
            _overlayView.hidden=YES;
            [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
        }
    }
    [self SetAllDataInDB];
}

@end
