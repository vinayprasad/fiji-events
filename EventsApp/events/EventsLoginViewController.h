//
//  EventsLoginViewController.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 02/08/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleSignIn;
#import "DBManager.h"

@interface EventsLoginViewController : UIViewController<GIDSignInUIDelegate,GIDSignInDelegate>
@property (strong, nonatomic) IBOutlet UITextField *txtUsername;
@property (strong, nonatomic) IBOutlet UITextField *txtPassword;
@property (strong, nonatomic) IBOutlet UIButton *btnSignIN;
@property (strong, nonatomic) IBOutlet UIButton *btnForgotLogin;
@property (strong, nonatomic) IBOutlet UIButton *btnGoogleSignIn;
@property (strong, nonatomic) IBOutlet UIImageView *imgItvtiLogo;
@property (nonatomic, strong) DBManager *dbManager;

@end
