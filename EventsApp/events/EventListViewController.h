//
//  EventListViewController.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 02/08/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface EventListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) NSString *name;
@property (strong, nonatomic) IBOutlet UITableView *tblEventList;
@property (nonatomic, strong) DBManager *dbManager;
@property (strong,nonatomic) NSString *datapath;
@property (nonatomic) sqlite3 *DB;
@end
