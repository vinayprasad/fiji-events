//
//  EnterCodeViewController.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 15/08/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "EnterCodeViewController.h"
#import "Constants.h"
#import "EventListViewController.h"
#import "SectionCollectionReusableView.h"
#import "Dummy.h"
#import "BannerCollectionViewCell.h"
#import <Haneke.h>

@interface EnterCodeViewController (){
    NSMutableArray *arreventsPublic;
    NSMutableArray *arreventsPrivate;
}
@property (strong, nonatomic) IBOutlet UIView *viewDialogue;
@property (strong, nonatomic) IBOutlet UILabel *lblMsg;
@property (strong, nonatomic) IBOutlet UITextField *txtCode;

@end

@implementation EnterCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [_viewDialogue.layer setCornerRadius:17.0f];
//    // border
//    [_viewDialogue.layer setBorderColor:[UIColor whiteColor].CGColor];
//    [_viewDialogue.layer setBorderWidth:0.9f];
//    _viewDialogue.clipsToBounds=YES;
    
    Constants *obj=[[Constants alloc]init];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:obj.dbName];
    
    [self getDatafromDB];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}

-(void)showMsg{
    [self showsUIAlertWithMessage:@"Nigga! Shut the f** Up" andTitle:@"taaatitania babari baraba"];
}

-(void)checkCode:(NSString *)code{
    if([code isEqualToString:@"batcave"]){
        [self changeDataInDB];
    }
}

-(void)changeDataInDB{
    NSString *query = [NSString stringWithFormat:@"UPDATE tblEvents SET active='1' WHERE id='13'"];
//    UPDATE table_name SET active='1' WHERE events_id='13'
    
    [self.dbManager executeQuery:query];
    
    
    //    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
         [self showMsg];
    }
    else{
        NSLog(@"Could not execute the query.");
    }
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    EventListViewController *vc = [sb instantiateViewControllerWithIdentifier:@"eventlist"];
    [vc.tblEventList reloadData];
}

-(void)getDatafromDB{
//    NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1'"];
    NSString *query = [NSString stringWithFormat:@"select * from tblEvents"];
    NSArray *dbresults =[[NSArray alloc]init];
    dbresults =[self.dbManager loadDataFromDB:query];
    NSLog(@"db elements %@",dbresults);
    
    NSMutableArray * arrGetEventPublic=[[NSMutableArray alloc]init];
    NSMutableArray * arrGetEventPrivate=[[NSMutableArray alloc]init];
    arreventsPublic=[[NSMutableArray alloc]init];
    arreventsPrivate=[[NSMutableArray alloc]init];
    
    if(dbresults.count>0){
        for(int i=0;i<dbresults.count;i++){
            Dummy *obj =[[Dummy alloc]init];
            obj.eventID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"id"]];
            obj.eventName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"name"]];
            obj.eventDesc=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"description"]];
            obj.startDate=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"start_date"]];
            obj.endDate=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"end_date"]];
            obj.active=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"active"]];
            obj.lastModified=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
            obj.eventImageURL=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"image_cover"]];
            obj.venueID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"venue_address"]];
            obj.latitude=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lat"]];
            obj.longtitude=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"long"]];
            if([obj.active intValue]==0){
                [arrGetEventPrivate addObject:obj];
            }
            else{
                [arrGetEventPublic addObject:obj];
            }
        }
    }
    
    arreventsPublic=arrGetEventPublic;
    arreventsPrivate=arrGetEventPrivate;
    [_collEventBanner reloadData];
    
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    
    if([_txtCode isFirstResponder] &&([touch view] != _txtCode)){
       
        [_txtCode resignFirstResponder];
        
    }
    
    [super touchesBegan:touches withEvent:event];
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(section==0){
        return arreventsPrivate.count;
    }
    
    if(section==1){
        return arreventsPublic.count;
    }
    return 0;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section==0){
        Dummy *obj=[[Dummy alloc]init];
        Constants *cObj=[[Constants alloc]init];
        NSLog(@"%@",arreventsPrivate);
        obj=[arreventsPrivate objectAtIndex:indexPath.row];
        
        BannerCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"Banner" forIndexPath:indexPath];
        cell.lblEventBannerName.text=obj.eventName;
        cell.tag=indexPath.row;
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (cell.tag ==indexPath.row) {
                    
                    NSString *urlpath=[NSString stringWithFormat:@"http://%@/%@",cObj.endpoint,obj.eventImageURL];
                    NSURL *url = [NSURL URLWithString:urlpath];
                    //            NSData *data = [NSData dataWithContentsOfURL:url];
                    
                    //                UIImage *img = [[UIImage alloc] initWithData:data];
                    [cell.imgBanner hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"LoadingImage.jpg"]];
                }
            });
        });
        return cell;
    }
    
    if(indexPath.section==1){
        Dummy *obj=[[Dummy alloc]init];
        Constants *cObj=[[Constants alloc]init];
        obj=[arreventsPublic objectAtIndex:indexPath.row];
        
        BannerCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"Banner" forIndexPath:indexPath];
        cell.lblEventBannerName.text=obj.eventName;
        cell.tag=indexPath.row;
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if (cell.tag ==indexPath.row) {
                    
                    NSString *urlpath=[NSString stringWithFormat:@"http://%@/%@",cObj.endpoint,obj.eventImageURL];
                    NSURL *url = [NSURL URLWithString:urlpath];
                    //            NSData *data = [NSData dataWithContentsOfURL:url];
                    
                    //                UIImage *img = [[UIImage alloc] initWithData:data];
                    [cell.imgBanner hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"LoadingImage.jpg"]];
                }
            });
        });
        return cell;
    }
    
    return 0;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        if(indexPath.section==0){
            SectionCollectionReusableView *headerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"sectionHeader" forIndexPath:indexPath];
            headerview.lblHeadername.text=@"My Events";
            reusableview = headerview;
        }
        
        if (indexPath.section==1){
            SectionCollectionReusableView *headerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"sectionHeader" forIndexPath:indexPath];
            headerview.lblHeadername.text=@"Public Events";
            reusableview = headerview;
        }
    }
    
    
    
    return reusableview;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"EventChange"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self performSegueWithIdentifier:@"mainPage" sender:self];
}
-(void)viewDidAppear:(BOOL)animated{
    [[self navigationController] setNavigationBarHidden:YES animated:YES];
}
@end
