//
//  LogOutTableViewCell.h
//  FIAEventsApp
//
//  Created by temp on 07/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogOutTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnNotification;
@property (weak, nonatomic) IBOutlet UILabel *lblNotificationTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblNotificationMsg;
@property (weak, nonatomic) IBOutlet UILabel *lblDaysAgo;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *actLoading;

@end
