//
//  UserProTableViewCell.m
//  FIAEventsApp
//
//  Created by temp on 07/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "UserProTableViewCell.h"

@implementation UserProTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
        [_ImgUserProPic.layer setCornerRadius:_ImgUserProPic.frame.size.width/2];
        _ImgUserProPic.layer.borderWidth=4;
        _ImgUserProPic.layer.masksToBounds = YES;
        _ImgUserProPic.clipsToBounds=YES;
        [_ImgUserProPic.layer setBorderColor:[UIColor colorWithRed:45/255.0 green:107/255.0 blue:139/255.0 alpha:1.0].CGColor];
    [_actIndicator startAnimating];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
