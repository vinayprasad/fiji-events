//
//  EventListTableViewCell.h
//  FIAEventsApp
//
//  Created by temp on 07/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventListTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblEventID;
@property (strong, nonatomic) IBOutlet UILabel *lblEventDate;

@end
