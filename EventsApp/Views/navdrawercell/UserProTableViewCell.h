//
//  UserProTableViewCell.h
//  FIAEventsApp
//
//  Created by temp on 07/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *ImgUserProPic;
@property (strong, nonatomic) IBOutlet UILabel *lblUserWelcome;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;

@end
