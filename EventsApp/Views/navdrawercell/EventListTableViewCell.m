//
//  EventListTableViewCell.m
//  FIAEventsApp
//
//  Created by temp on 07/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "EventListTableViewCell.h"

@implementation EventListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [_lblEventDate.layer setCornerRadius:_lblEventDate.frame.size.height/2];
    
    // border
    [_lblEventDate.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_lblEventDate.layer setBorderWidth:0.9f];
    _lblEventDate.clipsToBounds=YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
