//
//  SponsorViewController.m
//  FIAEventsApp
//
//  Created by temp on 08/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//
#import "QuartzCore/CALayer.h"
#import "SponsorViewController.h"
#import "SponsorDetailsViewController.h"
#import "MajorSponsorCollectionViewCell.h"
#import "MinorCollectionViewCell.h"
#import "SWRevealViewController.h"
#import "Sponsors.h"
#import "SponsorJsonDownload.h"
#import "Constants.h"
#import "CollectionReusableViewSectionHeader.h"
#import "Haneke.h"
#import "DBManager.h"
#import "Reachability.h"

@interface SponsorViewController (){
    NSMutableArray *SponsorMajor;
    NSMutableArray *SponsorMinor;
    long extraNum;
    long extraNumMajor;
    NSString *testEvent;
     NSString *EventRealID;
    SponsorJsonDownload *SponsorDownloads;
    NSMutableArray *arrSponsors;
    long EventID;
    long SectionID;
    long IndexNum;
    UIRefreshControl *refreshControl;
}
@property (strong, nonatomic) IBOutlet UILabel *lblBanner;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnDrawer;
@property (strong, nonatomic) IBOutlet UICollectionView *SponsorCollection;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (nonatomic, strong) DBManager *dbManager;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;

@end

@implementation SponsorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _overlayView.hidden=NO;
    Constants *obj=[[Constants alloc]init];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:obj.dbName];

    [self getSponsorDataFromDB];
    IndexNum=1;
    extraNumMajor=2;
    
    //----------------------------------------------
//    [_lblBanner.layer setCornerRadius:17.0f];
//
//    // border
//    [_lblBanner.layer setBorderColor:[UIColor whiteColor].CGColor];
//    [_lblBanner.layer setBorderWidth:0.9f];
//    _lblBanner.clipsToBounds=YES;
    
    [self.btnDrawer setTarget:self.revealViewController];
    [self.btnDrawer setAction:@selector(revealToggle: )];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [self.SponsorCollection addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refreshTable{
    [refreshControl endRefreshing];
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        [self showsUIAlertWithMessageWithButtons:@"Content cannot be Updated. No Internet Connection Avaliable" andTitle:@"Information"];
    }
    else
    {
        Reachability* reachability = [Reachability reachabilityWithHostName: @"www.google.com"];
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        NSLog(@"the nertwok is %ld", (long)netStatus);
        if((long)netStatus>0){
            [self UpdateSponsorData];
        }
        else{
            [self showsUIAlertWithMessage:@"Oh!No.Your internet connection is not stable. Please Check Internet connection and Try Again." andTitle:@"Alert"];
        }
    }
   
}

-(void)UpdateSponsorData{
    SponsorDownloads = [[SponsorJsonDownload alloc]init];
    [SponsorDownloads setDelegate:(id)self];
     NSString *BoolDBLock =[[NSUserDefaults standardUserDefaults] stringForKey:@"BoolDBLock"];
    if(![BoolDBLock isEqualToString:@"YES"]){
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"BoolDBLock"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [SponsorDownloads OrganizeDataFromJsonToObject];
    }
   
    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"sponsordetails"])
    {
        
        // Get reference to the destination view controller
        SponsorDetailsViewController *vc = [segue destinationViewController];
        NSIndexPath *indexPath = (NSIndexPath*) sender;
        if (indexPath.section==0) {
            Sponsors *objectToBeSent=[[Sponsors alloc]init];
        NSLog(@"indexpath row is %ld", (long)indexPath.row);
            objectToBeSent = [SponsorMajor objectAtIndex:indexPath.row];
            [vc setSponsorWebUrl:objectToBeSent];
        }
     
    }

}

-(void)getSponsorDataFromDB{
    
    NSString *query = [NSString stringWithFormat:@"select * from tblSponsors where active=='1'"];
    
    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
    NSLog(@"db elements %@",dbresults);
    
    NSMutableArray *arrGetSponsor=[[NSMutableArray alloc]init];
    for(int i=0;i<dbresults.count;i++){
        Sponsors *obj =[[Sponsors alloc]init];
        obj.SponsorID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"id"]];
        obj.SponsorCompanyName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"companyname"]];
        obj.SponsorLogoURL=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"logo"]];
        obj.lastmodified=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
        obj.SponsorTypeID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"sponsortype_id"]];
        obj.SponsorActive=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"active"]];
        obj.SponsorWebURL=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"url"]];
        obj.SponsorEventID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"event_id"]];
        [arrGetSponsor addObject:obj];
    }
    arrSponsors=[[NSMutableArray alloc]init];
    arrSponsors=arrGetSponsor;
    
        SponsorMajor=[[NSMutableArray array]init];
        SponsorMinor=[[NSMutableArray array]init];
        //NSNumber *comp=[[NSNumber alloc] initWithInt:1];
        //Sponsors *obj=[[Sponsors alloc]init];
        NSString *test;
        NSString *testEvent;
        
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        
        EventRealID =[[NSUserDefaults standardUserDefaults] stringForKey:@"EventRealID"];
        
        NSString *Major=@"1";
        NSString *Minor=@"2";
        for(Sponsors *obj in arrSponsors){
            test=[[NSString alloc]initWithFormat:@"%@",obj.SponsorTypeID ];
            testEvent=[[NSString alloc]initWithFormat:@"%@",obj.SponsorEventID ];
            NSLog(@"each object type =%@",test);
            if([test isEqual:Major] && [EventRealID isEqual:testEvent]){
                
                //NSLog(@"test value is %@",test);
                [SponsorMajor addObject:obj];
            }
            
            if([test isEqual:Minor] && [EventRealID isEqual:testEvent]){
                test=obj.SponsorTypeID;
                //NSLog(@"test value is %@",test);
                [SponsorMinor addObject:obj];
            }
        }
        NSLog(@"majors is %@ and minor is %lu",SponsorMajor,(unsigned long)SponsorMinor.count);
        [_SponsorCollection reloadData];
    if(SponsorMajor.count==0&&SponsorMajor.count==0){
        _lblStatus.text=@"No Sponsors Were Found.";
        [_actIndicator stopAnimating];
    }else{
        _overlayView.hidden=YES;
    }
    
    
}
    


-(void)getSponsorDownload:(NSMutableArray *)sponsors{
    
    @try {
        NSString *clearquery = [NSString stringWithFormat:@"delete from tblSponsors"];
        //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
        
        [self.dbManager executeQuery:clearquery];
        
        
        //    // If the query was successfully executed then pop the view controller.
        if (self.dbManager.affectedRows != 0) {
            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
        }
        else{
            NSLog(@"Could not execute the query.");
        }
        for(Sponsors *dbSponsor in sponsors){
            dbSponsor.SponsorCompanyName=[dbSponsor.SponsorCompanyName stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblSponsors (id,companyname,logo,lastmodified,sponsortype_id,active,url,event_id) Values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",dbSponsor.SponsorID,dbSponsor.SponsorCompanyName,dbSponsor.SponsorLogoURL,dbSponsor.lastmodified,dbSponsor.SponsorTypeID,dbSponsor.SponsorActive,dbSponsor.SponsorWebURL,dbSponsor.SponsorEventID];
            
            
            [self.dbManager executeQuery:query];
            
            
            //    // If the query was successfully executed then pop the view controller.
            if (self.dbManager.affectedRows != 0) {
                NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
            }
            else{
                NSLog(@"Could not execute the query.");
            }
        }
        
        [self getSponsorDataFromDB];
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"BoolDBLock"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } @catch (NSException *e) {
         [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
    }
    
    
}

- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
    
    [alert show];
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 2;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if(section==0){
        return SponsorMajor.count;
        
    }
    
    if (section==1){
        if(!(SponsorMinor.count%2==0)){
            extraNum=SponsorMinor.count+1;
            return SponsorMinor.count+1;

        }
        return SponsorMinor.count;
    }
  
    return 0;
    
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section==0){

        
        MajorSponsorCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Major" forIndexPath:indexPath];
        cell.tag=indexPath.row;
     
       
            Sponsors *obj= [[Sponsors alloc]init];
            Constants *cObj = [[Constants alloc]init];
            obj=[SponsorMajor objectAtIndex:indexPath.row];
            cell.lblSponsorName.text=[NSString stringWithFormat:@"%@",obj.SponsorCompanyName];
            if([obj.SponsorWebURL localizedCaseInsensitiveContainsString:@".pdf"]){
                cell.lblSponsorSite.text=[NSString stringWithFormat:@"View PDF"];
            }
            else{
                cell.lblSponsorSite.text=[NSString stringWithFormat:@"%@",obj.SponsorWebURL];
            }
        
            NSString *urlpath=[NSString stringWithFormat:@"http://%@/%@",cObj.endpoint,obj.SponsorLogoURL];
            NSURL *url = [NSURL URLWithString:urlpath];

            [cell.ImageMajor hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"LoadingImage.jpg"]];
        
        

        return cell;
        }
    
  
    
        if (indexPath.section==1){
            if(indexPath.row>SponsorMinor.count-1){
                MinorCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Minor" forIndexPath:indexPath];
                cell.ImageMinor.image=[UIImage imageNamed:@"white.png"];
//                if(indexPath.row==extraNum-1){
//
//                    CAShapeLayer * maskLayer = [CAShapeLayer layer];
//                    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: cell.bounds byRoundingCorners: UIRectCornerBottomRight cornerRadii: (CGSize){17.0, 17.0}].CGPath;
//                    cell.layer.mask = maskLayer;
//                }
                return cell;
            }

           MinorCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Minor" forIndexPath:indexPath];
    
            cell.tag=indexPath.row;
            cell.ImageMinor.image=[UIImage imageNamed:@"LoadingImage.jpg"];
            
            Sponsors *obj= [[Sponsors alloc]init];
            Constants *cObj = [[Constants alloc]init];
            obj=[SponsorMinor objectAtIndex:indexPath.row];
            NSString *compString=[[NSString alloc]initWithFormat:@"%@",obj.SponsorLogoURL];
            if([compString isEqualToString:@"<null>"]){
               UIImage *ImageToBeConverted=[UIImage imageNamed:@"imagenotfound.gif"];
                [cell.ImageMinor setImage:ImageToBeConverted];

            }else{
                    NSString *urlpath=[NSString stringWithFormat:@"http://%@/%@",cObj.endpoint,obj.SponsorLogoURL];
                    
                    
                    NSURL *url = [NSURL URLWithString:urlpath];
                    [cell.ImageMinor hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"LoadingImage.jpg"]];


            }
            

    
            return cell;

    
        }
   return 0;
}



-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        if(indexPath.section==0){
        CollectionReusableViewSectionHeader *headerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"sectionheader" forIndexPath:indexPath];
        headerview.lblSectionLabel.text=@"MAJOR SPONSORS";
        reusableview = headerview;
        }
        if (indexPath.section==1){
        CollectionReusableViewSectionHeader *headerview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"sectionheader" forIndexPath:indexPath];
        headerview.lblSectionLabel.text=@"SUPPORTING SPONSORS";
        reusableview = headerview;
        }
    }
    

    
    return reusableview;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section==0){
    [self performSegueWithIdentifier:@"sponsordetails" sender:indexPath];
    EventID=(long)indexPath.row;
    SectionID=(long)indexPath.section;
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
//    if(indexPath.row==SponsorMajor.count-1 && extraNumMajor==1 && indexPath.section==0){
//        return CGSizeMake(CGRectGetWidth(collectionView.frame),CGRectGetHeight(collectionView.frame)/3+20);
//    }
    
    if(indexPath.section==0){
        return CGSizeMake(CGRectGetWidth(collectionView.frame)-2,160);
    }
    
    return CGSizeMake(CGRectGetWidth(collectionView.frame)/2-10,CGRectGetHeight(collectionView.frame)/3);
    
}


- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if(section==1){
    return 0.0;
    }
    return 17.0;
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    if (section==0){
//        if(IndexNum==extraNumMajor){
//            return UIEdgeInsetsMake(0, 100, 0, 0);
//        }
       // else{
        return UIEdgeInsetsMake(5, 5, 5, 5);
        //}
    }
    
    
    
    return UIEdgeInsetsMake(0, 0, 0, 0);
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    NSString *eventName=[[NSUserDefaults standardUserDefaults] stringForKey:@"EventName"];
    _lblBanner.text=eventName;
    
    if (!(arrSponsors.count==0)){
        [self getSponsorDataFromDB];
    SponsorMajor=[[NSMutableArray array]init];
    SponsorMinor=[[NSMutableArray array]init];
  
    NSString *test;
        NSString *testEvent;
   
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    EventRealID =[[NSUserDefaults standardUserDefaults] stringForKey:@"EventRealID"];
    
    NSString *Major=@"1";
        NSString *Minor=@"2";
    for(Sponsors *obj in arrSponsors){
        test=[[NSString alloc]initWithFormat:@"%@",obj.SponsorTypeID ];
        testEvent=[[NSString alloc]initWithFormat:@"%@",obj.SponsorEventID ];
        NSLog(@"each object type =%@",test);
        if([test isEqual:Major] && [EventRealID isEqual:testEvent]){
            
            //NSLog(@"test value is %@",test);
            [SponsorMajor addObject:obj];
        }
        
        if([test isEqual:Minor] && [EventRealID isEqual:testEvent]){
            test=obj.SponsorTypeID;
            //NSLog(@"test value is %@",test);
            [SponsorMinor addObject:obj];
        }
    }
    NSLog(@"majors is %@ and minor is %lu",SponsorMajor,(unsigned long)SponsorMinor.count);
    [_SponsorCollection reloadData];
        if(SponsorMajor.count==0&&SponsorMajor.count==0){
            _overlayView.hidden=NO;
            _lblStatus.text=@"No Sponsors Were Found.";
            [_actIndicator stopAnimating];
        }else{
            _overlayView.hidden=YES;
        }
        
    }
}

- (void)showsUIAlertWithMessageWithButtons:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings",nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        NSLog(@"Cancel button clicked");
    }
    if (buttonIndex == 1){
        NSLog(@"settings button clicked");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-prefs:"]];
    }
}


@end
