//
//  AttendeesViewController.h
//  FIAEventsApp
//
//  Created by temp on 08/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AttendeesViewController : UIViewController <UICollectionViewDelegate,UICollectionViewDataSource,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UICollectionView *AttendeesCollection;

@end
