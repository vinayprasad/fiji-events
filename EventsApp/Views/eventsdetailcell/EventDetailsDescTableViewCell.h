//
//  EventDetailsDescTableViewCell.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 03/04/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailsDescTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblEventDescription;

@end
