//
//  EventDetailsDateTimeVenueTableViewCell.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 03/04/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailsDateTimeVenueTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblContentName;
@property (strong, nonatomic) IBOutlet UILabel *lblContent;
@property (weak, nonatomic) IBOutlet UIButton *btnSubsToEvent;

@end
