//
//  EventDetailsMapsTableViewCell.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 03/04/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface EventDetailsMapsTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet MKMapView *mapsEventLocation;

@end
