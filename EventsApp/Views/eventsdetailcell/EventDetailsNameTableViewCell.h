//
//  EventDetailsNameTableViewCell.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 03/04/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EventDetailsNameTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblEventName;

@end
