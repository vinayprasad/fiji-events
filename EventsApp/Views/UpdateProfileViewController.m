//
//  UpdateProfileViewController.m
//  FIAEventsApp
//
//  Created by Ashivan on 5/10/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import "UpdateProfileViewController.h"
#import "Constants.h"

@interface UpdateProfileViewController (){
    bool _flagged;
}
@property (weak, nonatomic) IBOutlet UIWebView *webViewUpdateProfile;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;

@end

@implementation UpdateProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    Constants *obj = [[Constants alloc ]init];
    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    NSString *UserID =[[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"];
    if(![Username isEqualToString:@"Guest"] && [UserID length]!=0){
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/web/profile/update?user_id=%@",obj.endpoint,UserID]];
        NSURLRequest *request =  [NSURLRequest requestWithURL:url];
        [_webViewUpdateProfile loadRequest:request];
    }
    
    else{
        [self showsUIAlertWithMessage:@"Sorry. Please Login to access this feature" andTitle:@"Information"];
        [_actIndicator stopAnimating];
        _actIndicator.hidesWhenStopped=YES;
    }
    
    self.webViewUpdateProfile.delegate=self;
   
//    [_lblBanner.layer setCornerRadius:17.0f];
//
//    // border
//    [_lblBanner.layer setBorderColor:[UIColor whiteColor].CGColor];
//    [_lblBanner.layer setBorderWidth:0.9f];
//    _lblBanner.clipsToBounds=YES;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Avoiding iOS bug

- (UIViewController *)presentingViewController {
    
    // Avoiding iOS bug. UIWebView with file input doesn't work in modal view controller
    
    if (_flagged) {
        return nil;
    } else {
        return [super presentingViewController];
    }
}

- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    
    // Avoiding iOS bug. UIWebView with file input doesn't work in modal view controller
    
    if ([viewControllerToPresent isKindOfClass:[UIDocumentMenuViewController class]]
        ||[viewControllerToPresent isKindOfClass:[UIImagePickerController class]]) {
        _flagged = YES;
    }
    
    [super presentViewController:viewControllerToPresent animated:flag completion:completion];
}

- (void)trueDismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    
    // Avoiding iOS bug. UIWebView with file input doesn't work in modal view controller
    
    _flagged = NO;
    [self dismissViewControllerAnimated:flag completion:completion];
}
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [self.actIndicator startAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.actIndicator stopAnimating];
    self.actIndicator.hidesWhenStopped=YES;
}

- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}
- (IBAction)btnBackWeb:(id)sender {
    _flagged=NO;
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"its done");
    }];
}

@end
