//
//  SpeakerDetailsViewController.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 06/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeakerDetailsViewController : UIViewController<UIWebViewDelegate,UIAlertViewDelegate>
-(void)setSpeakerUrl:(NSString*)url andName:(NSString*)speakername;
@end
