//
//  ReviewTestViewController.m
//  FIAEventsApp
//
//  Created by Ashivan on 5/9/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import "ReviewTestViewController.h"
#import "RatingTableViewCell.h"
#import "SpeakerRatingTableViewCell.h"
#import "Session Times.h"
#import "Events.h"
#import "EventRatingsTableViewCell.h"
#import "Constants.h"
#import "HCSStarRatingView.h"
#import "DBManager.h"

@interface ReviewTestViewController ()
{
    Session_Times *sObj;
    Events *eObj;
    BOOL endOfEvent;
    NSString *userID;
    
    //speaker variables
    NSString *speakerID;
    NSString *speakerComments;
    int speakerRatingsNum;
    
    //session variables
    NSString *sessionID;
    NSString *sessionComments;
    int sessionRatingsNum;
    
    //Event variables
    NSString *eventID;
    NSString *eventComments;
    int eventRatingsNum;
    
    //txtfeild
    UITextField *txtComment1;
    UITextField *txtComment2;
    UITextField *txtComment3;
    
    float FullWidth;
    float CutWidth;
    float keyboardSize;
}

@end

@implementation ReviewTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tblRatings.keyboardDismissMode= UIScrollViewKeyboardDismissModeOnDrag;
    //speaker variables
    speakerID=@"";
    speakerComments=@"Comment";
    
    //session variables
    sessionID=@"";
    sessionComments=@"Comment";
    
    //Event variables
    eventID=@"";
    eventComments=@"Comment";
    keyboardSize=300;
    FullWidth=self.view.frame.size.height;
    CutWidth=FullWidth-keyboardSize;
    
    _lblSessionTitle.text=sObj.sessionProgramme;
    
    [self someMethodWhereYouSetUpYourObserver];
    userID =[[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"];
    
    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    [self.tblRatings addGestureRecognizer:tap];
    
    Constants *obj=[[Constants alloc]init];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:obj.dbName];
    sessionID=[NSString stringWithFormat:@"%@",sObj.sessionID];
    [self getSessionDataForDisplay];
    [self getSpeakerDataForDisplay];
    
}
- (void)someMethodWhereYouSetUpYourObserver
{
    // This could be in an init method.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myNotificationMethod:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
}

- (void)myNotificationMethod:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
    keyboardSize=keyboardFrameBeginRect.size.height;
    CutWidth=FullWidth-keyboardSize;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //if end of event then 3 else 2
    if(endOfEvent==YES){
        return 3;
    }
    return 2;//session and speaker
    
}

- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    NSLog(@"Session Changed rating to %.1f", sender.value);
    sessionRatingsNum=sender.value;
}

- (IBAction)didChangeValueSpeaker:(HCSStarRatingView *)sender {
    NSLog(@" Speaker Changed rating to %.1f", sender.value);
    speakerRatingsNum=sender.value;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    eventID=[NSString stringWithFormat:@"%@", eObj.eventID];
    
    if (indexPath.row==0){
        RatingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Rating" forIndexPath:indexPath];
        cell.lblTitle.text=[NSString stringWithFormat:@"Session Rating-%@",sObj.sessionProgramme];
        sessionID=[NSString stringWithFormat:@"%@", sObj.sessionID];
        NSString *temp=cell.txtComments.text;
        cell.txtComments.text=sessionComments;
        cell.txtComments.delegate=self;
        HCSStarRatingView *starRatingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(0,0,cell.ratingView.frame.size.width,cell.ratingView.frame.size.height)];
        starRatingView.maximumValue = 5;
        starRatingView.minimumValue = 0;
        starRatingView.value = sessionRatingsNum;
        starRatingView.allowsHalfStars = NO;
        starRatingView.tintColor = [UIColor colorWithRed:0.141 green:0.455 blue:0.663 alpha:1.00];       [starRatingView addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
        [cell.ratingView addSubview:starRatingView];
        
        [cell.btnSubmit addTarget:self action:@selector(btnSessionClick:) forControlEvents:UIControlEventTouchUpInside];
        
        long RatingNo =[[NSUserDefaults standardUserDefaults] integerForKey:@"SessionRating"];
        
        NSLog(@"Rating Number => %ld and %@",RatingNo , sObj.sessionProgramme);
        
        return cell;
    }
    
    if (indexPath.row==1){
        SpeakerRatingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SpeakerRatings" forIndexPath:indexPath];
        
        cell.lblTitle.text=[NSString stringWithFormat:@"Speaker Rating-%@",sObj.sessionSpeaker];
        speakerID=[NSString stringWithFormat:@"%@", sObj.sessionSpeakerID];
        NSString *temp=cell.txtComments.text;
        cell.txtComments.text=speakerComments;
        HCSStarRatingView *starRatingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(0,0,cell.ratingView.frame.size.width,cell.ratingView.frame.size.height)];
        starRatingView.maximumValue = 5;
        starRatingView.minimumValue = 0;
        starRatingView.value = speakerRatingsNum;
        starRatingView.allowsHalfStars = NO;
        starRatingView.tintColor = [UIColor colorWithRed:0.141 green:0.455 blue:0.663 alpha:1.00];;
        [starRatingView addTarget:self action:@selector(didChangeValueSpeaker:) forControlEvents:UIControlEventValueChanged];
        [cell.ratingView addSubview:starRatingView];
        
        [cell.btnSubmit addTarget:self action:@selector(btnSpeakerClick:) forControlEvents:UIControlEventTouchUpInside];
        long RatingNo =[[NSUserDefaults standardUserDefaults] integerForKey:@"SpeakerRating"];
        NSLog(@"Rating Number => %ld and %@",RatingNo , sObj.sessionProgramme);
        return cell;
    }
    //will event be here or event details
    if (indexPath.row==2){
        EventRatingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventRatings" forIndexPath:indexPath];
        cell.lblTitle.text=[NSString stringWithFormat:@"Event Rating-%@",eObj.eventName];
        eventID=[NSString stringWithFormat:@"%@", eObj.eventID];
        eventComments=[NSString stringWithFormat:@"%@",cell.txtComments.text];
        txtComment3=cell.txtComments;
        [cell.btnSubmit addTarget:self action:@selector(btnEventsClick:) forControlEvents:UIControlEventTouchUpInside];
        long RatingNo =[[NSUserDefaults standardUserDefaults] integerForKey:@"EventRating"];
        NSLog(@"Rating Number => %ld and %@",RatingNo , sObj.sessionProgramme);
        return cell;
    }
    
    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if(endOfEvent==YES){
//        return tableView.frame.size.height/3;
//    }
    return UITableViewAutomaticDimension;;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,FullWidth)];
}

-(void)setSessionDetails:(Session_Times *)sessionObj andCheck:(BOOL)isitEndOfEvent andEventID:(Events*)eventObj{
    sObj=sessionObj;
    endOfEvent=isitEndOfEvent;
    eObj=eventObj;
    
}
// 0 is session
//1 is speaker

-(void)btnSessionClick:(id)sender{
    
    if([self getSessionData]==true){
        
        [self showsUIAlertWithMessage:@"Rating has been updated" andTitle:@"Success"];
        
    }
    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    if(![Username isEqualToString:@"Guest"]){
        //send all info about session
        long RatingNo =[[NSUserDefaults standardUserDefaults] integerForKey:@"SessionRating"];
        NSIndexPath *nowIndex = [NSIndexPath indexPathForRow:0 inSection:0];
        RatingTableViewCell*cell = [self.tblRatings cellForRowAtIndexPath:nowIndex];
        //[self LoginForUser:username andpassword:password];
        Constants *cObj=[[Constants alloc]init];
        NSLog(@"ip adddress is :%@",cObj.endpoint);
        
        NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/rest/rate",cObj.endpoint]]];
        
        NSString *temp=cell.txtComments.text;
        sessionComments=temp;
        NSString *userUpdate =[NSString stringWithFormat:@"type=%@&userid=%@&sessionid=%@&rate=%@&message=%@", @"session",userID,sessionID,[NSString stringWithFormat:@"%d", sessionRatingsNum],sessionComments];
        
        //insert rate into db
        
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        //create the Method "GET" or "POST"
        [urlRequest setHTTPMethod:@"POST"];
        
        [urlRequest setValue:@"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd" forHTTPHeaderField:@"Authorization"];
        //Convert the String to Data
        NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
        
        //Apply the data to the body
        [urlRequest setHTTPBody:data1];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSLog(@"Response =%@",response);
            if(httpResponse.statusCode == 200)
            {
                NSError *parseError = nil;
                NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                NSLog(@"The response is - %@",responseDictionary);
                NSDictionary *success = [responseDictionary objectForKey:@"data"];
                NSLog(@"The success is - %@",success);
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self insertIntoSQLite:userID andComments:sessionComments andRatings:[NSString stringWithFormat:@"%d", sessionRatingsNum] andSessionID:sessionID andEventID:eventID andtype:0];
                    NSLog(@"query executed");
                    
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"Error response is %@", response);
                    
                });
                
            }
        }];
        [dataTask resume];
        //sobj.sessionID;
        //     [self hideKeyboard];
        [self showsUIAlertWithMessage:@"Thank You for Your Submission" andTitle:@"Submitted-Session"];
    }
    else{
        [self showsUIAlertWithMessage:@"Sorry.Please Login to access this feature" andTitle:@"Information"];
    }
    
}
-(void)btnSpeakerClick:(id)sender{
    if([self getSpeakerData]==true){
        
        [self showsUIAlertWithMessage:@"Rating has been updated" andTitle:@"Success"];
        
    }
    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    if(![Username isEqualToString:@"Guest"]){
        
        if([speakerID length]==0){
            [self showsUIAlertWithMessage:@"This Session has no Speaker" andTitle:@"Alert"];
        }
        //send all info about speaker
        //     long speakerRatingNo =[[NSUserDefaults standardUserDefaults] integerForKey:@"SpeakerRating"];
        NSIndexPath *nowIndex = [NSIndexPath indexPathForRow:1 inSection:0];
        RatingTableViewCell*cell = [self.tblRatings cellForRowAtIndexPath:nowIndex];
        //[self LoginForUser:username andpassword:password];
        Constants *cObj=[[Constants alloc]init];
        NSLog(@"ip adddress is :%@",cObj.endpoint);
        
        NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/rest/rate",cObj.endpoint]]];
        
        NSString *temp=cell.txtComments.text;
        speakerComments=temp;
        NSString *userUpdate =[NSString stringWithFormat:@"type=%@&userid=%@&speakerid=%@&rate=%@&message=%@", @"speaker",userID,speakerID,[NSString stringWithFormat:@"%d", speakerRatingsNum],speakerComments];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        //create the Method "GET" or "POST"
        [urlRequest setHTTPMethod:@"POST"];
        
        [urlRequest setValue:@"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd" forHTTPHeaderField:@"Authorization"];
        //Convert the String to Data
        NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
        
        //Apply the data to the body
        [urlRequest setHTTPBody:data1];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSLog(@"Response =%@",response);
            if(httpResponse.statusCode == 200)
            {
                NSError *parseError = nil;
                NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                NSLog(@"The response is - %@",responseDictionary);
                NSDictionary *success = [responseDictionary objectForKey:@"data"];
                NSLog(@"The success is - %@",success);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self insertIntoSQLiteSpeaker:userID andComments:speakerComments andRatings:[NSString stringWithFormat:@"%d", speakerRatingsNum] andSessionID:sessionID andEventID:eventID andtype:1];
                    NSLog(@"query executed");
                    
                });
                
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"Error response is %@", response);
                    
                    //                [self showsUIAlertWithMessage:@"Error Message" andTitle:@"host not found. Please try again"];
                    //[self performSegueWithIdentifier:@"MainPage" sender:self];
                });
                
            }
        }];
        [dataTask resume];
        //sobj.sessionID;
        //sObj.sessionSpeaker;
        //     [self hideKeyboard];
        [self showsUIAlertWithMessage:@"Thank You for Your Submission" andTitle:@"Submitted-Speaker"];
        
    }
    else{
        [self showsUIAlertWithMessage:@"Sorry.Please Login to access this feature" andTitle:@"Information"];
    }
}

-(void)btnEventsClick:(id)sender{
    //send all info about speaker
    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    if(![Username isEqualToString:@"Guest"]){
        long eventRatingNo =[[NSUserDefaults standardUserDefaults] integerForKey:@"EventRating"];
        NSIndexPath *nowIndex = [NSIndexPath indexPathForRow:2 inSection:0];
        RatingTableViewCell*cell = [self.tblRatings cellForRowAtIndexPath:nowIndex];
        //[self LoginForUser:username andpassword:password];
        Constants *cObj=[[Constants alloc]init];
        NSLog(@"ip adddress is :%@",cObj.endpoint);
        
        NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/rest/rate",cObj.endpoint]]];
        
        NSString *temp=cell.txtComments.text;
        eventComments=temp;
        NSString *userUpdate =[NSString stringWithFormat:@"type=%@&userid=%@&eventid=%@&rate=%@&message=%@", @"event",userID,eventID,[NSString stringWithFormat:@"%ld", eventRatingNo],eventComments];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        //create the Method "GET" or "POST"
        [urlRequest setHTTPMethod:@"POST"];
        
        [urlRequest setValue:@"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd" forHTTPHeaderField:@"Authorization"];
        //Convert the String to Data
        NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
        
        //Apply the data to the body
        [urlRequest setHTTPBody:data1];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSLog(@"Response =%@",response);
            if(httpResponse.statusCode == 200)
            {
                NSError *parseError = nil;
                NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                NSLog(@"The response is - %@",responseDictionary);
                NSDictionary *success = [responseDictionary objectForKey:@"data"];
                NSLog(@"The success is - %@",success);
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"Error response is %@", response);
                    
                });
                
            }
        }];
        [dataTask resume];
        
        [self showsUIAlertWithMessage:@"Thank You for Your Submission" andTitle:@"Submitted-Event"];
        
    }
    else{
        [self showsUIAlertWithMessage:@"Sorry.Please Login to access this feature" andTitle:@"Information"];
    }
}

- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,FullWidth)];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
//    textView.text=@"";
//    textView.textColor=[UIColor blackColor];
    
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,FullWidth)];
    //     [self hideKeyboard];
    return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    if(textView.tag==0){
       
        [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
         [textView isFirstResponder];
          return YES;
    }
    
    if(textView.tag==1){
        
         [self.view setFrame:CGRectMake(0,-keyboardSize,self.view.frame.size.width,self.view.frame.size.height)];
        [textView isFirstResponder];
          return YES;
    }
    
    
    return NO;
}




-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if([txtComment1 isFirstResponder] && [touch view] !=txtComment1){
        [txtComment1 resignFirstResponder];
        
        
    }
    if([txtComment2 isFirstResponder] && [touch view] != txtComment2){
        [txtComment2 resignFirstResponder];
        
        
    }
    if([txtComment3 isFirstResponder] && [touch view] != txtComment3){
        [txtComment3 resignFirstResponder];
        
        
    }
    
    [super touchesBegan:touches withEvent:event];
}

-(void)hideKeyboard{
    //    [txtComment1 resignFirstResponder];
    //     [txtComment2 resignFirstResponder];
    //     [txtComment3 resignFirstResponder];
    
}

-(void)insertIntoSQLite:(NSString*)userid andComments:(NSString*)comments andRatings:(NSString*)ratings andSessionID:(NSString*)sessionid andEventID:(NSString*)eventid andtype:(int)typeNum{
    //  need a type colmn for type session/speaker/event in sqlite
    NSString *query;
    if([self getSessionData]==true){
        query = [NSString stringWithFormat:@"UPDATE tblReview SET comments='%@' ,rating='%@' Where sessionid='%@' and rate_type='%d'",comments,ratings,sessionid,typeNum];
        
        //        UPDATE "main"."tblReview" SET "comments" = ?1, "rating" = ?2 WHERE  "id" = 2
    }
    else{
        query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblReview (user_id,comments,rating,sessionid,eventid,rate_type) Values ('%@','%@','%@','%@','%@','%d')",userid,comments,ratings,sessionid,eventid,typeNum];
    }
    //    NSString *clearquery2 = [NSString stringWithFormat:@"delete from tblReview"];
    
    [self.dbManager executeQuery:query];
    
    
    //    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
    }
    else{
        NSLog(@"Could not execute the query.");
    }
}

-(void)insertIntoSQLiteSpeaker:(NSString*)userid andComments:(NSString*)comments andRatings:(NSString*)ratings andSessionID:(NSString*)sessionid andEventID:(NSString*)eventid andtype:(int)typeNum{
    //  need a type colmn for type session/speaker/event in sqlite
    NSString *query;
    if([self getSpeakerData]==true){
        query = [NSString stringWithFormat:@"UPDATE tblReview SET comments='%@' ,rating='%@' Where sessionid='%@' and rate_type='%d'",comments,ratings,sessionid,typeNum];
        
        //        UPDATE "main"."tblReview" SET "comments" = ?1, "rating" = ?2 WHERE  "id" = 2
    }
    else{
        query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblReview (user_id,comments,rating,sessionid,eventid,rate_type) Values ('%@','%@','%@','%@','%@','%d')",userid,comments,ratings,sessionid,eventid,typeNum];
    }
    //    NSString *clearquery2 = [NSString stringWithFormat:@"delete from tblReview"];
    
    [self.dbManager executeQuery:query];
    
    
    //    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
    }
    else{
        NSLog(@"Could not execute the query.");
    }
}
-(BOOL)getSessionData{
    NSString *query = [NSString stringWithFormat:@"select * from tblReview where rate_type=0 and sessionid=%@",sessionID];
    
    //    //    // Execute the query.
    
    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
    if(dbresults.count>0){
        NSLog(@"%@",dbresults);
        //                for(int i=0;i<dbresults.count;i++){
        //                    sessionComments=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"comments"]];
        //                    NSString *ratings =[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"rating"]];
        //
        //                    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        //                    f.numberStyle = NSNumberFormatterDecimalStyle;
        //                    NSNumber *myNumber = [f numberFromString:ratings];
        //
        //                    sessionRatingsNum=[myNumber intValue];
        //
        //
        //
        //                }
        return true;
    }
    return false;
}

-(BOOL)getSpeakerData{
    NSString *query = [NSString stringWithFormat:@"select * from tblReview where rate_type=1 and sessionid=%@",sessionID];
    
    //    //    // Execute the query.
    
    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
    if(dbresults.count>0){
        NSLog(@"%@",dbresults);
        //        for(int i=0;i<dbresults.count;i++){
        //            speakerComments=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"comments"]];
        //            NSString *ratings =[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"rating"]];
        //
        //            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        //            f.numberStyle = NSNumberFormatterDecimalStyle;
        //            NSNumber *myNumber = [f numberFromString:ratings];
        //
        //            speakerRatingsNum=[myNumber intValue];
        //
        //
        //
        //        }
        return true;
    }
    return false;
}

-(void)getSessionDataForDisplay{
    NSString *query = [NSString stringWithFormat:@"select * from tblReview where rate_type=0 and sessionid=%@",sessionID];
    
    //    //    // Execute the query.
    
    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
    if(dbresults.count>0){
        NSLog(@"%@",dbresults);
        for(int i=0;i<dbresults.count;i++){
            sessionComments=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"comments"]];
            NSString *ratings =[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"rating"]];
            
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *myNumber = [f numberFromString:ratings];
            
            sessionRatingsNum=[myNumber intValue];
            
        }
    }
    
}

-(void)getSpeakerDataForDisplay{
    NSString *query = [NSString stringWithFormat:@"select * from tblReview where rate_type=1 and sessionid=%@",sessionID];
    
    //    //    // Execute the query.
    
    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
    if(dbresults.count>0){
        NSLog(@"%@",dbresults);
        for(int i=0;i<dbresults.count;i++){
            speakerComments=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"comments"]];
            NSString *ratings =[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"rating"]];
            
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *myNumber = [f numberFromString:ratings];
            
            speakerRatingsNum=[myNumber intValue];
            
            
            
        }
    }
    
}

@end
