//
//  ReviewTestViewController.h
//  FIAEventsApp
//
//  Created by Ashivan on 5/9/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
#import "Session Times.h"
#import "Events.h"

@interface ReviewTestViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIScrollViewDelegate,UITextViewDelegate>
@property (nonatomic, strong) DBManager *dbManager;
@property (weak, nonatomic) IBOutlet UILabel *lblSessionTitle;
@property (strong, nonatomic) IBOutlet UITableView *tblRatings;
-(void)setSessionDetails:(Session_Times *)sessionObj andCheck:(BOOL)isitEndOfEvent andEventID:(Events*)eventObj;
@end
