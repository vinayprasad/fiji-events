
//  EventViewController.h
//  FIAEventsApp
//
//  Created by temp on 08/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@interface EventViewController : UIViewController <UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate,SWRevealViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tblEvents;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UIButton *btnPrev;
@property (weak, nonatomic) NSMutableArray *events;
@property (strong, nonatomic) NSMutableArray *dayIndexes;
@property (weak, nonatomic) IBOutlet UIButton *btnEventDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnProgramSchedule;







@end
