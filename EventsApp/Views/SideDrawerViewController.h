//
//  SideDrawerViewController.h
//  FIAEventsApp
//
//  Created by Ashivan on 5/9/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"
@import GoogleSignIn;

@interface SideDrawerViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate,GIDSignInDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tblSideDrawerContent;
@property NSMutableArray *objects;
@property (nonatomic, strong) DBManager *dbManager;

@end
