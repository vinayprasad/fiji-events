//
//  EventViewController.m
//  FIAEventsApp
//
//  Created by temp on 08/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

/*
 figure out how to seque without moving into the view but retaining index at the same time
 */

#import "EventViewController.h"
#import "EventSliderCollectionViewCell.h"
#import "ChangeEventTableViewCell.h"
#import "SessionStartTableViewCell.h"
#import "SessionFinishedTableViewCell.h"
#import "SessionUpcomingTableViewCell.h"
#import "ScheduleHeaderTableViewCell.h"
#import "ScheduleFooterTableViewCell.h"
#import "ReviewViewController.h"
#import "EventDetailsViewController.h"
#import "EventJsonDownload.h"
#import "Dummy.h"
#import "Constants.h"
#import "Session Times.h"
#import "Haneke.h"
#import "DBManager.h"
#import "Question&AnswerViewController.h"
#import "PollingViewController.h"
#import "Reachability.h"
#import "Users.h"
#import "UserInfoDownload.h"
#import "Attendees.h"
#import "Events.h"
#import "EventJsonDownload.h"
#import "AttendeesJsonDownload.h"
#import "Speakers.h"
#import "SpeakersJSONDownload.h"
#import "Users.h"
#import "UserInfoDownload.h"
#import "Sponsors.h"
#import "SponsorJsonDownload.h"


@interface EventViewController (){
    NSMutableArray *arrEventImg;
    NSMutableArray *arrEventImgNames;
    NSMutableArray *arrEvents;
    NSMutableArray *arrSessionTimes;
    NSMutableArray *arrSessionTimesCut;
    NSArray *session;
    NSString *Slidername;
    NSString *value;
    long EventID;
    NSIndexPath *index;
    EventJsonDownload *_eventDownloaded;
    UICollectionView *EventSlider;
    long eventMenuChange;
    long eventRealID;
    long timeHour;
    long timeMinutes;
    long delay;
    int indexToScroll;
    int sessionDayNum;
    int sessions;
    BOOL indexToScrollAsc;
    BOOL checkDay;
    BOOL checkLoop;
    NSNumber *hasDiscussionValue;
    NSString *tomorrowDate;
    NSString *tomorrowTime;
    NSString *sessonDateToComp;
    UIRefreshControl *refreshControl;
    
    
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnDrawer;
@property (weak, nonatomic) IBOutlet UILabel *lblBanner;
@property (strong, nonatomic) IBOutlet UILabel *lblEventHeading;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;
@property (nonatomic, strong) DBManager *dbManager;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;

@end

@implementation EventViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [self.tblEvents addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    indexToScroll = 0;
    checkDay=false;
    checkLoop=false;
    sessionDayNum=0;
    tomorrowDate=@"2018-04-28";
    tomorrowTime=@"09:00 AM";
    hasDiscussionValue=[[NSNumber alloc]initWithInt:1];
    Constants *obj=[[Constants alloc]init];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:obj.dbName];
    [self getDownloadedDataFromDB];
    // Do any additional setup after loading the view.
    //    EventJsonDownload *temp=[EventJsonDownload alloc];
    delay=2;
    NSLog(@"-----------------------------------------------------------");
    //    EventID=0;
    
    [self.btnDrawer setTarget:self.revealViewController];
    [self.btnDrawer setAction:@selector(revealToggle: )];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    self.revealViewController.rearViewRevealWidth=self.view.frame.size.width-60;
    
    [self performSelector:@selector(refreshLabel) withObject:nil];
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -mark Refresh Events
-(void)refreshTable{
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        [refreshControl endRefreshing];
        [self showsUIAlertWithMessageWithButtons:@"Content cannot be Updated. No Internet Connection Avaliable" andTitle:@"Information"];
    }
    else
    {
        Reachability* reachability = [Reachability reachabilityWithHostName: @"www.google.com"];
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        NSLog(@"the nertwok is %ld", (long)netStatus);
        if((long)netStatus>0){
            [refreshControl endRefreshing];
            EventJsonDownload *eventDownloaded = [[EventJsonDownload alloc]init];
            [eventDownloaded setDelegate:(id)self];
            NSString *BoolDBLock =[[NSUserDefaults standardUserDefaults] stringForKey:@"BoolDBLock"];
            if(![BoolDBLock isEqualToString:@"YES"]){
                [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"BoolDBLock"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [eventDownloaded OrganizeDataFromJsonToObject];
            }
        }
        else{
            [self showsUIAlertWithMessage:@"Oh!No.Your internet connection is not stable. Please Check Internet connection and Try Again." andTitle:@"Alert"];
        }
        
        //        [_tblEvents reloadData];
        //         [self showsUIAlertWithMessageWithButtons:@"Content has been Updated" andTitle:@"Success"];
    }
    
}
#pragma -mark UPDATE DATA
-(void)getDownloaded:(NSMutableArray *)events andSession:(NSMutableArray *)session{
    
    @try {
        NSString *clearquery = [NSString stringWithFormat:@"delete from tblEvents"];
        //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
        
        [self.dbManager executeQuery:clearquery];
        
        
        //    // If the query was successfully executed then pop the view controller.
        if (self.dbManager.affectedRows != 0) {
            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
        }
        else{
            NSLog(@"Could not execute the query.");
        }
        for( Dummy *dbEvents in events){
            dbEvents.eventName=[dbEvents.eventName stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            dbEvents.eventDesc=[ dbEvents.eventDesc stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblEvents (active,name,id,description,end_date,image_cover,lastmodified,start_date,long,lat,venue_address) Values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",dbEvents.active,dbEvents.eventName,dbEvents.eventID,dbEvents.eventDesc,dbEvents.endDate,dbEvents.eventImageURL,dbEvents.lastModified,dbEvents.startDate,dbEvents.longtitude,dbEvents.latitude,dbEvents.venueID];
            
            
            [self.dbManager executeQuery:query];
            
            
            //    // If the query was successfully executed then pop the view controller.
            if (self.dbManager.affectedRows != 0) {
                NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
            }
            else{
                NSLog(@"Could not execute the query.");
            }
        }
        NSString *clearquery2 = [NSString stringWithFormat:@"delete from tblSessions"];
        //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
        
        [self.dbManager executeQuery:clearquery2];
        
        
        //    // If the query was successfully executed then pop the view controller.
        if (self.dbManager.affectedRows != 0) {
            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
        }
        else{
            NSLog(@"Could not execute the query.");
        }
        NSLog(@"Session count %ld",session.count);
        for(Session_Times *dbSession in session){
            dbSession.sessionProgramme=[dbSession.sessionProgramme stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblSessions (sessionid,start_time,end_time,program,active,lastmodified,venue_id,events_id,session_date,hasdiscussion,speakername,speakerid,isdebate) Values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",dbSession.sessionID, dbSession.sessionStartTime,dbSession.sessionEndTime,dbSession.sessionProgramme,dbSession.active,dbSession.lastmodified,dbSession.venueID,dbSession.eventID,dbSession.sessionSDate,dbSession.hasDiscussion,dbSession.sessionSpeaker,dbSession.sessionSpeakerID,dbSession.isDebatable];
            
            if([dbSession.eventID intValue]==13){
                NSLog(@"");
            }
            [self.dbManager executeQuery:query];
            
            //    // If the query was successfully executed then pop the view controller.
            if (self.dbManager.affectedRows != 0) {
                NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
            }
            else{
                NSLog(@"Could not execute the query.");
            }
        }
        [self getDownloadedDataFromDB];
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"BoolDBLock"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } @catch (NSException *e) {
        [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
    }
    
    //    [self getDownloadedDataFromDB];
    
}

#pragma -mark  END UPDATE DATA

-(void)getDownloadedDataFromDB{
    @try{
        //        NSNumber *eventNum=[[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"EventChange"];
        //        eventNum=[[NSNumber alloc]initWithInt:11];
        //        [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"EventChange"]
        //        NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1' and id==%ld",(long)[eventNum integerValue]];
        NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1'"];
        
        NSArray *dbresults =[self.dbManager loadDataFromDB:query];
        NSLog(@"db elements %@",dbresults);
        
        NSMutableArray * arrGetEvent=[[NSMutableArray alloc]init];
        arrEvents=[[NSMutableArray alloc]init];
        if(dbresults.count>0){
            for(int i=0;i<dbresults.count;i++){
                Dummy *obj =[[Dummy alloc]init];
                
                
                obj.eventID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"id"]];
                obj.eventName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"name"]];
                obj.eventDesc=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"description"]];
                obj.startDate=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"start_date"]];
                obj.endDate=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"end_date"]];
                obj.active=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"active"]];
                obj.lastModified=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
                obj.eventImageURL=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"image_cover"]];
                obj.venueID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"venue_address"]];
                obj.latitude=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lat"]];
                obj.longtitude=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"long"]];
                
                
                [arrGetEvent addObject:obj];
                
            }
        }
        arrEvents=arrGetEvent;
        
        // Get Session from Sqlite
        //        NSString *Squery = [NSString stringWithFormat:@"select * from tblSessions where active=='1' and events_id=%d",[eventNum intValue]];
        NSString *Squery = [NSString stringWithFormat:@"select * from tblSessions where active=='1'"];
        
        NSArray *dbSresults =[self.dbManager loadDataFromDB:Squery];
        //        NSLog(@"db elements %@",dbSresults);
        
        NSMutableArray * arrGetSession=[[NSMutableArray alloc]init];
        for(int i=0;i<dbSresults.count;i++){
            Session_Times *obj =[[Session_Times alloc]init];
            obj.eventID=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"events_id"]];
            obj.sessionStartTime=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"start_time"]];
            obj.sessionEndTime=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"end_time"]];
            obj.sessionProgramme=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"program"]];
            obj.active=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"active"]];
            obj.lastmodified=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
            obj.venueID=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"venue_id"]];
            
#pragma testingsessiondate
            
            //                    if(i<22){
            //                        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            //                        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            //                        NSString *currentdate=[dateFormatter stringFromDate:[NSDate date]];
            //            //            NSDate *datetoday=[dateFormatter dateFromString:currentdate];
            //                        NSDate *datetoday=[dateFormatter dateFromString:@"2018-05-29"];
            //                        //            NSLog(@"todays date=>%@",datetoday);
            //                        obj.sessionDate=datetoday;
            //                    }
            //                    else{
            //                        //            obj.sessionDate=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"session_date"]];
            //                        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            //                        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            //                        //            NSString *currentdate=[dateFormatter stringFromDate:[NSDate date]];
            //                        NSDate *datetomorrow=[dateFormatter dateFromString:@"2018-05-30"];
            //
            //                        obj.sessionDate=datetomorrow;
            //                    }
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
            obj.sessionSDate=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"session_date"]];
            obj.sessionDate=[dateFormatter dateFromString:obj.sessionSDate];
            obj.hasDiscussion=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"hasdiscussion"]];
            obj.sessionSpeakerID=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"speakerid"]];
            obj.sessionSpeaker=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"speakername"]];
            obj.sessionID=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"sessionid"]];
            obj.isDebatable=[[dbSresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"isdebate"]];
            [arrGetSession addObject:obj];
        }
        arrSessionTimes=arrGetSession;
        
        NSLog(@"arrsession is %ld",arrSessionTimes.count);
        
        Dummy * Dobj=[[Dummy alloc]init];
        
        Dobj = [arrEvents objectAtIndex:EventID];
        eventRealID=[Dobj.eventID longLongValue];
        
        value=Dobj.eventName;
        
        _lblEventHeading.text=Dobj.eventName;
        [[NSUserDefaults standardUserDefaults] setValue:value forKey:@"EventName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setInteger:EventID forKey:@"EventID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",Dobj.eventID] forKey:@"EventRealID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSString *MainEventID=[[NSString alloc]initWithFormat:@"%@",Dobj.eventID];
        NSString *ObjectEventID;
        arrSessionTimesCut=[[NSMutableArray alloc]init];
        if(arrSessionTimes.count>0){
            for (Session_Times *obj in arrSessionTimes){
                ObjectEventID=[[NSString alloc]initWithFormat:@"%@",obj.eventID];
                if([MainEventID isEqualToString:ObjectEventID]){
                    [arrSessionTimesCut addObject:obj];
                }
            }
        }
        [self setDayIndexes];
        //    NSLog(@"______________________________arrsessioncut______________________");
        NSLog(@"times cut %ld",arrSessionTimesCut.count);
        [_tblEvents reloadData];
        //    [EventSlider reloadData];
        
        
        [EventSlider scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:EventID inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        
        if(arrEvents.count==0){
            _lblStatus.text=@"Sorry. No Events Found.";
            [_actIndicator stopAnimating];
        }
        else{
            [_actIndicator stopAnimating];
            _overlayView.hidden=YES;
        }
    }@catch(NSException *e){
        
        [self showsUIAlertWithMessage:@"Oh!No.Your internet connection is not stable. Please Check Internet connection and Try Again." andTitle:@"Alert"];
        
    }
    
    
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"idEventDetails"])
    {
        // Get reference to the destination view controller
        EventDetailsViewController *vc = [segue destinationViewController];
        Dummy *ObjectToBeSent=[[Dummy alloc]init];
        
        int i=0;
        
        for(Dummy *obj in arrEvents){
            if (i == EventID){
                ObjectToBeSent.eventID=obj.eventID;
                ObjectToBeSent.eventName=obj.eventName;
                ObjectToBeSent.eventImageURL=obj.eventImageURL;
                ObjectToBeSent.startDate =obj.startDate;
                ObjectToBeSent.venueID=obj.venueID;
                ObjectToBeSent.eventDesc=obj.eventDesc;
                ObjectToBeSent.latitude=obj.latitude;
                ObjectToBeSent.longtitude=obj.longtitude;
                //[arrEventImg addObject:obj.eventImageURL];
                //                NSLog(@"The image url is %@ at index %ld",obj.eventImageURL,EventID);
                //[arrEventImgNames addObject:obj.eventName];
                
            }
            i++;
        }
        // Pass any objects to the view controller here, like...
        [vc setEventDetailObject:ObjectToBeSent];
    }
    if ([[segue identifier] isEqualToString:@"rate"])
    {
        bool checkEventProgress=FALSE;
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SessionRating"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"SpeakerRating"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // Get reference to the destination view controller
        ReviewViewController *vc = [segue destinationViewController];
        UIButton *index = (UIButton*) sender;
        //        NSLog(@"indexpath row is %ld", (long)index.tag);
        //        if (indexPath.section==0) {
        Session_Times *objectToBeSent=[[Session_Times alloc]init];
        Events *eventObjToBeSent=[[Events alloc]init];
        eventObjToBeSent=[arrEvents objectAtIndex:EventID];
        //        NSLog(@"indexpath row is %ld", (long)index.tag-1);
        objectToBeSent = [arrSessionTimesCut objectAtIndex:(long)index.tag-1];
        NSLog(@"--------------------------------------------------------------");
        NSLog(@"-----> %@",objectToBeSent.sessionProgramme);
        NSLog(@"--------------------------------------------------------------");
        if(index.tag-1==arrSessionTimesCut.count-1){
            
            checkEventProgress=TRUE;
        }
        [vc setSessionDetails:objectToBeSent andCheck:checkEventProgress andEventID:eventObjToBeSent];
        //        }
        
    }
    
    if ([[segue identifier] isEqualToString:@"panel"])
    {
        // Get reference to the destination view controller
        Question_AnswerViewController *vc = [segue destinationViewController];
        UIButton *index = (UIButton*) sender;
        NSLog(@"indexpath row is %ld", (long)index.tag);
        
        Session_Times *objectToBeSent=[[Session_Times alloc]init];
        objectToBeSent = [arrSessionTimesCut objectAtIndex:(long)index.tag-1];
        
        [vc SetSessionID:[NSString stringWithFormat:@"%@", objectToBeSent.sessionID]];
        
        
    }
    if ([[segue identifier] isEqualToString:@"polling"])
    {
        // Get reference to the destination view controller
        PollingViewController *vc = [segue destinationViewController];
        UIButton *index = (UIButton*) sender;
        NSLog(@"indexpath row is %ld", (long)index.tag);
        
        Session_Times *objectToBeSent=[[Session_Times alloc]init];
        objectToBeSent = [arrSessionTimesCut objectAtIndex:(long)index.tag-1];
        
        [vc SetSessionID:[NSString stringWithFormat:@"%@", objectToBeSent.sessionID]];
        [vc SetUserID:[NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"]]];
        
    }
    
    
}
-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    if(![identifier isEqualToString:@"idEventDetails"]){
        NSString *UserID =[[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"];
        Dummy *event=[arrEvents objectAtIndex:EventID];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
        [formatter setDateFormat:@"yyyy-MM-dd hh:mm aa"];
        
        NSString *currentDate= [formatter stringFromDate:[NSDate date]];
        NSDateFormatter *df=[[NSDateFormatter alloc]init];
        [df setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSDate *yyymmddEvent=[df dateFromString:event.endDate];
        [df setDateFormat:@"yyyy-MM-dd"];
        NSString *yyymmdd=[df stringFromDate:yyymmddEvent];
        
        NSString *EventDate = [NSString stringWithFormat:@"%@ 11:59 PM",yyymmdd];
        
        NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
        
        if([self compareDateAndTimeIsDecending2:currentDate:EventDate]==true){
            if([Username isEqualToString:@"Guest"]){
                [self showsUIAlertWithMessage:@"Sorry. Please Login to access this feature" andTitle:@"Information"];
                return NO;
            }
            else{
                [self showsUIAlertWithMessage:@"You cannot access this feature because the event has ended." andTitle:@"Notice"];
                return NO;
            }
            
        }
        else{
            if ([identifier isEqualToString:@"rate"]){
                
                
                
                if([Username isEqualToString:@"Guest"]){
                    [self showsUIAlertWithMessage:@"Sorry. Please Login to access this feature" andTitle:@"Information"];
                    return NO;
                }
                
                if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                {
                    [self showsUIAlertWithMessageWithButtons:@"You need to be connected to the internet to access this feature" andTitle:@"Alert"];
                    return NO;
                }
                NSString *query = [NSString stringWithFormat:@"select * from tblAttendees where active=='1' and events_id='%ld' and user_id='%@';",eventRealID,UserID];
                
                NSArray *dbresults =[self.dbManager loadDataFromDB:query];
                //        NSLog(@"db elements %@",dbresults);
                
                if(dbresults.count==0){
                    [self showsUIAlertWithMessage:@"Sorry. Please Subscribe for the event to access this feature" andTitle:@"Information"];
                    return NO;
                }
                
            }
            if ([identifier isEqualToString:@"discussion"]){
                NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
                if([Username isEqualToString:@"Guest"]){
                    [self showsUIAlertWithMessage:@"Sorry. Please Login to access this feature" andTitle:@"Information"];
                    return NO;
                }
                else{
                    NSString *query = [NSString stringWithFormat:@"select * from tblAttendees where active=='1' and events_id='%ld' and user_id='%@';",eventRealID,UserID];
                    
                    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
                    //            NSLog(@"db elements %@",dbresults);
                    
                    if(dbresults.count==0){
                        [self showsUIAlertWithMessage:@"Sorry. Please Subscribe for the event to access this feature" andTitle:@"Information"];
                        return NO;
                    }
                }
                
                if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                {
                    [self showsUIAlertWithMessageWithButtons:@"You need to be connected to the internet to access this feature" andTitle:@"Alert"];
                    return NO;
                }
                
                
            }
            if ([identifier isEqualToString:@"panel"]){
                NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
                if([Username isEqualToString:@"Guest"]){
                    [self showsUIAlertWithMessage:@"Sorry. Please Login to access this feature" andTitle:@"Information"];
                    return NO;
                }
                else{
                    NSString *query = [NSString stringWithFormat:@"select * from tblAttendees where active=='1' and events_id='%ld' and user_id='%@';",eventRealID,UserID];
                    
                    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
                    //            NSLog(@"db elements %@",dbresults);
                    
                    if(dbresults.count==0){
                        [self showsUIAlertWithMessage:@"Sorry. Please Subscribe for the event to access this feature" andTitle:@"Information"];
                        return NO;
                    }
                }
                
                if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                {
                    [self showsUIAlertWithMessageWithButtons:@"You need to be connected to the internet to access this feature" andTitle:@"Alert"];
                    return NO;
                }
                
            }
            if ([identifier isEqualToString:@"polling"]){
                NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
                if([Username isEqualToString:@"Guest"]){
                    [self showsUIAlertWithMessage:@"Sorry. Please Login to access this feature" andTitle:@"Information"];
                    return NO;
                }
                else{
                    NSString *query = [NSString stringWithFormat:@"select * from tblAttendees where active=='1' and events_id='%ld' and user_id='%@';",eventRealID,UserID];
                    
                    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
                    //            NSLog(@"db elements %@",dbresults);
                    
                    if(dbresults.count==0){
                        [self showsUIAlertWithMessage:@"Sorry. Please Subscribe for the event to access this feature" andTitle:@"Information"];
                        return NO;
                    }
                }
                
                if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
                {
                    [self showsUIAlertWithMessageWithButtons:@"You need to be connected to the internet to access this feature" andTitle:@"Alert"];
                    return NO;
                }
                
            }
        }
        
    }
    
    return YES;
}

#pragma CollectionView Methods
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return   arrEvents.count;
    
}

- (UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    EventSliderCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EventBannerSlider" forIndexPath:indexPath];
    //        NSString *imgname = @"LoadingImage.jpg";
    //
    //        cell.ImgEventChangeSlider.image = [UIImage imageNamed:imgname];
    cell.tag = indexPath.row;
    Dummy *object = [[Dummy alloc]init];
    Constants *cObj=[[Constants alloc]init];
    object= [arrEvents objectAtIndex:indexPath.row];
    
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            //              if (cell.tag ==indexPath.row) {
            
            NSString *urlpath=[NSString stringWithFormat:@"http://%@/%@",cObj.endpoint,object.eventImageURL];
            NSURL *url = [NSURL URLWithString:urlpath];
            //            NSData *data = [NSData dataWithContentsOfURL:url];
            
            //                UIImage *img = [[UIImage alloc] initWithData:data];
            [cell.ImgEventChangeSlider hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"LoadingImage.jpg"]];
            //            }
        });
    });
    
    
    return cell;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    EventID = indexPath.row;
}



-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(CGRectGetWidth(collectionView.frame)-2,CGRectGetHeight(collectionView.frame));
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView
                        layout:(UICollectionViewLayout *)collectionViewLayout
        insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 1, 0, 1);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 2;
}


#pragma -mark TableView Methods


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0){
        return 1;
    }
    if(section==1){
        if (sessions==0) {
            return 0;
        }
        return arrSessionTimesCut.count+1;
    }
    
    return 0;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.section==0){
        ChangeEventTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChangeEvent" forIndexPath:indexPath];
        EventSlider= cell.Collection;
        
        [cell.btnBlurPrev setHidden:true];
        [cell.btnBlurNext setHidden:true];
        
        [cell.btnPrgrammeSchedule addTarget:self action:@selector(btnShowSchedule:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.btnEventDetails addTarget:self action:@selector(btnEventDetailsClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        //        if (EventID==0){
        //            [cell.btnBlurPrev setHidden:true];
        //        }
        //        if (EventID==arrEvents.count-1){
        //            [cell.btnBlurNext setHidden:true];
        //        }
        return cell;
    }
    
    if (indexPath.section==1){
        
        if(indexPath.row==0){
            ScheduleHeaderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellScheduleHeader" forIndexPath:indexPath];
            //            [cell.btnSchedule addTarget:self action:@selector(btnShowSchedule:) forControlEvents:UIControlEventTouchUpInside];
            
            sessonDateToComp=@"";
            sessionDayNum=0;
            return cell;
        }
        
        if((indexPath.row>0 && indexPath.row<arrSessionTimesCut.count+1)){//start of If Alpha
            Session_Times *sessionObjecttest =[[Session_Times alloc]init];
            NSLog(@"indexpath is %ld",indexPath.row-1);
            
            sessionObjecttest = [arrSessionTimesCut objectAtIndex:indexPath.row-1];
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *sessionDate=[dateFormatter stringFromDate:sessionObjecttest.sessionDate];
            NSString *currentdate=[dateFormatter stringFromDate:[NSDate date]];
            NSString *test1 = [NSString stringWithFormat:@"%@ %@",sessionDate,sessionObjecttest.sessionStartTime];
            NSString *test2;
            if([self checkDateFormatis24Hour]==YES){
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                NSString *currentTime=[dateFormatter stringFromDate:[NSDate date]];
                //                NSDate *DateToBEConvertedTo12 = [dateFormatter dateFromString:currentTime1];
                //                [dateFormatter setDateFormat:@"hh:mm aa"];
                //                currentTime1=[dateFormatter stringFromDate:DateToBEConvertedTo12];
                NSDate *temp = [dateFormatter dateFromString:currentTime];
                NSDateFormatter *df=[[NSDateFormatter alloc] init];
                [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
                [df setDateFormat:@"yyyy-MM-dd hh:mm aa"];
                NSString *totalDate=[df stringFromDate:temp];
                [df setDateFormat:@"hh:mm aa"];
                currentTime=[df stringFromDate:temp];
                test2= [NSString stringWithFormat:@"%@ %@", currentdate,currentTime];
            }
            else{
                [dateFormatter setDateFormat:@"hh:mm aa"];
                NSString *currentTime=[dateFormatter stringFromDate:[NSDate date]];
                test2 = [NSString stringWithFormat:@"%@ %@", currentdate,currentTime];
            }
            
            
            NSString *hasDiscusFromObject=[NSString stringWithFormat:@"%@",sessionObjecttest.hasDiscussion];
            NSString *isDebateFromObject=[NSString stringWithFormat:@"%@",sessionObjecttest.isDebatable];
            
            if([sessonDateToComp length]==0){
                sessonDateToComp=[NSString stringWithFormat:@"%@",sessionDate];
                checkDay=true;
                sessionDayNum++;
            }
            else{
                
                if([self compareDateAndTimeIsEqualTo:sessonDateToComp :sessionDate]==TRUE){
                    checkDay=false;
                }
                else{
                    sessonDateToComp=[NSString stringWithFormat:@"%@",sessionDate];
                    checkDay=true;
                    sessionDayNum++;
                }
            }
            
            //session finished or done with
            if((![sessionObjecttest.sessionEndTime isEqual:@""]) && ([self compareDateAndTimeIsDecending:test2:test1]==TRUE)){
                if([hasDiscusFromObject isEqualToString:@"1"]){
                    
                    if(indexToScroll==-1){
                        SessionFinishedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellSessionFinish" forIndexPath:indexPath];
                        cell.tag=indexPath.row;
                        cell.btnRate.tag=indexPath.row;
                        cell.btnPolls.tag=indexPath.row;
                        cell.btnQuestions.tag=indexPath.row;
                        cell.lblDay.tag=indexPath.row;
                        
                        Session_Times *sessionObject =[[Session_Times alloc]init];
                        sessionObject = [arrSessionTimesCut objectAtIndex:indexPath.row-1];
                        cell.lblSessionName.text=sessionObject.sessionProgramme;
                        cell.lblStartTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionStartTime] ;
                        cell.lblFinishTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionEndTime];
                        //                        cell.lblDay.hidden=NO;
                        if([isDebateFromObject isEqualToString:@"1"]){
                            cell.btnPolls.hidden=NO;
                            cell.lblPoll.hidden=NO;
                        }
                        else{
                            cell.btnPolls.hidden=YES;
                            cell.lblPoll.hidden=YES;
                        }
                        cell.btnQuestions.hidden=NO;
                        cell.btnRate.hidden=NO;
                        cell.lblRate.hidden=NO;
                        cell.lblPanel.hidden=NO;
                        if([sessionObject.sessionSpeaker length]==0){
                            cell.lblSpeakerName.text=@"Speaker Name";
                        }else{
                            //                            NSLog(@"session speaker is %lu",(unsigned long)sessionObject.sessionSpeaker.length);
                            cell.lblSpeakerName.text=sessionObject.sessionSpeaker;
                        }
                        [cell.lblDay setHidden:true];
                        
                        for (int i=0; i<_dayIndexes.count; i++) {
                            
                            if (indexPath.row-1==[[_dayIndexes objectAtIndex:i] intValue]) {
                                cell.lblDay.text=[NSString stringWithFormat:@"Day %d", i+1];
                                [cell.lblDay setHidden:false];
                                //                                NSLog(@"indexpath is %ld dayindex is %d", indexPath.row -1, [[_dayIndexes objectAtIndex:i] intValue]);
                                
                            }
                        }
                        
                        //                        if(checkDay==true){
                        //                            if(cell.tag==cell.lblDay.tag){
                        //                            cell.lblDay.hidden=NO;
                        //                            cell.lblDay.text=[NSString stringWithFormat:@"Day %d",sessionDayNum];
                        //                            }
                        //                        }
                        //                        else{
                        //                            if(cell.tag==cell.lblDay.tag){
                        //                            cell.lblDay.hidden=YES;
                        //                            }
                        //                        }
                        
                        return cell;
                    }
                    else if(indexPath.row<indexToScroll+1){
                        SessionFinishedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellSessionFinish" forIndexPath:indexPath];
                        cell.tag=indexPath.row;
                        cell.btnRate.tag=indexPath.row;
                        cell.btnPolls.tag=indexPath.row;
                        cell.btnQuestions.tag=indexPath.row;
                        cell.lblDay.tag=indexPath.row;
                        Session_Times *sessionObject =[[Session_Times alloc]init];
                        sessionObject = [arrSessionTimesCut objectAtIndex:indexPath.row-1];
                        cell.lblSessionName.text=sessionObject.sessionProgramme;
                        cell.lblStartTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionStartTime] ;
                        cell.lblFinishTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionEndTime] ;
                        
                        if([isDebateFromObject isEqualToString:@"1"]){
                            cell.btnPolls.hidden=NO;
                            cell.lblPoll.hidden=NO;
                        }
                        else{
                            cell.btnPolls.hidden=YES;
                            cell.lblPoll.hidden=YES;
                        }
                        cell.btnQuestions.hidden=NO;
                        cell.btnRate.hidden=NO;
                        cell.lblRate.hidden=NO;
                        cell.lblPanel.hidden=NO;
                        if([sessionObject.sessionSpeaker length]==0){
                            cell.lblSpeakerName.text=@"";
                        }else{
                            //                            NSLog(@"session speaker is %lu",(unsigned long)sessionObject.sessionSpeaker.length);
                            cell.lblSpeakerName.text=sessionObject.sessionSpeaker;
                        }
                        
                        //                        if(checkDay==true){
                        //                            if(cell.tag==cell.lblDay.tag){
                        //                                cell.lblDay.hidden=NO;
                        //                                cell.lblDay.text=[NSString stringWithFormat:@"Day %d",sessionDayNum];
                        //                            }
                        //                        }
                        //                        else{
                        //                            if(cell.tag==cell.lblDay.tag){
                        //                                cell.lblDay.hidden=YES;
                        //                            }
                        //                        }
                        [cell.lblDay setHidden:true];
                        for (int i=0; i<_dayIndexes.count; i++) {
                            if (indexPath.row-1==[[_dayIndexes objectAtIndex:i] intValue]) {
                                cell.lblDay.text=[NSString stringWithFormat:@"Day %d", i+1];
                                [cell.lblDay setHidden:false];
                                //                                NSLog(@"indexpath is %ld dayindex is %d", indexPath.row -1, [[_dayIndexes objectAtIndex:i] intValue]);
                                
                            }
                        }
                        return cell;
                    }
                    
                }
                else{
                    if(indexToScroll==-1){
                        SessionFinishedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellSessionFinish" forIndexPath:indexPath];
                        cell.tag=indexPath.row;
                        cell.btnRate.tag=indexPath.row;
                        cell.btnPolls.tag=indexPath.row;
                        cell.btnQuestions.tag=indexPath.row;
                        cell.lblDay.tag=indexPath.row;
                        Session_Times *sessionObject =[[Session_Times alloc]init];
                        sessionObject = [arrSessionTimesCut objectAtIndex:indexPath.row-1];
                        cell.lblSessionName.text=sessionObject.sessionProgramme;
                        cell.lblStartTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionStartTime] ;
                        cell.lblFinishTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionEndTime] ;
                        //                            NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
                        if([isDebateFromObject isEqualToString:@"1"]){
                            cell.btnPolls.hidden=NO;
                            cell.lblPoll.hidden=NO;
                        }
                        else{
                            cell.btnPolls.hidden=YES;
                            cell.lblPoll.hidden=YES;
                        }
                        cell.btnQuestions.hidden=YES;
                        if([sessionObject.sessionSpeaker length]==0){
                            cell.btnRate.hidden=YES;
                            cell.lblRate.hidden=YES;
                        }
                        else{
                            cell.btnRate.hidden=NO;
                            cell.lblRate.hidden=NO;
                        }
                        
                        cell.lblPanel.hidden=YES;
                        
                        if([sessionObject.sessionSpeaker length]==0){
                            cell.lblSpeakerName.text=@"";
                        }else{
                            //                            NSLog(@"session speaker is %lu",(unsigned long)sessionObject.sessionSpeaker.length);
                            cell.lblSpeakerName.text=sessionObject.sessionSpeaker;
                        }
                        
                        //                        if(checkDay==true){
                        //                            if(cell.tag==cell.lblDay.tag){
                        //                                cell.lblDay.hidden=NO;
                        //                                cell.lblDay.text=[NSString stringWithFormat:@"Day %d",sessionDayNum];
                        //                            }
                        //                        }
                        //                        else{
                        //                            if(cell.tag==cell.lblDay.tag){
                        //                                cell.lblDay.hidden=YES;
                        //                            }
                        //                        }
                        [cell.lblDay setHidden:true];
                        for (int i=0; i<_dayIndexes.count; i++) {
                            
                            if (indexPath.row-1==[[_dayIndexes objectAtIndex:i] intValue]) {
                                cell.lblDay.text=[NSString stringWithFormat:@"Day %d", i+1];
                                [cell.lblDay setHidden:false];
                                //                                NSLog(@"indexpath r is %ld dayindex is %d", indexPath.row -1, [[_dayIndexes objectAtIndex:i] intValue]);
                                
                            }
                        }
                        return cell;
                    }
                    else if(indexPath.row<indexToScroll+1){
                        SessionFinishedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellSessionFinish" forIndexPath:indexPath];
                        cell.tag=indexPath.row;
                        cell.btnRate.tag=indexPath.row;
                        cell.btnPolls.tag=indexPath.row;
                        cell.btnQuestions.tag=indexPath.row;
                        cell.lblDay.tag=indexPath.row;
                        Session_Times *sessionObject =[[Session_Times alloc]init];
                        sessionObject = [arrSessionTimesCut objectAtIndex:indexPath.row-1];
                        cell.lblSessionName.text=sessionObject.sessionProgramme;
                        cell.lblStartTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionStartTime] ;
                        cell.lblFinishTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionEndTime] ;
                        //                            NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
                        if([isDebateFromObject isEqualToString:@"1"]){
                            cell.btnPolls.hidden=NO;
                            cell.lblPoll.hidden=NO;
                        }
                        else{
                            cell.btnPolls.hidden=YES;
                            cell.lblPoll.hidden=YES;
                        }
                        cell.btnQuestions.hidden=YES;
                        if([sessionObject.sessionSpeaker length]==0){
                            cell.btnRate.hidden=YES;
                            cell.lblRate.hidden=YES;
                        }
                        else{
                            cell.btnRate.hidden=NO;
                            cell.lblRate.hidden=NO;
                        }
                        
                        cell.lblPanel.hidden=YES;
                        
                        if([sessionObject.sessionSpeaker length]==0){
                            cell.lblSpeakerName.text=@"";
                        }else{
                            //                            NSLog(@"session speaker is %lu",(unsigned long)sessionObject.sessionSpeaker.length);
                            cell.lblSpeakerName.text=sessionObject.sessionSpeaker;
                        }
                        //                        if(checkDay==true){
                        //                            if(cell.tag==cell.lblDay.tag){
                        //                                cell.lblDay.hidden=NO;
                        //                                cell.lblDay.text=[NSString stringWithFormat:@"Day %d",sessionDayNum];
                        //                            }
                        //                        }
                        //                        else{
                        //                            if(cell.tag==cell.lblDay.tag){
                        //                                cell.lblDay.hidden=YES;
                        //                            }
                        //                        }
                        [cell.lblDay setHidden:true];
                        for (int i=0; i<_dayIndexes.count; i++) {
                            
                            if (indexPath.row-1==[[_dayIndexes objectAtIndex:i] intValue]) {
                                cell.lblDay.text=[NSString stringWithFormat:@"Day %d", i+1];
                                [cell.lblDay setHidden:false];
                                //                                NSLog(@"indexpath is %ld dayindex is %d", indexPath.row -1, [[_dayIndexes objectAtIndex:i] intValue]);
                                
                            }
                        }
                        
                        return cell;
                    }
                    
                }
            }//end beta
            
            if(indexPath.row==indexToScroll+1 && (!(indexToScroll==-1))){
                if([hasDiscusFromObject isEqualToString:@"1"]){
                    SessionStartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellSessionStart" forIndexPath:indexPath];
                    cell.tag=indexPath.row;
                    cell.lblDay.tag=indexPath.row;
                    Session_Times *sessionObject =[[Session_Times alloc]init];
                    sessionObject = [arrSessionTimesCut objectAtIndex:indexPath.row-1];
                    cell.lblSessionName.text=sessionObject.sessionProgramme;
                    cell.lblStartTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionStartTime] ;
                    cell.lblFinishTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionEndTime] ;
                    if([isDebateFromObject isEqualToString:@"1"]){
                        cell.btnPolling.hidden=NO;
                        cell.lblPolls.hidden=NO;
                    }
                    else{
                        cell.btnPolling.hidden=YES;
                        cell.lblPolls.hidden=YES;
                    }
                    cell.btnForum.hidden=NO;
                    //                    cell.btnPolling.hidden=NO;
                    cell.lblPanel.hidden=NO;
                    //                    cell.lblPolls.hidden=NO;
                    cell.btnPolling.tag=indexPath.row;
                    cell.btnForum.tag=indexPath.row;
                    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
                    if([Username isEqualToString:@"Guest"]){
                        [cell.btnForum addTarget:self action:@selector(btnForumAction) forControlEvents:UIControlEventTouchUpInside];
                    }
                    
                    cell.lblFinishTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionEndTime] ;
                    if([sessionObject.sessionSpeaker length]==0){
                        cell.lblSpeakerName.text=@"";
                    }else{
                        //                        NSLog(@"session speaker is %lu",(unsigned long)sessionObject.sessionSpeaker.length);
                        cell.lblSpeakerName.text=sessionObject.sessionSpeaker;
                    }
                    
                    //                    if(checkDay==true ){
                    //                        if(cell.tag==cell.lblDay.tag){
                    //                            cell.lblDay.hidden=NO;
                    //                            cell.lblDay.text=[NSString stringWithFormat:@"Day %d",sessionDayNum];
                    //                        }
                    //                    }
                    //                    else{
                    //                        if(cell.tag==cell.lblDay.tag){
                    //                            cell.lblDay.hidden=YES;
                    //                        }
                    //                    }
                    [cell.lblDay setHidden:true];
                    for (int i=0; i<_dayIndexes.count; i++) {
                        
                        if (indexPath.row-1==[[_dayIndexes objectAtIndex:i] intValue]) {
                            cell.lblDay.text=[NSString stringWithFormat:@"Day %d", i+1];
                            [cell.lblDay setHidden:false];
                            //                            NSLog(@"indexpath is %ld dayindex is %d", indexPath.row -1, [[_dayIndexes objectAtIndex:i] intValue]);
                            
                            
                        }
                    }
                    return cell;
                }
                else{
                    SessionStartTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellSessionStart" forIndexPath:indexPath];
                    cell.tag=indexPath.row;
                    cell.lblDay.tag=indexPath.row;
                    Session_Times *sessionObject =[[Session_Times alloc]init];
                    sessionObject = [arrSessionTimesCut objectAtIndex:indexPath.row-1];
                    cell.lblSessionName.text=sessionObject.sessionProgramme;
                    cell.lblStartTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionStartTime] ;
                    cell.lblFinishTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionEndTime] ;
                    if([isDebateFromObject isEqualToString:@"1"]){
                        cell.btnPolling.hidden=NO;
                        cell.lblPolls.hidden=NO;
                    }
                    else{
                        cell.btnPolling.hidden=YES;
                        cell.lblPolls.hidden=YES;
                    }
                    cell.btnForum.hidden=YES;
                    //                    cell.btnPolling.hidden=YES;
                    cell.lblPanel.hidden=YES;
                    //                    cell.lblPolls.hidden=YES;
                    cell.btnPolling.tag=indexPath.row;
                    cell.btnForum.tag=indexPath.row;
                    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
                    if([Username isEqualToString:@"Guest"]){
                        [cell.btnForum addTarget:self action:@selector(btnForumAction) forControlEvents:UIControlEventTouchUpInside];
                    }
                    
                    cell.lblFinishTime.text=[NSString stringWithFormat:@"%@",sessionObject.sessionEndTime] ;
                    if([sessionObject.sessionSpeaker length]==0){
                        cell.lblSpeakerName.text=@"";
                    }else{
                        //                        NSLog(@"session speaker is %lu",(unsigned long)sessionObject.sessionSpeaker.length);
                        cell.lblSpeakerName.text=sessionObject.sessionSpeaker;
                    }
                    
                    //                    if(checkDay==true ){
                    //                        if(cell.tag==cell.lblDay.tag){
                    //                            cell.lblDay.hidden=NO;
                    //                            cell.lblDay.text=[NSString stringWithFormat:@"Day %d",sessionDayNum];
                    //                        }
                    //                    }
                    //                    else{
                    //                        if(cell.tag==cell.lblDay.tag){
                    //                            cell.lblDay.hidden=YES;
                    //                        }
                    //                    }
                    [cell.lblDay setHidden:true];
                    for (int i=0; i<_dayIndexes.count; i++) {
                        
                        if (indexPath.row-1==[[_dayIndexes objectAtIndex:i] intValue]) {
                            cell.lblDay.text=[NSString stringWithFormat:@"Day %d", i+1];
                            [cell.lblDay setHidden:false];
                            //                            NSLog(@"indexpath is %ld dayindex is %d", indexPath.row -1, [[_dayIndexes objectAtIndex:i] intValue]);
                            
                            
                        }
                    }
                    return cell;
                }
                
            }//end beta 2
            
            
            Session_Times *sessionObject =[[Session_Times alloc]init];
            sessionObject = [arrSessionTimesCut objectAtIndex:indexPath.row-1];
            SessionUpcomingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellSessionUpcoming" forIndexPath:indexPath];
            cell.tag=indexPath.row;
            cell.lblDay.tag=indexPath.row;
            NSString *endTime=[[NSString alloc]initWithFormat:@"%@",sessionObject.sessionEndTime];
            if([endTime length]==0 && cell.tag==indexPath.row){
                cell.lblStartTime.hidden=YES;
                cell.lblFinishTime.hidden=YES;
                cell.lblIndicator.hidden=YES;
                cell.lblSpeakerName.hidden=YES;
                cell.lblSessionName.hidden=YES;
                cell.lblSessionTitle.hidden=NO;
                cell.lblSessionTitle.text=sessionObject.sessionProgramme;
                
                //                cell.lblSessionTitle.textColor=[UIColor blueColor];
            }
            else if(cell.tag==indexPath.row){
                cell.lblStartTime.hidden=NO;
                cell.lblFinishTime.hidden=NO;
                cell.lblIndicator.hidden=NO;
                cell.lblSpeakerName.hidden=NO;
                cell.lblSessionName.hidden=NO;
                cell.lblSessionTitle.hidden=YES;
                //                cell.lblSessionName.textColor=[UIColor colorWithRed:25/255.0 green:181/255.0 blue:255/255.0 alpha:1.0];
                cell.lblStartTime.text= [[NSString alloc] initWithFormat:@"%@", sessionObject.sessionStartTime ];
                cell.lblFinishTime.text=[[NSString alloc] initWithFormat:@"%@", sessionObject.sessionEndTime ];
                if([sessionObject.sessionSpeaker length]==0){
                    cell.lblSpeakerName.text=@"";
                }else{
                    cell.lblSpeakerName.text=sessionObject.sessionSpeaker;
                }
                cell.lblSessionName.text=sessionObject.sessionProgramme;
            }
            //            if(checkDay==true ){
            //                if(cell.tag==cell.lblDay.tag){
            //                    cell.lblDay.hidden=NO;
            //
            //                    cell.lblDay.text=[NSString stringWithFormat:@"Day %d",sessionDayNum];
            //                }
            //            }
            //            else{
            //                if(cell.tag==cell.lblDay.tag){
            //                    cell.lblDay.hidden=YES;
            //                }
            //            }
            [cell.lblDay setHidden:true];
            for (int i=0; i<_dayIndexes.count; i++) {
                
                if (indexPath.row-1==[[_dayIndexes objectAtIndex:i] intValue]) {
                    cell.lblDay.text=[NSString stringWithFormat:@"Day %d", i+1];
                    //                    NSLog(@"indexpath is %ld dayindex is %d", indexPath.row -1, [[_dayIndexes objectAtIndex:i] intValue]);
                    
                    [cell.lblDay setHidden:false];
                    
                }
            }
            return cell;
        }//end of if alpha
        //        if(indexPath.row==arrSessionTimesCut.count+1){
        //            ScheduleFooterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellScheduleFooter" forIndexPath:indexPath];
        //            return cell;
        //        }
        
    }//end of section 1
    
    return 0;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row==0 && indexPath.section==0){
        return 502;
    }
    return UITableViewAutomaticDimension;
}

//-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
//    long h;
//}


//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    for (UICollectionViewCell *cell in [EventSlider visibleCells]) {
//        NSIndexPath *indexPath = [EventSlider indexPathForCell:cell];
////        NSLog(@"index visible is ---->>>%lu",indexPath.row);
//        index = indexPath;
//    }
//    Dummy *Dobj = [[Dummy alloc]init];
//    Dobj=[arrEvents objectAtIndex:index.row];
//
//    //        _lblBanner.textColor=[UIColor blueColor];
//
//    if(value==Dobj.eventName){
//        if(EventID==0){
//            //                SWRevealViewController * vc = self.revealViewController;
//            //                [self presentViewController:vc animated:NO completion:nil];
//            //load reveal view controller here
//
//        }
//    } else{
//
//        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",Dobj.eventID] forKey:@"EventRealID"];
//        eventRealID=[Dobj.eventID longLongValue];
//
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        value = Dobj.eventName;
//
//        _lblEventHeading.text=Dobj.eventName;
//
//
//
//        //NSLog(@"collection index=%ld",(long)index.row);
//
//        NSString *MainEventID=[[NSString alloc]initWithFormat:@"%@",Dobj.eventID];
//        NSString *ObjectEventID;
//        arrSessionTimesCut=[[NSMutableArray alloc]init];
//        if(arrSessionTimes.count>0){
//            for (Session_Times *obj in arrSessionTimes){
//                ObjectEventID=[[NSString alloc]initWithFormat:@"%@",obj.eventID];
//                if([MainEventID isEqualToString:ObjectEventID]){
//                    [arrSessionTimesCut addObject:obj];
//                }
//            }
//        }
//        NSLog(@"______________________________arrsessioncut______________________");
//        NSLog(@"%@",arrSessionTimesCut);
//        [self setDayIndexes];
//        [_tblEvents reloadData];
//    }
//    if(!(EventID==index.row)){
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        [_tblEvents scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
//    }
//    EventID=index.row;
//
//    [[NSUserDefaults standardUserDefaults] setValue:value forKey:@"EventName"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//
//    [[NSUserDefaults standardUserDefaults] setInteger:EventID forKey:@"EventID"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//
//    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",Dobj.eventID] forKey:@"EventRealID"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//
//    [[NSUserDefaults standardUserDefaults] setInteger:EventID forKey:@"EventChange"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//
//
//}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.section==1){
        [self performSegueWithIdentifier:@"rate" sender:indexPath];
    }
    
}

-(void)viewWillAppear:(BOOL)animated{
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    
    
    
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    //        AttendeesJsonDownload *AttendeesDownloads = [[AttendeesJsonDownload alloc]init];
    //        [AttendeesDownloads setDelegate:(id)self];
    //
    //        [AttendeesDownloads OrganizeDataFromJsonToObject];
    //        //
    //        //    //Speakers
    //        SpeakersJsonDownload *SpeakersDownloads = [[SpeakersJsonDownload alloc]init];
    //        [SpeakersDownloads setDelegate:(id)self];
    //
    //        [SpeakersDownloads OrganizeDataFromJsonToObject];
    //        //
    //        //    //Sponsors
    //        SponsorJsonDownload *SponsorsDownloads = [[SponsorJsonDownload alloc]init];
    //        [SponsorsDownloads setDelegate:(id)self];
    //
    //        [SponsorsDownloads OrganizeDataFromJsonToObject];
    
    //        EventJsonDownload *eventDownloaded = [[EventJsonDownload alloc]init];
    //        [eventDownloaded setDelegate:(id)self];
    //
    //        [eventDownloaded OrganizeDataFromJsonToObject];
    
    sessions=0;
    eventMenuChange=[[NSUserDefaults standardUserDefaults] integerForKey:@"EventChange"];
    if(eventMenuChange==EventID){
        
    }
    else{
        
        EventID=eventMenuChange;
        
        Dummy * Dobj=[[Dummy alloc]init];
        
        Dobj = [arrEvents objectAtIndex:EventID];
        
        value=Dobj.eventName;
        eventRealID=[Dobj.eventID longLongValue];
        _lblEventHeading.text=Dobj.eventName;
        NSString *MainEventID=[[NSString alloc]initWithFormat:@"%@",Dobj.eventID];
        NSString *ObjectEventID;
        arrSessionTimesCut=[[NSMutableArray alloc]init];
        if(arrSessionTimes.count>0){
            for (Session_Times *obj in arrSessionTimes){
                ObjectEventID=[[NSString alloc]initWithFormat:@"%@",obj.eventID];
                if([MainEventID isEqualToString:ObjectEventID]){
                    [arrSessionTimesCut addObject:obj];
                }
            }
        }
        [[NSUserDefaults standardUserDefaults] setValue:value forKey:@"EventName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setInteger:EventID forKey:@"EventID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",Dobj.eventID] forKey:@"EventRealID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        //         [self getDownloadedDataFromDB];
        
        
        
        [self setDayIndexes];
        [_tblEvents reloadData];
        [EventSlider scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:EventID inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        
        
        
        
    }
    EventSlider.scrollEnabled = NO;
    
}


- (IBAction)btnNext:(id)sender {
    NSIndexPath *indexpath;
    
    for (UICollectionViewCell *cell in [EventSlider visibleCells]) {
        NSIndexPath *indexPath = [EventSlider indexPathForCell:cell];
        indexpath=indexPath;
    }
    long path;
    path=(long)indexpath.row;
    Dummy *Dobj=[[Dummy alloc]init];
    if(path<arrEvents.count-1){
        path=(long)indexpath.row+1;
        
        [EventSlider scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:path inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        NSLog(@"the path index is : %ld",path);
        EventID=path;
        
        Dobj=[arrEvents objectAtIndex:path];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",Dobj.eventID] forKey:@"EventRealID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        value=Dobj.eventName;
        eventRealID=[Dobj.eventID longLongValue];
        _lblEventHeading.text=Dobj.eventName;
        NSString *MainEventID=[[NSString alloc]initWithFormat:@"%@",Dobj.eventID];
        NSString *ObjectEventID;
        arrSessionTimesCut=[NSMutableArray array];
        for (Session_Times *obj in arrSessionTimes){
            ObjectEventID=[[NSString alloc]initWithFormat:@"%@",obj.eventID];
            if([MainEventID isEqualToString:ObjectEventID]){
                [arrSessionTimesCut addObject:obj];
            }
        }
        [self setDayIndexes];
        [_tblEvents reloadData];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [_tblEvents scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:@"EventName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:EventID forKey:@"EventID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",Dobj.eventID] forKey:@"EventRealID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:EventID forKey:@"EventChange"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    NSLog(@"btnNext clicked");
    
    
    
}

- (IBAction)btnPrevious:(id)sender {
    NSIndexPath *indexpath;
    for (UICollectionViewCell *cell in [EventSlider visibleCells]) {
        NSIndexPath *indexPath = [EventSlider indexPathForCell:cell];
        indexpath=indexPath;
    }
    long path;
    path=(long)indexpath.row;
    Dummy *Dobj=[[Dummy alloc]init];
    
    if(path>0){
        path=(long)indexpath.row-1;
        [EventSlider scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:path inSection:0] atScrollPosition:UICollectionViewScrollPositionLeft animated:YES];
        NSLog(@"the path index is : %ld",path);
        EventID=path;
        
        Dobj=[arrEvents objectAtIndex:path];
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",Dobj.eventID] forKey:@"EventRealID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        value=Dobj.eventName;
        eventRealID=[Dobj.eventID longLongValue];
        _lblEventHeading.text=Dobj.eventName;
        NSString *MainEventID=[[NSString alloc]initWithFormat:@"%@",Dobj.eventID];
        NSString *ObjectEventID;
        arrSessionTimesCut=[NSMutableArray array];
        for (Session_Times *obj in arrSessionTimes){
            ObjectEventID=[[NSString alloc]initWithFormat:@"%@",obj.eventID];
            if([MainEventID isEqualToString:ObjectEventID]){
                [arrSessionTimesCut addObject:obj];
            }
        }
        [self setDayIndexes];
        [_tblEvents reloadData];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [_tblEvents scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    [[NSUserDefaults standardUserDefaults] setValue:value forKey:@"EventName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:EventID forKey:@"EventID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",Dobj.eventID] forKey:@"EventRealID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [[NSUserDefaults standardUserDefaults] setInteger:EventID forKey:@"EventChange"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSLog(@"btnPrevious clicked");
    
}

- (void)refreshLabel
{
    
    if(arrSessionTimesCut.count>0){
        int i=0;
        int check=-1;
        int checkasc=0;
        //        NSDateFormatter *dateFormatter1=[[NSDateFormatter alloc] init];
        //        [dateFormatter1 setDateFormat:@"hh:mm aa"];
        //            NSDate *compareCurrentTime =[dateFormatter1 dateFromString:currentTime];
        
        for(Session_Times *sessionObj in arrSessionTimesCut){
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *currentdate=[dateFormatter stringFromDate:[NSDate date]];
            NSString *sessionDate =[dateFormatter stringFromDate:sessionObj.sessionDate];
            NSString *currentTime1=[[NSString alloc]init];
            if([self checkDateFormatis24Hour]==YES){
                //                currentTime1=[[NSString alloc]init];
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                currentTime1=[dateFormatter stringFromDate:[NSDate date]];
                //                NSDate *DateToBEConvertedTo12 = [dateFormatter dateFromString:currentTime1];
                //                [dateFormatter setDateFormat:@"hh:mm aa"];
                //                currentTime1=[dateFormatter stringFromDate:DateToBEConvertedTo12];
                NSDate *temp = [dateFormatter dateFromString:currentTime1];
                NSDateFormatter *df=[[NSDateFormatter alloc] init];
                [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
                [df setDateFormat:@"yyyy-MM-dd hh:mm aa"];
                NSString *totalDate=[df stringFromDate:temp];
                [df setDateFormat:@"hh:mm aa"];
                currentTime1=[df stringFromDate:temp];
                
                
                
                NSLog(@"----->>%@",currentTime1);
            }
            else{
                [dateFormatter setDateFormat:@"hh:mm aa"];
                currentTime1=[dateFormatter stringFromDate:[NSDate date]];
                //                currentTime = [NSString stringWithFormat:@"%@ %@", currentdate,currentTime];
            }
            //            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            //            [formatter setDateFormat:@"yyyy-MM-dd hh:mm aa"];
            
            NSString *time1 = [NSString stringWithFormat:@"%@ %@", currentdate,currentTime1];
            //            [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
            NSDate *TodayDateTime=[dateFormatter dateFromString:time1];
            NSString *time2 = [NSString stringWithFormat:@"%@ %@", sessionDate,sessionObj.sessionStartTime];
            
            //-------------------------------------------------------------
            if([self compareDateAndTimeIsDecending:time1 :time2]==TRUE){
                check=i;
                indexToScrollAsc=FALSE;
            }
            //                else{
            //                    check=0;
            //                }
            
            i=i+1;
            
        }
        if(check>-1){
            //            check=10;
            Session_Times *testObj=[arrSessionTimesCut objectAtIndex:check];
            //            NSLog(@"arr has => %@ => %@",[arrSessionTimesCut objectAtIndex:check], testObj.sessionProgramme);
            NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSString *currentdate=[dateFormatter stringFromDate:[NSDate date]];
            NSString *sessionDate=[dateFormatter stringFromDate:testObj.sessionDate];
            NSString *test1 = [NSString stringWithFormat:@"%@ %@", sessionDate,testObj.sessionEndTime];
            
            NSString *currentTime;
            if([self checkDateFormatis24Hour]==YES){
                NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
                currentTime=[dateFormatter stringFromDate:[NSDate date]];
                //                NSDate *DateToBEConvertedTo12 = [dateFormatter dateFromString:currentTime1];
                //                [dateFormatter setDateFormat:@"hh:mm aa"];
                //                currentTime1=[dateFormatter stringFromDate:DateToBEConvertedTo12];
                NSDate *temp = [dateFormatter dateFromString:currentTime];
                NSDateFormatter *df=[[NSDateFormatter alloc] init];
                [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
                [df setDateFormat:@"yyyy-MM-dd hh:mm aa"];
                NSString *totalDate=[df stringFromDate:temp];
                [df setDateFormat:@"hh:mm aa"];
                currentTime=[df stringFromDate:temp];
                
                
                NSLog(@"----->>%@",currentTime);
            }
            else{
                [dateFormatter setDateFormat:@"hh:mm aa"];
                currentTime=[dateFormatter stringFromDate:[NSDate date]];
                //                currentTime = [NSString stringWithFormat:@"%@ %@", currentdate,currentTime];
            }
            
            NSString *test2 = [NSString stringWithFormat:@"%@ %@", currentdate,currentTime];
            
            if([self compareDateAndTimeIsDecending:test2:test1]==TRUE){
                check=-1;
                indexToScrollAsc=TRUE;
                //                NSLog(@"value of IndexToScroll => %ld",arrSessionTimesCut.count);
            }
            
        }
        
        
        if(check==indexToScroll){
            
        }
        else{
            if(check==-1){
                indexToScroll=-1;
                
            }
            else{
                indexToScroll=check;
            }
            checkLoop=true;
            [_tblEvents reloadData];
            
        }
        
        [self performSelector:@selector(refreshLabel) withObject:nil afterDelay:delay];
    }
    //    });
    
    
    
}

-(BOOL)compareDateAndTimeIsDecending:(NSString*) stringDate1 : (NSString*) stringDate2{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm aa"];
    NSDate *date1= [formatter dateFromString:stringDate1];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm aa"];
    NSDate *date2 = [formatter dateFromString:stringDate2];
    
    NSComparisonResult result = [date1 compare:date2];
    if(result == NSOrderedDescending)
    {
        return TRUE;
    }
    else if(result == NSOrderedAscending)
    {
    }
    else
    {
        return TRUE;
    }
    return FALSE;
}

-(BOOL)compareDateAndTimeIsDecending2:(NSString*) stringDate1 : (NSString*) stringDate2{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"]];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm aa"];
    NSDate *date1= [formatter dateFromString:stringDate1];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm aa"];
    NSDate *date2 = [formatter dateFromString:stringDate2];
    
    NSComparisonResult result = [date1 compare:date2];
    if(result == NSOrderedDescending)
    {
        return TRUE;
    }
    else if(result == NSOrderedAscending)
    {
    }
    else
    {
    }
    return FALSE;
}
-(BOOL)compareDateAndTimeIsEqualTo:(NSString*) stringDate1 : (NSString*) stringDate2{
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *date1= [formatter dateFromString:stringDate1];
    //    NSLog(@"%@",date1);
    NSDate *date2 = [formatter dateFromString:stringDate2];
    //                NSLog(@"%@",date2);
    NSComparisonResult result = [date1 compare:date2];
    if(result == NSOrderedDescending)
    {
        //        NSLog(@"desending session ID => %@currenttime :%@  - session%@",sessionObj.sessionID, date1, date2);
        //        check= i;
        
    }
    else if(result == NSOrderedAscending)
    {
        //        NSLog(@"assending session ID => %@currenttime :%@  - session%@",sessionObj.sessionID, date1, date2);
        //        checkasc=i;
    }
    else
    {
        //        NSLog(@"session ID => %@currenttime :%@  - session%@",sessionObj.sessionID, date1, date2);
        return TRUE;
    }
    
    return FALSE;
}

-(BOOL)checkDateFormatis24Hour{
    NSDateFormatter *checkformatter = [[NSDateFormatter alloc] init];
    [checkformatter setLocale:[NSLocale currentLocale]];
    [checkformatter setDateStyle:NSDateFormatterNoStyle];
    [checkformatter setTimeStyle:NSDateFormatterShortStyle];
    NSString *dateString = [checkformatter stringFromDate:[NSDate date]];
    NSRange amRange = [dateString rangeOfString:[checkformatter AMSymbol]];
    NSRange pmRange = [dateString rangeOfString:[checkformatter PMSymbol]];
    BOOL is24h = (amRange.location == NSNotFound && pmRange.location == NSNotFound);
    //    NSLog(@"%@\n",(is24h ? @"YES" : @"NO"));
    
    return  is24h;
}

-(void)btnRateAction{
    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    if([Username isEqualToString:@"Guest"]){
        [self showsUIAlertWithMessage:@"Sorry. Please Login to access this feature" andTitle:@"Information"];
    }
}

-(void)btnPollsAction{
    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    if([Username isEqualToString:@"Guest"]){
        [self showsUIAlertWithMessage:@"Sorry. Please Login to access this feature" andTitle:@"Information"];
    }
}

-(void)btnForumAction{
    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    if([Username isEqualToString:@"Guest"]){
        [self showsUIAlertWithMessage:@"Sorry. Please Login to access this feature" andTitle:@"Information"];
    }
}

#pragma -mark UIAlert Box
- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}
- (void)showsUIAlertWithMessageWithButtons:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings",nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        NSLog(@"Cancel button clicked");
    }
    if (buttonIndex == 1){
        NSLog(@"settings button clicked");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-prefs:"]];
    }
}
#pragma -mark Day splitter
-(void)setDayIndexes{
    
    //    Session_Times *sessionObjecttest =[[Session_Times alloc]init];
    int i = 0;
    _dayIndexes = [[NSMutableArray alloc]init];
    NSDate *eventStart;
    long int dayNum= -1;
    for (Session_Times *session in arrSessionTimesCut) {
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd"];
        NSString *sessionDate=[dateFormatter stringFromDate:session.sessionDate];
        //        NSLog(@"date is %@", sessionDate);
        if (i==0) {
            eventStart = session.sessionDate;
        }
        NSDate *fromDate;
        NSDate *toDate;
        
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                     interval:NULL forDate:eventStart];
        [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                     interval:NULL forDate:session.sessionDate];
        
        NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                                   fromDate:fromDate toDate:toDate options:0];
        
        NSLog(@"%ld", (long)[difference day]);
        if (dayNum<[difference day]) {
            NSLog(@"its different at index %d",i);
            
            [_dayIndexes addObject:[NSNumber numberWithInt:i]];
            dayNum=[difference day];
        }
        
        i++;
    }
    
}
-(void)getSessions{
    Dummy * Dobj=[[Dummy alloc]init];
    
    Dobj = [arrEvents objectAtIndex:EventID];
    
    value=Dobj.eventName;
    NSString *MainEventID=[[NSString alloc]initWithFormat:@"%@",Dobj.eventID];
    NSString *ObjectEventID;
    arrSessionTimesCut=[NSMutableArray array];
    for (Session_Times *obj in arrSessionTimes){
        ObjectEventID=[[NSString alloc]initWithFormat:@"%@",obj.eventID];
        if([MainEventID isEqualToString:ObjectEventID]){
            [arrSessionTimesCut addObject:obj];
        }
    }
    sessions =1;
    
    [_tblEvents reloadData];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    [_tblEvents scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
}
- (void)btnShowSchedule:(id)sender{
    
    [self getSessions];
}
-(void)btnEventDetailsClicked:(id)sender{
    NSLog(@"Event details button clicked");
    [self performSegueWithIdentifier:@"idEventDetails" sender:self];
}
@end




