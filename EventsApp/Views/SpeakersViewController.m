//
//  SpeakersViewController.m
//  FIAEventsApp
//
//  Created by temp on 08/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "SpeakersViewController.h"
#import "SpeakersCollectionViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "SWRevealViewController.h"
#import "Speakers.h"
#import "SpeakersJSONDownload.h"
#import "Constants.h"
#import "SpeakerDetailsViewController.h"
#import "Haneke.h"
#import "DBManager.h"
#import "Reachability.h"

@interface SpeakersViewController ()
{
    
    NSMutableArray *arrSpeakersCutObjects;
    NSArray *arrSpeakersCompany;
    
    NSString *EventRealID;
    NSNumber *RealEventID;
    NSNumber *objEventID;
  
    NSMutableArray *arrSpeakersObjects;
     SpeakersJsonDownload *SpeakerDownloads;
    UIRefreshControl *refreshControl;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnDrawer;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;
@property (nonatomic, strong) DBManager *dbManager;
@end

@implementation SpeakersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _overlayView.hidden=NO;
    // Do any additional setup after loading the view.
    
    Constants *obj=[[Constants alloc]init];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:obj.dbName];
    [self getSpeakerDownloadDataFromDB];

    
    refreshControl = [[UIRefreshControl alloc]init];
    [self.SpeakerCollection addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    [_lblTitle.layer setCornerRadius:17.0f];
    
    // border
//    [_lblTitle.layer setBorderColor:[UIColor whiteColor].CGColor];
//    [_lblTitle.layer setBorderWidth:0.9f];
    
    [self.btnDrawer setTarget:self.revealViewController];
    [self.btnDrawer setAction:@selector(revealToggle: )];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
     //[[self navigationController] setNavigationBarHidden:NO animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Refresh Speaker Data
-(void)refreshTable{
    [refreshControl endRefreshing];
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
         [self showsUIAlertWithMessageWithButtons:@"Content cannot be Updated. No Internet Connection Avaliable" andTitle:@"Information"];
    }
    else
    {
        Reachability* reachability = [Reachability reachabilityWithHostName: @"www.google.com"];
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        NSLog(@"the nertwok is %ld", (long)netStatus);
        if((long)netStatus>0){
             [self UpdateSpeakerData];
        }
        else{
            [self showsUIAlertWithMessage:@"Oh!No.Your internet connection is not stable. Please Check Internet connection and Try Again." andTitle:@"Alert"];
        }
       
//        [self.SpeakerCollection reloadData];
//         [self showsUIAlertWithMessageWithButtons:@"Content has been Updated" andTitle:@"Success"];
    }
    }
   

-(void)UpdateSpeakerData{
    SpeakerDownloads = [[SpeakersJsonDownload alloc]init];
    [SpeakerDownloads setDelegate:(id)self];
    NSString *BoolDBLock =[[NSUserDefaults standardUserDefaults] stringForKey:@"BoolDBLock"];
    if(![BoolDBLock isEqualToString:@"YES"]){
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"BoolDBLock"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
       [SpeakerDownloads OrganizeDataFromJsonToObject];
    }
    
    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//     Get the new view controller using [segue destinationViewController].
//     Pass the selected object to the new view controller.
    
    if ([[segue identifier] isEqualToString:@"speakerDetails"])
    {
        
        // Get reference to the destination view controller
        SpeakerDetailsViewController *vc = [segue destinationViewController];
        NSIndexPath *indexPath = (NSIndexPath*) sender;
       
            Speakers *objectToBeSent=[[Speakers alloc]init];
            NSLog(@"indexpath row is %ld", (long)indexPath.row);
            objectToBeSent = [arrSpeakersCutObjects objectAtIndex:indexPath.row];
            NSLog(@"%@",objectToBeSent.speakerPDFUrl);
            [vc setSpeakerUrl:objectToBeSent.speakerPDFUrl andName:[NSString stringWithFormat:@"%@ %@",objectToBeSent.speakerFirstName,objectToBeSent.speakerLastName]];
       
        
    }
}

-(void)getSpeakerDownloadDataFromDB{
    
    NSString *query = [NSString stringWithFormat:@"select * from tblSpeakers where active=='1'"];
    
    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
    NSLog(@"db elements %@",dbresults);
    
    NSMutableArray *arrGetSpeaker=[[NSMutableArray alloc]init];
    for(int i=0;i<dbresults.count;i++){
        Speakers *obj =[[Speakers alloc]init];
        
        obj.lastmodified=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
        obj.speakerFirstName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"first_name"]];
        obj.speakerLastName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"last_name"]];
      obj.speakerPicURL=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"picture"]];
        obj.speakerCompany=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"company_name"]];
        obj.speakerEventID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"eventid"]];
        obj.speakerPDFUrl=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"url"]];
        obj.speakerPosition=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"designation"]];
        obj.active=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"active"]];
        obj.lastmodified=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
        obj.speakerID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"speakerid"]];
        
        [arrGetSpeaker addObject:obj];
    
    }
    
    arrSpeakersObjects=[[NSMutableArray alloc]init];
    arrSpeakersObjects=arrGetSpeaker;
    
    
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    EventRealID =[[NSUserDefaults standardUserDefaults] stringForKey:@"EventRealID"];
    RealEventID=[f numberFromString:EventRealID];
    arrSpeakersCutObjects=[NSMutableArray array];
    for(Speakers *speakerObj in arrSpeakersObjects){
        NSLog(@"%@",speakerObj.speakerEventID);
        NSString *SpeakerId=[[NSString alloc]initWithFormat:@"%@",speakerObj.speakerEventID];
        objEventID=[f numberFromString:SpeakerId];
        
        if([objEventID isEqualToValue:RealEventID]){
            [arrSpeakersCutObjects addObject:speakerObj];
            
        }
        
    }
    NSLog(@"----->>%lu",(unsigned long)arrSpeakersCutObjects.count);
    [_SpeakerCollection reloadData];
    
    if(arrSpeakersCutObjects.count==0){
        _lblStatus.text=@"No Speakers were Found";
        [_actIndicator stopAnimating];
    }
    else{
    _overlayView.hidden=YES;
    }
}


-(void)getSpeakersDownload:(NSMutableArray *)speakers{
    
    @try {
        NSString *clearquery = [NSString stringWithFormat:@"delete from tblSpeakers"];
        //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
        
        [self.dbManager executeQuery:clearquery];
        
        
        //    // If the query was successfully executed then pop the view controller.
        if (self.dbManager.affectedRows != 0) {
            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
        }
        else{
            NSLog(@"Could not execute the query.");
        }
        for(Speakers *dbSpeaker in speakers){
            dbSpeaker.speakerFirstName=[ dbSpeaker.speakerFirstName stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            dbSpeaker.speakerCompany=[ dbSpeaker.speakerCompany stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblSpeakers (first_name,last_name,picture,company_name,eventid,url,designation,active,lastmodified,speakerid) Values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",dbSpeaker.speakerFirstName,dbSpeaker.speakerLastName,dbSpeaker.speakerPicURL,dbSpeaker.speakerCompany,dbSpeaker.speakerEventID,dbSpeaker.speakerPDFUrl,dbSpeaker.speakerPosition,dbSpeaker.active,dbSpeaker.lastmodified, dbSpeaker.speakerID];
            
            [self.dbManager executeQuery:query];
            
            
            //    // If the query was successfully executed then pop the view controller.
            if (self.dbManager.affectedRows != 0) {
                NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
            }
            else{
                NSLog(@"Could not execute the query.");
            }
        }
        [self getSpeakerDownloadDataFromDB];
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"BoolDBLock"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } @catch (NSException *e) {
        [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];
    }
    
    
}


-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    //return arrSpeakersImg.count;
    return arrSpeakersCutObjects.count;//arrSpeakersObjects.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    SpeakersCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SpeakerPros" forIndexPath:indexPath];
    Speakers *obj =[[Speakers alloc]init];
    obj=[arrSpeakersCutObjects objectAtIndex:indexPath.row];
   // cell.SpeakerProPic.image=[UIImage imageNamed:@"LoadingImage.jpg"];
    //NSString *imgname = [arrSpeakersImg objectAtIndex:indexPath.row];
    
//    cell.SpeakerProPic.image = [UIImage imageNamed:imgname];
//    cell.SpeakerName.text = [arrSpeakersNames objectAtIndex:indexPath.row];
//
//    cell.SpeakerPosition.text =[arrSpeakersPos objectAtIndex:indexPath.row];
//    cell.SpeakerCompany.text = [arrSpeakersCompany objectAtIndex:indexPath.row];
    NSString *SpeakerFullName=[[NSString alloc]initWithFormat:@"%@ %@", obj.speakerFirstName,obj.speakerLastName];
    cell.SpeakerName.text=SpeakerFullName;
    cell.SpeakerCompany.text=obj.speakerCompany;
    cell.SpeakerPosition.text=obj.speakerPosition;
    Constants *c =[[Constants alloc]init];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(queue, ^{
        
        //NSData *data = [NSData dataWithContentsOfURL:url];
        dispatch_async(dispatch_get_main_queue(), ^{
            //UIImage *img = [[UIImage alloc] initWithData:data];
            //cell.SpeakerProPic.image = img;
            NSString *urlpath= [NSString stringWithFormat:@"http://%@/%@",c.endpoint,obj.speakerPicURL] ;
            
            
            NSURL *url = [NSURL URLWithString:[urlpath stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
             [cell.SpeakerProPic hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"LoadingImage.jpg"]];
        });
    });
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(CGRectGetWidth(collectionView.frame),190);
    
}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(2, 2, 2, 2);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"index path selected is %ld",(long)indexPath.row);
    Speakers *objectToBeSent=[[Speakers alloc]init];
    NSLog(@"indexpath row is %ld", (long)indexPath.row);
    objectToBeSent = [arrSpeakersCutObjects objectAtIndex:indexPath.row];
    if([objectToBeSent.speakerPDFUrl isEqualToString:@""]){
       
    }
    else{
    [self performSegueWithIdentifier:@"speakerDetails" sender:indexPath];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    NSString *eventName=[[NSUserDefaults standardUserDefaults] stringForKey:@"EventName"];
    _lblTitle.text=eventName;
    [self getSpeakerDownloadDataFromDB];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    
    EventRealID =[[NSUserDefaults standardUserDefaults] stringForKey:@"EventRealID"];
    RealEventID=[f numberFromString:EventRealID];
    arrSpeakersCutObjects=[NSMutableArray array];
    for(Speakers *speakerObj in arrSpeakersObjects){
        NSLog(@"%@",speakerObj.speakerEventID);
        NSString *SpeakerId=[[NSString alloc]initWithFormat:@"%@",speakerObj.speakerEventID];
        objEventID=[f numberFromString:SpeakerId];
        
        if([objEventID isEqualToValue:RealEventID]){
            [arrSpeakersCutObjects addObject:speakerObj];
            
        }
        
    }
    NSLog(@"----->>%lu",(unsigned long)arrSpeakersCutObjects.count);
    
    NSLog(@"num of speakers for Event %@ is equal to %@",eventName,EventRealID);
    [_SpeakerCollection reloadData];
    if(arrSpeakersCutObjects.count==0){
        _overlayView.hidden=NO;
        _lblStatus.text=@"No Speakers were Found";
        [_actIndicator stopAnimating];
    }
    else{
        _overlayView.hidden=YES;
    }
}

- (void)showsUIAlertWithMessageWithButtons:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings",nil];
    [alert show];
    
}

- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        NSLog(@"Cancel button clicked");
    }
    if (buttonIndex == 1){
        NSLog(@"settings button clicked");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-prefs:"]];
    }
}
@end
