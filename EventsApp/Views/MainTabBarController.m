//
//  MainTabBarController.m
//  FIAEventsApp
//
//  Created by temp on 21/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "MainTabBarController.h"
#import "SWRevealViewController.h"
#import "EventViewController.h"
#import "SponsorViewController.h"
#import "AttendeesViewController.h"
#import "SpeakersViewController.h"
#import "PortalOptionsClass.h"
#import "ColorsRGB.h"

@interface MainTabBarController (){
    NSMutableArray *originalMutArray;
    NSMutableArray *arrFinal;
    NSMutableArray *arrFinalColors;
    UIView *overlay;
    UIActivityIndicatorView  *av;
    UILabel *lblDisplay;
    UIButton *btnBack;
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnOpen;
@property (weak, nonatomic) IBOutlet UITabBar *tabbar;

@end

@implementation MainTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString *bolCheckTabs=[[NSUserDefaults standardUserDefaults] stringForKey:@"TabsLoadedSucessfully"];
    
    if([bolCheckTabs isEqualToString:@"NO"]){
        
    
    overlay=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    av = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];

    av.color=[UIColor colorWithRed:255/255.0f green:64/255.0f blue:255/255.0f alpha:1.0];
    av.frame = CGRectMake(round((overlay.frame.size.width - 25) / 2), round((overlay.frame.size.height - 25) / 2), 50, 50);
    av.tag  = 1;
    av.hidesWhenStopped=NO;
    [overlay addSubview:av];
  lblDisplay = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-100, self.view.frame.size.height/2-100, 200, 100)];
    lblDisplay.text=@" We are Setting Things Up. This will only take a moment.";
    lblDisplay.adjustsFontSizeToFitWidth=YES;
    lblDisplay.numberOfLines=0;
    lblDisplay.textAlignment=NSTextAlignmentCenter;
    [overlay addSubview:lblDisplay];
        
    btnBack=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2+200, self.view.frame.size.height/2+200, 200, 100)];
    btnBack.titleLabel.text=@"Back";
    btnBack.backgroundColor=[UIColor cyanColor];
    [btnBack addTarget:self action:@selector(btnBackAction) forControlEvents:UIControlEventTouchUpInside];
    [overlay addSubview:btnBack];
    [av startAnimating];
    overlay.backgroundColor =[UIColor whiteColor];
        
        
//    [overlay addSubview:actIndictor];
    [self.view addSubview:overlay];
//     Do any additional setup after loading the view.

    [self getJSONData];
    }
    else{
        overlay=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        av = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        av.color=[UIColor colorWithRed:255/255.0f green:64/255.0f blue:255/255.0f alpha:1.0];
        av.frame = CGRectMake(round((overlay.frame.size.width - 25) / 2), round((overlay.frame.size.height - 25) / 2), 50, 50);
        av.tag  = 1;
        av.hidesWhenStopped=NO;
        [overlay addSubview:av];
        lblDisplay = [[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-100, self.view.frame.size.height/2-100, 200, 100)];
        lblDisplay.text=@"Loading Desired Action.This will only take a moment.";
        lblDisplay.adjustsFontSizeToFitWidth=YES;
        lblDisplay.numberOfLines=0;
        lblDisplay.textAlignment=NSTextAlignmentCenter;
        [overlay addSubview:lblDisplay];
        [av startAnimating];
        overlay.backgroundColor =[UIColor whiteColor];
        //    [overlay addSubview:actIndictor];
        btnBack=[[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2+200, self.view.frame.size.height/2+200, 200, 100)];
        btnBack.titleLabel.text=@"Back";
        btnBack.backgroundColor=[UIColor cyanColor];
        [btnBack addTarget:self action:@selector(btnBackAction) forControlEvents:UIControlEventTouchUpInside];
        [overlay addSubview:btnBack];
        [self.view addSubview:overlay];
        //     Do any additional setup after loading the view.
       
        [self getJSONData];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)btnBackAction{
    [self performSegueWithIdentifier:@"login" sender:self];
}

-(void)setDisplayTabs1:(UIColor*)colortint andBackground:(UIColor *)colorback{
//    self.tabBar.tintColor=[UIColor whiteColor];
//    self.tabBar.barTintColor=colorback;
//    //    self.tabBar.backgroundColor=color;
//    originalMutArray =[self.viewControllers mutableCopy];
//    [originalMutArray removeLastObject];
//    self.viewControllers = originalMutArray;
//    [self.tabBarController.view setUserInteractionEnabled:NO];
}

-(void)setDisplayTabs2{
    //    self.tabBar.tintColor=[UIColor whiteColor];
    //    self.tabBar.barTintColor=[UIColor grayColor];
    originalMutArray=[self.viewControllers mutableCopy];
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UIViewController *vc = [sb instantiateViewControllerWithIdentifier:@"1"];
    
    [originalMutArray addObject:vc];
    self.viewControllers=originalMutArray;
    
    
    //    self.viewControllers = originalMutArray;
}

-(void)SetTabs{
    NSLog(@"entered set tabs from jason function");
    NSMutableArray *arrUIViewTabs=[[NSMutableArray alloc]init];
    //    self.viewControllers=arrUIViewTabs;
    if(arrFinal.count>0){
    for(PortalOptionsClass *obj in arrFinal){
        if([obj.tabname isEqualToString:@"event"]&& [obj.active isEqualToString:@"1"]){
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            EventViewController *vc = [sb instantiateViewControllerWithIdentifier:@"1"];
            vc.tabBarItem.image=[UIImage imageNamed:@"events.png"];
            [arrUIViewTabs addObject: vc];
        }
        if([obj.tabname isEqualToString:@"speakers"]&& [obj.active isEqualToString:@"1"]){
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SpeakersViewController *vc = [sb instantiateViewControllerWithIdentifier:@"2"];
            vc.tabBarItem.image=[UIImage imageNamed:@"Speakers.png"];
            [arrUIViewTabs addObject: vc];
        }
        if([obj.tabname isEqualToString:@"sponsor"]&& [obj.active isEqualToString:@"1"]){
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            SponsorViewController *vc = [sb instantiateViewControllerWithIdentifier:@"4"];
            vc.tabBarItem.image=[UIImage imageNamed:@"Sponsors.png"];
            [arrUIViewTabs addObject: vc];
        }
        if([obj.tabname isEqualToString:@"attendees"]&& [obj.active isEqualToString:@"1"]){
            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            AttendeesViewController *vc = [sb instantiateViewControllerWithIdentifier:@"3"];
            vc.tabBarItem.image=[UIImage imageNamed:@"attendees.png"];
            [arrUIViewTabs addObject: vc];
        }

        }
    }
    else{
        UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        EventViewController *vc = [sb instantiateViewControllerWithIdentifier:@"1"];
        vc.tabBarItem.image=[UIImage imageNamed:@"events.png"];
        [arrUIViewTabs addObject: vc];
    }
    self.viewControllers=arrUIViewTabs;
    [self.tabBarController.view setNeedsDisplay];
    [self.tabBarController.view setUserInteractionEnabled:NO];
   
    
    
}
-(void)getJSONData{
    NSLog(@"entered get tabs from jason function");
    NSDictionary *headers = @{ @"Authorization": @"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd",
                               @"Cache-Control": @"no-cache",
                               @"Postman-Token": @"c390182f-2a7a-431f-9831-9afe1a9e19b9" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://ashivanram.tk/apiTest/readTabs.php"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                        dispatch_sync(dispatch_get_main_queue(), ^{
                                                            [self showsUIAlertWithMessageWithButtons:@"Could not fetch data.Please try again." andTitle:@"Ooops!! Something Happened"];
//                                                            [self performSegueWithIdentifier:@"errorLogin" sender:self];

                                                            NSMutableArray *arrUIViewTabs=[[NSMutableArray alloc]init];
                                                            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                            EventViewController *vc = [sb instantiateViewControllerWithIdentifier:@"1"];
                                                            vc.tabBarItem.image=[UIImage imageNamed:@"events.png"];
                                                            [arrUIViewTabs addObject: vc];
                                                            self.viewControllers=arrUIViewTabs;
                                                            [self.tabBarController.view setNeedsDisplay];
                                                            [self.tabBarController.view setUserInteractionEnabled:NO];
//                                                            [overlay removeFromSuperview];
                                                            [av stopAnimating];
                                                            lblDisplay.text=@"Error Loading Data";
//                                                            [overlay addSubview:btnBack];
                                                            [self performSegueWithIdentifier:@"login" sender:self];
                                                        });

                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);

                                                        NSDictionary *arrJSON=[NSJSONSerialization
                                                                               JSONObjectWithData:data
                                                                               options:0
                                                                               error:&error];
                                                        NSLog(@"%@", arrJSON);
                                                        //                                                        PortalOptionsClass *obj = [[PortalOptionsClass alloc]init];
                                                        self->arrFinal=[[NSMutableArray alloc]init];
                                                        arrJSON=[arrJSON objectForKey:@"tabs"];
                                                        NSLog(@"%@",arrJSON);
                                                        for(NSDictionary *JSONobj in arrJSON){
                                                            PortalOptionsClass *obj = [[PortalOptionsClass alloc]init];
                                                            NSLog(@"%@",JSONobj);
                                                            obj.tabname=[JSONobj objectForKey:@"tabs"];
                                                            obj.active= [JSONobj objectForKey:@"active"];

                                                            [self->arrFinal addObject:obj];
                                                        }

                                                        NSLog(@"array has %@",self->arrFinal);

                                                        dispatch_sync(dispatch_get_main_queue(), ^{
                                                            [self SetTabs];
                                                            [self getcolorsfromJSON];
                                                        });



                                                    }
                                                }];
    [dataTask resume];
}
-(void)viewWillAppear:(BOOL)animated{
//    [self getJSONData];
}
-(void)getcolorsfromJSON{
    NSLog(@"entered get colors from jason function");
    NSDictionary *headers = @{ @"Authorization": @"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd",
                               @"Cache-Control": @"no-cache"
                               };

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://yonalsen.000webhostapp.com/api/product/readcolor.php"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];

    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                        dispatch_sync(dispatch_get_main_queue(), ^{
                                                            [self showsUIAlertWithMessageWithButtons:@"Could not fetch data.Please try again." andTitle:@"Ooops!! Something Happened"];
                                                            //                                                            [self performSegueWithIdentifier:@"errorLogin" sender:self];

                                                            NSMutableArray *arrUIViewTabs=[[NSMutableArray alloc]init];
                                                            UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                            EventViewController *vc = [sb instantiateViewControllerWithIdentifier:@"1"];
                                                            vc.tabBarItem.image=[UIImage imageNamed:@"events.png"];
                                                            [arrUIViewTabs addObject: vc];
                                                            self.viewControllers=arrUIViewTabs;
                                                            [self.tabBarController.view setNeedsDisplay];
                                                            [self.tabBarController.view setUserInteractionEnabled:NO];
//                                                            [overlay removeFromSuperview];
                                                              [av stopAnimating];
                                                            lblDisplay.text=@"Error Loading Data";
                                                            [self performSegueWithIdentifier:@"login" sender:self];
                                                        });
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        NSLog(@"%@", httpResponse);
                                                        NSDictionary *arrJSON2=[NSJSONSerialization
                                                                                JSONObjectWithData:data
                                                                                options:0
                                                                                error:&error];
                                                        arrJSON2=[arrJSON2 objectForKey:@"colors"];
                                                        arrFinalColors = [[NSMutableArray alloc]init];
                                                        NSLog(@"%@",arrJSON2);
                                                        for(NSDictionary *dicObj in arrJSON2){
                                                            ColorsRGB *obj=[[ColorsRGB alloc]init];
                                                            obj.id=[dicObj objectForKey:@"id"];
                                                            obj.eventname=[dicObj objectForKey:@"event"];
                                                            obj.R=[dicObj objectForKey:@"R"];
                                                            obj.G=[dicObj objectForKey:@"G"];
                                                            obj.B=[dicObj objectForKey:@"B"];

                                                            [self->arrFinalColors addObject:obj];

                                                        }
                                                        NSLog(@"%@",self->arrFinalColors);
                                                        dispatch_sync(dispatch_get_main_queue(), ^{
                                                            [self setColors];
                                                        });


                                                    }
                                                }];
    [dataTask resume];
}

-(void)setColors{
    NSLog(@"entered set colors from jason function");
    self.tabBar.tintColor=[UIColor blueColor];//selected color
    self.tabBar.unselectedItemTintColor=[UIColor whiteColor];
    for(ColorsRGB *obj in arrFinalColors){
        if([obj.eventname isEqualToString:@"USP"]){
            //            self.tabBar.tintColor=[UIColor whiteColor];
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;

            NSNumber *Rcolor=[f numberFromString:obj.R];
            NSNumber *Gcolor=[f numberFromString:obj.G];
            NSNumber *Bcolor=[f numberFromString:obj.B];
            self.tabBar.barTintColor=[UIColor colorWithRed:[Rcolor intValue]/255.0 green:[Gcolor intValue]/255.0 blue:[Bcolor intValue]/255.0 alpha:1.0];

        }
    }

     [overlay removeFromSuperview];
    [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"TabsLoadedSucessfully"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
}
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    //    item.image=[UIImage animatedImageNamed:@"0.gif" duration:2.0];
//    UIView *itemView=[item valueForKey:@"view"];
//    [UIView animateWithDuration:1.0 animations:^{
//
//        [itemView setTransform:CGAffineTransformMakeRotation(M_PI)];
//        [itemView setTransform:CGAffineTransformMakeRotation(0)];
//    }];
}
- (void)showsUIAlertWithMessageWithButtons:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}


@end
