//
//  AttendeesViewController.m
//  FIAEventsApp
//
//  Created by temp on 08/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "AttendeesViewController.h"
#import "AttendeesProCollectionViewCell.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "Attendees.h"
#import "AttendeesJsonDownload.h"
#import "Constants.h"
#import "Haneke.h"
#import "DBManager.h"
#import "Reachability.h"

@interface AttendeesViewController (){
    AttendeesJsonDownload *AttendeesDownloads;
    NSNumber *EventID;
    NSNumber *objEventID;
    NSString *EventRealID;
    NSNumber *RealEventID;
    long numOfRows;
    UIRefreshControl *refreshControl;
}

@property (strong, nonatomic) IBOutlet UILabel *lblBanner;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnDrawer;
//@property (strong, nonatomic) IBOutlet UICollectionView *CollAttendees;
@property (nonatomic, strong) DBManager *dbManager;
@property (strong,nonatomic) NSString *datapath;
@property (nonatomic) sqlite3 *DB;
@property (weak, nonatomic) IBOutlet UIView *overlayVIew;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;



@end

@implementation AttendeesViewController{
    NSMutableArray *arrAttendees;
    NSMutableArray *arrComparedAttendeeData;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _overlayVIew.hidden=NO;
    Constants *obj=[[Constants alloc]init];
      self.dbManager = [[DBManager alloc] initWithDatabaseFilename:obj.dbName];

    [self getAttendeesDownloadFromDB];

//    [_lblBanner.layer setCornerRadius:17.0f];
//    
//    // border
////    [_lblBanner.layer setBorderColor:[UIColor whiteColor].CGColor];
////    [_lblBanner.layer setBorderWidth:0.9f];
//    _lblBanner.clipsToBounds=YES;
    
    [self.btnDrawer setTarget:self.revealViewController];
    [self.btnDrawer setAction:@selector(revealToggle: )];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    refreshControl = [[UIRefreshControl alloc]init];
    [self.AttendeesCollection addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)refreshTable{
    
    [refreshControl endRefreshing];
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
//             [_AttendeesCollection reloadData];
        [self showsUIAlertWithMessageWithButtons:@"Content cannot be Updated. No Internet Connection Avaliable" andTitle:@"Information"];
        
    
       
    }
    else
    {
        Reachability* reachability = [Reachability reachabilityWithHostName: @"www.google.com"];
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        NSLog(@"the nertwok is %ld", (long)netStatus);
        if((long)netStatus>0){
            [self UpdateAttendeeData];
        }
        else{
            [self showsUIAlertWithMessage:@"Oh!No.Your internet connection is not stable. Please Check Internet connection and Try Again." andTitle:@"Alert"];
        }
        
    }
    
}

-(void)UpdateAttendeeData{
    AttendeesJsonDownload *AttendeesDownloads = [[AttendeesJsonDownload alloc]init];
    [AttendeesDownloads setDelegate:(id)self];
    NSString *BoolDBLock =[[NSUserDefaults standardUserDefaults] stringForKey:@"BoolDBLock"];
    if(![BoolDBLock isEqualToString:@"YES"]){
        [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:@"BoolDBLock"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        [AttendeesDownloads OrganizeDataFromJsonToObject];
    }
    
    
   
//    [self.AttendeesCollection reloadData];
    
    
    
}


-(void)getAttendeesDownload:(NSMutableArray *)attendees{
    
    @try {
        NSString *clearquery = [NSString stringWithFormat:@"delete from tblAttendees"];
        //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
        
        [self.dbManager executeQuery:clearquery];
        
        
        //    // If the query was successfully executed then pop the view controller.
        if (self.dbManager.affectedRows != 0) {
            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
        }
        else{
            NSLog(@"Could not execute the query.");
        }
        for(Attendees *dbAttendee in attendees){
            dbAttendee.AttendeeCompany=[ dbAttendee.AttendeeCompany stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            dbAttendee.AttendeesFirstName=[ dbAttendee.AttendeesFirstName stringByReplacingOccurrencesOfString:@"\"" withString:@"\"\""];
            NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblAttendees (events_id,user_id,active,lastmodified,first_name,last_name,company_name,picture) Values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",dbAttendee.AttendeeEventID,dbAttendee.attendeeID,dbAttendee.active,dbAttendee.attendeeLastModified,dbAttendee.AttendeesFirstName,dbAttendee.AttendeesLastName,dbAttendee.AttendeeCompany,dbAttendee.AttendeesPicURL];
            //        NSString *query = [NSString stringWithFormat:@"delete from tblAttendees where events_id=='%@'",dbAttendee.AttendeeEventID];
            
            [self.dbManager executeQuery:query];
            
            
            //    // If the query was successfully executed then pop the view controller.
            if (self.dbManager.affectedRows != 0) {
                NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
            }
            else{
                NSLog(@"Could not execute the query.");
            }
        }
        NSString *query = [NSString stringWithFormat:@"select * from tblAttendees where active=='1' order by lastmodified DESC"];
        
        //    //    // Execute the query.
        
        NSArray *dbresults =[self.dbManager loadDataFromDB:query];
        if(dbresults.count>0){
            //    NSLog(@"db elements %@",dbresults);
            
            
            //        checkLastModifiedAttendees=[[dbresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
            //        [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedAttendees forKey:@"LastModified-Attendee-v1"];
            //        [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        [self getAttendeesDownloadFromDB];
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"BoolDBLock"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } @catch (NSException *e) {
        [self showsUIAlertWithMessage:@"Oops!! Something went wrong. Check Internet connection and Try Again" andTitle:@"Information"];

    }
    
    
}

- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    alert.tag=5;
    [alert show];
    
    
}
- (void)showsUIAlertWithMessageWithButtons:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings",nil];
    [alert show];
    
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        NSLog(@"Cancel button clicked");
    }
    if (buttonIndex == 1){
        NSLog(@"settings button clicked");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-prefs:"]];
    }
}


-(void)getAttendeesDownloadFromDB{
   EventRealID =[[NSUserDefaults standardUserDefaults] stringForKey:@"EventRealID"];
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;

    RealEventID=[f numberFromString:EventRealID];
    
    
    NSString *query = [NSString stringWithFormat:@"select * from tblAttendees where active=='1'"];

    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
    NSLog(@"db elements %@",dbresults);
    
    arrAttendees=[[NSMutableArray alloc]init];
    for (int i =0; i < dbresults.count; i++){
        Attendees *obj =[[Attendees alloc]init];
         obj.AttendeeEventID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"events_id"]];
        obj.attendeeID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"user_id"]];
        obj.active=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"active"]];
        obj.attendeeLastModified=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
        obj.AttendeesFirstName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"first_name"]];
        obj.AttendeesLastName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"last_name"]];
        obj.AttendeesPicURL=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"picture"]];
        obj.AttendeeCompany=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"company_name"]];
        
        [arrAttendees addObject:obj];
//        NSLog(@"---> one obj =%@",obj.AttendeesFirstName);
    }
   
    NSLog(@"attendees is %@",arrAttendees);
    
//    arrAttendees = attendees;

    
    
    
    numOfRows=0;
   
    //RealEventID=[f numberFromString:EventRealID];
    arrComparedAttendeeData=[[NSMutableArray alloc]init];
    for(Attendees *attendee in arrAttendees){
        objEventID=[f numberFromString:attendee.AttendeeEventID];
        if([objEventID isEqualToValue:RealEventID]){
            [arrComparedAttendeeData addObject:attendee];
            numOfRows++;
        }
        
    }
    [_AttendeesCollection reloadData];
    
    if(arrAttendees.count==0){
        _lblStatus.text=@"No Attendees were Found";
        [_actIndicator stopAnimating];
    }else{
        
    
    _overlayVIew.hidden=YES;
    }
    NSString
        *eventName=[[NSUserDefaults standardUserDefaults] stringForKey:@"EventName"];
    _lblBanner.text=eventName;
//    NSLog(@"%lu",numOfRows);
    
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return arrComparedAttendeeData.count;
    
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    Attendees *obj = [[Attendees alloc]init];
    
    AttendeesProCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AttendPros" forIndexPath:indexPath];
  
    obj=[arrComparedAttendeeData objectAtIndex:indexPath.row];
    
    cell.AttendeeName.text=[NSString stringWithFormat:@"%@ %@",obj.AttendeesFirstName,obj.AttendeesLastName];
    cell.AttendeesCompany.text=obj.AttendeeCompany;
    
    Constants *cObj=[[Constants alloc]init];
    cell.tag=indexPath.row;
    
        dispatch_async(dispatch_get_main_queue(), ^{
            if(cell.tag==indexPath.row){
            //UIImage *img = [[UIImage alloc] initWithData:data];
         //   cell.AttendeeProPic.image = img;
                NSString *urlpath=[NSString stringWithFormat:@"http://%@/%@",cObj.endpoint,obj.AttendeesPicURL];
                NSURL *url = [NSURL URLWithString:urlpath];
                [cell.AttendeeProPic hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"LoadingImage.jpg"]] ;
            }
        });
        
        return  cell;
        
 
   
    
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((CGRectGetWidth(collectionView.frame)/2)-8,(CGRectGetHeight(collectionView.frame)/2)+8);

}

-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    
    return UIEdgeInsetsMake(2, 2, 2, 2);
}

-(void)viewWillAppear:(BOOL)animated{
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    NSString *eventName=[[NSUserDefaults standardUserDefaults] stringForKey:@"EventName"];
    _lblBanner.text=eventName;
    NSNumber *temp=[NSNumber numberWithInteger:[[NSUserDefaults standardUserDefaults] integerForKey:@"EventID"]];
    [self getAttendeesDownloadFromDB];
    if(EventID==temp){
    }
    else{
        
        
        numOfRows=0;
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        
      EventRealID =[[NSUserDefaults standardUserDefaults] stringForKey:@"EventRealID"];
        RealEventID=[f numberFromString:EventRealID];
        //RealEventID=[f numberFromString:EventRealID];
        arrComparedAttendeeData=[NSMutableArray array];
        for(Attendees *attendee in arrAttendees){
            objEventID=[f numberFromString:attendee.AttendeeEventID];
            if([objEventID isEqualToValue:RealEventID]){
                [arrComparedAttendeeData addObject:attendee];
                numOfRows++;
            }
            
        }
        NSLog(@"%lu",numOfRows);
        NSLog(@"cut data %@",arrComparedAttendeeData);
        [_AttendeesCollection reloadData];
        
    }
    if(arrComparedAttendeeData.count==0){
        _overlayVIew.hidden=NO;
        _lblStatus.text=@"No Attendees were Found";
        [_actIndicator stopAnimating];
    }else{
        
        
        _overlayVIew.hidden=YES;
    }
    EventID =[NSNumber numberWithInteger:[[NSUserDefaults standardUserDefaults] integerForKey:@"EventID"]];
    
    
    
}


@end

