//
//  SpeakerDetailsViewController.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 06/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "SpeakerDetailsViewController.h"
#import "Constants.h"
#import "Reachability.h"

@interface SpeakerDetailsViewController (){
    NSString *SURL;
    NSString *SName;
}
@property (strong, nonatomic) IBOutlet UIWebView *SpeakerPDFWebView;
@property (strong, nonatomic) IBOutlet UILabel *lblBanner;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *actIndLoadingIcon;


@end

@implementation SpeakerDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //NSURL *url = [NSURL URLWithString:@"http://www.findfiji.me"];
    self.SpeakerPDFWebView.delegate=self;
    NSURL *url = [NSURL URLWithString:SURL];
    NSURLRequest *request =  [NSURLRequest requestWithURL:url];
    [_SpeakerPDFWebView loadRequest:request];
    
    _lblBanner.text=SName;
//    [_lblBanner.layer setCornerRadius:17.0f];
//
//    // border
//    [_lblBanner.layer setBorderColor:[UIColor whiteColor].CGColor];
//    [_lblBanner.layer setBorderWidth:0.9f];
//    _lblBanner.clipsToBounds=YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setSpeakerUrl:(NSString*)url andName:(NSString *)speakername{
    if([url isEqualToString:@""]){
        NSString *finalurl=[[NSString alloc]initWithFormat:@"http://www.findfiji.me"];
        NSLog(@"%@",finalurl);
        SURL=finalurl;
    }
    else{
        Constants *obj=[[Constants alloc]init];
        NSString *finalurl=[[NSString alloc]initWithFormat:@"http://%@/%@",obj.endpoint,url];
        NSLog(@"%@",finalurl);
        SURL=finalurl;
    }
    
    SName=speakername;
  
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
   
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        [self showsUIAlertWithMessageWithButtons:@"You need to be connected to the internet to view this content" andTitle:@"Alert"];
    }
    else
    {
         [self.actIndLoadingIcon startAnimating];
    }

}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.actIndLoadingIcon stopAnimating];
    self.actIndLoadingIcon.hidesWhenStopped=YES;
}

- (void)showsUIAlertWithMessageWithButtons:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings",nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        NSLog(@"Cancel button clicked");
    }
    if (buttonIndex == 1){
        NSLog(@"settings button clicked");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-prefs:"]];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
