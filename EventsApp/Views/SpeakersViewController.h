//
//  SpeakersViewController.h
//  FIAEventsApp
//
//  Created by temp on 08/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeakersViewController : UIViewController <UICollectionViewDataSource,UICollectionViewDelegate,UIAlertViewDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *SpeakerCollection;

@end
