//
//  SpeakerRatingTableViewCell.h
//  FIAEventsApp
//
//  Created by Ashivan on 4/16/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SpeakerRatingTableViewCell : UITableViewCell<UITextFieldDelegate,UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
//@property (weak, nonatomic) IBOutlet UITextField *txtComments;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmit;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *btnStarColl;
@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (weak, nonatomic) IBOutlet UITextView *txtComments;
- (IBAction)btnSelectStar:(UIButton *)sender;
@end
