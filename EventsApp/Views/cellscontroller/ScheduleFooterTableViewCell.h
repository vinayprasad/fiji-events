//
//  ScheduleFooterTableViewCell.h
//  FIAEventsApp
//
//  Created by temp on 15/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleFooterTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgFoot;
@property (strong, nonatomic) IBOutlet UIImageView *ImgDisplayed;


@end
