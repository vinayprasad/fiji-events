//
//  YourChatTableViewCell.h
//  FIAEventsApp
//
//  Created by temp on 14/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YourChatTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *ChatProPic;
@property (strong, nonatomic) IBOutlet UILabel *lblName;
@property (strong, nonatomic) IBOutlet UILabel *lblMessage;
@property (strong, nonatomic) IBOutlet UIImageView *ImgBackground;

@end
