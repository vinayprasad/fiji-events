//
//  SessionFinishedTableViewCell.m
//  FIAEventsApp
//
//  Created by temp on 15/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "SessionFinishedTableViewCell.h"

@implementation SessionFinishedTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [_lblIndicator.layer setCornerRadius:self.lblIndicator.frame.size.width/2];
    _lblIndicator.layer.borderWidth=0.5;
    _lblIndicator.layer.masksToBounds = YES;
    _lblIndicator.clipsToBounds=YES;
    [_lblIndicator.layer setBorderColor:[UIColor clearColor].CGColor];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)btnPanel:(id)sender {
//    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
//    if([Username isEqualToString:@"Guest"]){
//          [self showsUIAlertWithMessage:@"Sorry. Please Login to access this feature" andTitle:@"Information"];
//    }
    
}

- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}
@end

