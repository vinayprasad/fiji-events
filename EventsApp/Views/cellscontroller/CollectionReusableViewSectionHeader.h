//
//  CollectionReusableViewSectionHeader.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 15/03/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CollectionReusableViewSectionHeader : UICollectionReusableView
@property (strong, nonatomic) IBOutlet UILabel *lblSectionLabel;

@end
