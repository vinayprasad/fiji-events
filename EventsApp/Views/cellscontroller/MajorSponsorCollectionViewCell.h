//
//  MajorSponsorCollectionViewCell.h
//  FIAEventsApp
//
//  Created by temp on 12/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MajorSponsorCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *ImageMajor;
@property (strong, nonatomic) IBOutlet UIImageView *imgBackground;
@property (strong, nonatomic) IBOutlet UILabel *lblSponsorName;
@property (strong, nonatomic) IBOutlet UILabel *lblSponsorSite;
@property (strong, nonatomic) IBOutlet UIImageView *ImgArrow;

@end
