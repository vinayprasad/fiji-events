//
//  SessionUpcomingTableViewCell.m
//  FIAEventsApp
//
//  Created by temp on 15/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "SessionUpcomingTableViewCell.h"

@implementation SessionUpcomingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
        [_lblIndicator.layer setCornerRadius:self.lblIndicator.frame.size.width/2];
        _lblIndicator.layer.borderWidth=0.5;
        _lblIndicator.layer.masksToBounds = YES;
        _lblIndicator.clipsToBounds=YES;
        [_lblIndicator.layer setBorderColor:[UIColor clearColor].CGColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
