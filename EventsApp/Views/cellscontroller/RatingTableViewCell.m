//
//  RatingTableViewCell.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 26/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "RatingTableViewCell.h"

@implementation RatingTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
   
    //    // border
        [_txtComments.layer setBorderColor:[UIColor blackColor].CGColor];
        [_txtComments.layer setBorderWidth:0.5f];
//    _txtComments.textColor=[UIColor lightGrayColor];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnSelectStar:(UIButton *)sender {
    NSInteger TagID = sender.tag;
    for(UIButton *btn in _btnStarColl){
        if(btn.tag<=TagID){
            [btn setTitle:@"★" forState:UIControlStateNormal];
//            _txtComments.text=[NSString stringWithFormat:@"%ld Stars",TagID+1];
            [self getStarvalue:TagID+1];
            [[NSUserDefaults standardUserDefaults] setInteger:TagID+1 forKey:@"SessionRating"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else{
            [btn setTitle:@"☆" forState:UIControlStateNormal];
        }
    }
   
}


-(NSInteger)getStarvalue:(NSInteger)RatingNumber{
    return RatingNumber;
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if([_txtComments isFirstResponder] && [touch view] != _txtComments){
        [_txtComments resignFirstResponder];
        
    }
   
    [super touchesBegan:touches withEvent:event];
}


@end
