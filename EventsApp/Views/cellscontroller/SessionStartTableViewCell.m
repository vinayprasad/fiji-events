//
//  SessionStartTableViewCell.m
//  FIAEventsApp
//
//  Created by temp on 15/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "SessionStartTableViewCell.h"

@implementation SessionStartTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    //----------------------------------------------
//    [_btnForum.layer setCornerRadius:17.0f];
//
//    // border
//    [_btnForum.layer setBorderColor:[UIColor whiteColor].CGColor];
//    [_btnForum.layer setBorderWidth:0.9f];
//    //----------------------------------------------
//    [_lblSpeakerName.layer setCornerRadius:17.0f];
//
//    // border
//    [_lblSpeakerNames.layer setBorderColor:[UIColor whiteColor].CGColor];
//    [_lblSpeakerNames.layer setBorderWidth:0.9f];
    
    [_lblIndicator.layer setCornerRadius:self.lblIndicator.frame.size.width/2];
    _lblIndicator.layer.borderWidth=0.5;
    _lblIndicator.layer.masksToBounds = YES;
    _lblIndicator.clipsToBounds=YES;
    [_lblIndicator.layer setBorderColor:[UIColor clearColor].CGColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
