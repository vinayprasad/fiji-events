//
//  SideDrawerViewController.m
//  FIAEventsApp
//
//  Created by Ashivan on 5/9/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import "SideDrawerViewController.h"
#import "DetailViewController.h"
#import "EventJsonDownload.h"
#import "Dummy.h"
#import "EventListTableViewCell.h"
#import "EventViewController.h"
#import "UserInfoDownload.h"
#import "Users.h"
#import "UserProTableViewCell.h"
#import "MyProfileViewController.h"
#import "Constants.h"
#import "Haneke.h"
#import "DBManager.h"
#import "SWRevealViewController.h"
#import "NotificationDownload.h"
#import "Feeds.h"
#import "LogOutTableViewCell.h"
@import GoogleSignIn;

@interface SideDrawerViewController (){
    
        EventJsonDownload *EventList;
        UserInfoDownload *userinfoObj;
        Users *currentUserInfo;
        NSMutableArray *arrEvents;
        long eventID;
    bool loadingNotification;
        UIView *overlayview;
    NSMutableArray *arrnotification;
        
    
}

@end

@implementation SideDrawerViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    overlayview =[[UIView alloc]initWithFrame:CGRectMake(self.view.frame.origin.x
                                                         ,self.view.frame.origin.y
                                                         , self.view.frame.size.width, self.view.frame.size.height)];
    
    
    Constants *obj=[[Constants alloc]init];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:obj.dbName];
    
    [self setUserInfo];
    [self getDataFromDB];
    
}

-(void)getDownloaded:(Users *)userinfo{
     NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    if(userinfo==NULL||[Username isEqualToString:@"Guest"]){
        currentUserInfo=[[Users alloc]init];
        currentUserInfo.FirstName=@"Guest";
        currentUserInfo.LastName=@"";
        currentUserInfo.Picture=@"guesticon.png";
    }else{
        currentUserInfo=userinfo;
    }
    [_tblSideDrawerContent reloadData];
}

-(void)getNotification:(NSMutableArray *)notify{
    arrnotification=notify;
    loadingNotification=true;
    [_tblSideDrawerContent reloadData];
    long num=[[NSUserDefaults standardUserDefaults] integerForKey:@"EventID"];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:num inSection:1];
    [_tblSideDrawerContent selectRowAtIndexPath:indexPath
                                       animated:YES
                                 scrollPosition:UITableViewScrollPositionNone];
    [self tableView:_tblSideDrawerContent didSelectRowAtIndexPath:indexPath ];
}

-(void)getDataFromDB{
    NSString *query = [NSString stringWithFormat:@"select * from tblEvents where active=='1'"];
    
    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
    NSLog(@"db elements %@",dbresults);
    
    NSMutableArray * arrGetEvent=[NSMutableArray array];
    //        NSMutableArray * arrGetEvent;
    for(int i=0;i<dbresults.count;i++){
        Dummy *obj =[[Dummy alloc]init];
        
        
        obj.eventID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"id"]];
        obj.eventName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"name"]];
        obj.eventDesc=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"description"]];
        obj.startDate=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"start_date"]];
        obj.endDate=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"end_date"]];
        obj.active=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"active"]];
        obj.lastModified=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
        obj.eventImageURL=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"image_cover"]];
        obj.venueID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"venue_address"]];
        obj.latitude=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lat"]];
        obj.longtitude=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"long"]];
        
        [arrGetEvent addObject:obj];
    }
    arrEvents=arrGetEvent;
    
    [_tblSideDrawerContent reloadData];

}

-(void)getDownloaded:(NSMutableArray *)events andSession:(NSMutableArray *)session{
    NSLog(@"stuff is %@",events);
    NSLog(@"session is %@",session);
    arrEvents=[NSMutableArray array];
    arrEvents = events;
    NSLog(@"events array=%@",arrEvents);
    
//    [_tblSideDrawerContent reloadData];
    
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
//    [_tblSideDrawerContent selectRowAtIndexPath:indexPath
//                          animated:YES
//                    scrollPosition:UITableViewScrollPositionNone];
//    [self tableView:_tblSideDrawerContent didSelectRowAtIndexPath:indexPath ];
    
    
}


- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setUserInfo];
    loadingNotification=false;
    [_tblSideDrawerContent reloadData];
    NotificationDownload *NotifyDownloads = [[NotificationDownload alloc]init];
    [NotifyDownloads setDelegate:(id)self];
    
    [NotifyDownloads OrganizeDataFromJsonToObject];

    [self.revealViewController.frontViewController.view addSubview:overlayview];
    [overlayview addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [overlayview addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    
    
//     [_tblSideDrawerContent reloadData];
    
    
    
    
    
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self.revealViewController.frontViewController.view addGestureRecognizer:self.revealViewController.tapGestureRecognizer];
    overlayview.removeFromSuperview;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"profile"])
    {
        // Get reference to the destination view controller
        MyProfileViewController *vc = [segue destinationViewController];
        // Pass any objects to the view controller here, like...
        [vc setUserObject:currentUserInfo];
    }
}


#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section==0){
        return 1;
    }
    if(section==1){
        return arrEvents.count;
    }
    if(section==2){
        if(loadingNotification==false){
            return 1;
        }else{
        return arrnotification.count;
        }
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    Constants *cObj=[[Constants alloc]init];
    if (indexPath.section==0){
        UserProTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserProCell" forIndexPath:indexPath];
        
        cell.lblUserWelcome.text=[[NSString stringWithFormat:@"Welcome %@ %@",currentUserInfo.FirstName,currentUserInfo.LastName]capitalizedString];
        NSString *urlpath=[NSString stringWithFormat:@"%@",currentUserInfo.Picture];
        //        NSLog(@"%@",currentUserInfo.Picture);
        NSURL *url = [NSURL URLWithString:urlpath];
        NSString *DidGoogleSignIn =[[NSUserDefaults standardUserDefaults] stringForKey:@"DidGoogleSignIn"];
        if([DidGoogleSignIn isEqualToString:@"YES"]){
            [cell.ImgUserProPic hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"LoadingImage.jpg"]];
        }
        else{
            
            cell.ImgUserProPic.image=[UIImage imageNamed:currentUserInfo.Picture];
        }
        return cell;
    }
    
    if(indexPath.section==1){
        EventListTableViewCell  *cell= [tableView dequeueReusableCellWithIdentifier:@"EventListCell" forIndexPath:indexPath];

        Dummy *obj =[[Dummy alloc]init];
        obj=[arrEvents objectAtIndex:indexPath.row];
        cell.lblEventID.text=[[[NSString alloc]initWithFormat:@"%@",obj.eventName] uppercaseString];
        NSString *temp2=obj.startDate;

        
        cell.lblEventDate.text=temp2;
        
        return cell;
        
    }
    
    if(indexPath.section==2){
        LogOutTableViewCell *cell= [tableView dequeueReusableCellWithIdentifier:@"Notification" forIndexPath:indexPath];
        Feeds *obj=[[Feeds alloc]init];
        if(loadingNotification==false){
            cell.lblDaysAgo.hidden=YES;
            cell.lblNotificationMsg.hidden=YES;
            cell.lblNotificationTitle.hidden=YES;
            cell.actLoading.hidden=NO;
            [cell.actLoading startAnimating];
        }
        else{
            cell.lblDaysAgo.hidden=NO;
            cell.lblNotificationMsg.hidden=NO;
            cell.lblNotificationTitle.hidden=NO;
            cell.actLoading.hidden=YES;
        long indexNotify = arrnotification.count-indexPath.row-1;
        obj=[arrnotification objectAtIndex:indexNotify];
        cell.lblNotificationTitle.text=[obj.notifyTitle uppercaseString];
        cell.lblNotificationMsg.text=obj.notifyBody;
        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss aa"];
//        NSString *sessionDate=[dateFormatter stringFromDate:session.sessionDate];
        NSDate *notificationDate = [dateFormatter dateFromString:obj.lastmodified];
        NSDate *today=[NSDate date];
        NSLog(@"date is %@", notificationDate);
        
//            eventStart = session.sessionDate;
        
        NSDate *fromDate;
        NSDate *toDate;

        NSCalendar *calendar = [NSCalendar currentCalendar];

        [calendar rangeOfUnit:NSCalendarUnitDay startDate:&fromDate
                     interval:NULL forDate:notificationDate];
        [calendar rangeOfUnit:NSCalendarUnitDay startDate:&toDate
                     interval:NULL forDate:today];
        NSDateComponents *difference = [calendar components:NSCalendarUnitDay
                                                   fromDate:fromDate toDate:toDate options:0];
        
        NSLog(@"%ld", (long)[difference day]);
            if([difference day]==0){
            cell.lblDaysAgo.text=@"Today";
        }
        else if([difference day]==1){
            cell.lblDaysAgo.text=@"Yesterday";
        }
        else{
        cell.lblDaysAgo.text=[NSString stringWithFormat:@"%ld Days Ago",(long)[difference day]];
        }
    }
        return cell;
    }
    return 0;
}

-(NSDate*)ConvertStringToDate :(NSString*)StringDate{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd-MM-yyyy'T'HH:mm:ss.SSSZ"];
    
    return [dateFormatter dateFromString:StringDate];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row==0 && indexPath.section==0){
        NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
        if(![Username isEqualToString:@"Guest"]){
            [self performSegueWithIdentifier:@"profile" sender:self];
        }
        else{
            [self showsUIAlertWithMessageNEW:@"Sorry. Please login to access this feature." andTitle:@"Alert-Restricted Access"];
            //            [self performSelector:@selector(MoveToEvents) withObject:nil afterDelay:1];
        }
    }
    eventID=(long)indexPath.row;
    NSLog(@"eventId ==>> %ld",eventID);
    [[NSUserDefaults standardUserDefaults] setInteger:eventID forKey:@"EventChange"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section==0){
        return 250;
    }
    if (indexPath.section==1){
        return 100;
    }

    return UITableViewAutomaticDimension;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section==0){
       UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 0)];
        return view;
    }
    
    if(section==1){
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
    /* Create custom view to display section header... */
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 20)];
    [label setFont:[UIFont boldSystemFontOfSize:17]];
        label.textColor=[UIColor whiteColor];
//         [label setTextAlignment:NSTextAlignmentCenter];
//        label.textAlignment = NSTextAlignmentCenter;
        NSString *string =@"Events";
    /* Section header is in 0th index... */
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:45/255.0 green:107/255.0 blue:139/255.0 alpha:1.0]]; //your background color...
    return view;
    }
    
    if(section==2){
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
        /* Create custom view to display section header... */
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, tableView.frame.size.width, 20)];
        [label setFont:[UIFont boldSystemFontOfSize:17]];
        label.textColor=[UIColor whiteColor];
//         label.textAlignment = NSTextAlignmentCenter;
        NSString *string =@"Notifications";
        /* Section header is in 0th index... */
        [label setText:string];
        [view addSubview:label];
       [view setBackgroundColor:[UIColor colorWithRed:45/255.0 green:107/255.0 blue:139/255.0 alpha:1.0]];//your background color...
    return view;
    }
    
    return 0;
}

- (IBAction)btnLogOut:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"EventName"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"EventID"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"EventChange"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"LoginToken"];
    [self performSegueWithIdentifier:@"LogOut" sender:self];
}
- (IBAction)btnLogOutMain:(id)sender {
    [self showsUIAlertWithMessage:@"Are you sure you want to logout?" andTitle:@"Alert"];
    
    
}


-(void)setUserInfo{
    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    
     NSString *DidGoogleSignIn =[[NSUserDefaults standardUserDefaults] stringForKey:@"DidGoogleSignIn"];
    
    //check if google sign in was used or normal sign in
    //then it will be accurate
    NSString *UserID =[[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"];
    
    if([UserID length]==0 || ![DidGoogleSignIn isEqualToString:@"YES"]){
        
        Users *obj =[[Users alloc]init];
        obj.FirstName=@"User";
        obj.LastName=@"";
        obj.Picture=@"guesticon.png";
        currentUserInfo=obj;
        
    }
    else{
                 NSString *UserID =[[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"];
                NSString *query = [NSString stringWithFormat:@"select * from tblCurrentUser where active=='1' and id=='%@'",UserID];
        
                NSArray *dbresults =[self.dbManager loadDataFromDB:query];
                NSLog(@"db elements %@",dbresults);
        
        
                if(dbresults.count>0){
                    Users *obj =[[Users alloc]init];
                    int i=0;
                    obj.FirstName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"first_name"]];
                    obj.LastName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"last_name"]];
                    obj.Active=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"active"]];
                    obj.LastModified=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
                    obj.CompanyName=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"company_name"]];
                    obj.Designation=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"designation"]];
                    obj.Picture=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"picture"]];
                    obj.Email=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"email"]];
                    obj.Phone=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"phone"]];
                    obj.Gender=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"gender"]];
                    obj.Qualificaton=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"qualification"]];
                    obj.UserID=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"id"]];
        
                    currentUserInfo=obj;
                }
    }
    
    
    if([DidGoogleSignIn isEqualToString:@"YES"]){

        Users *obj = [[Users alloc]init];
        obj.FirstName=[[[[GIDSignIn sharedInstance]currentUser] profile] name];
        obj.LastName=@"";//[[[[GIDSignIn sharedInstance]currentUser] profile] givenName];
        obj.Picture=[NSString stringWithFormat:@"%@",[[[[GIDSignIn sharedInstance]currentUser] profile] imageURLWithDimension:128]];
        currentUserInfo=obj;
//        }
        }
    
}
- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK",nil];
    [alert show];
    
}
- (void)showsUIAlertWithMessageNEW:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:nil];
    [alert show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        NSLog(@"Cancel button clicked");
    }
    if (buttonIndex == 1){
        [[GIDSignIn sharedInstance]signOut];
        [[NSUserDefaults standardUserDefaults] setValue:@"" forKey:@"EventName"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"EventID"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"EventChange"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"LoginToken"];
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"TabsLoadedSucessfully"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [self performSegueWithIdentifier:@"LogOut" sender:self];
          [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"DidGoogleSignIn"];
    }
}




@end
