//
//  EventDetailsViewController.m
//  FIAEventsApp
//
//  Created by temp on 19/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "EventDetailsViewController.h"
#import "EventJsonDownload.h"
#import "Dummy.h"
#import "Constants.h"
#import "Haneke.h"
#import "EventDetailsDescTableViewCell.h"
#import "EventDetailsMapsTableViewCell.h"
#import "EventDetailsNameTableViewCell.h"
#import "EventDetailsDateTimeVenueTableViewCell.h"
#import "EventDetailsImageTableViewCell.h"
#import "EventRatingsTableViewCell.h"
#import "HCSStarRatingView.h"
#import "DBManager.h"
@import Firebase;
@import UIKit;

@interface EventDetailsViewController (){
    NSString *eventDetails;
    NSArray *arrEventInfo;
    Dummy *objectEvent;
    
    NSString *eventID;
    NSString *subsCheck;
    NSString *userID;
    NSString *eventComments;
    long eventRatingsNum;
    
    float FullWidth;
    float CutWidth;
    float keyboardSize;
    
    NSString * testTopic;
}
@property (strong, nonatomic) IBOutlet UITableView *tblEventDetails;
@property (nonatomic, strong) DBManager *dbManager;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicatorTopicSet;


@end
@implementation EventDetailsViewController

- (void)viewDidLoad{
    [super viewDidLoad];
    eventID=[NSString stringWithFormat:@"%@",objectEvent.eventID];
     self.tblEventDetails.keyboardDismissMode= UIScrollViewKeyboardDismissModeOnDrag;
    eventComments=@"Comment";
    keyboardSize=170;
    FullWidth=self.view.frame.size.height;
    CutWidth=FullWidth-keyboardSize;
//    eventID
    [self someMethodWhereYouSetUpYourObserver];
    userID =[[NSUserDefaults standardUserDefaults] stringForKey:@"UserID"];
    Constants *obj=[[Constants alloc]init];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:obj.dbName];
    
    [self getEventDataForDisplay];
    
}

- (void)someMethodWhereYouSetUpYourObserver
{
    // This could be in an init method.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(myNotificationMethod:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
}

- (void)myNotificationMethod:(NSNotification*)notification
{
    NSDictionary* keyboardInfo = [notification userInfo];
    NSValue* keyboardFrameBegin = [keyboardInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrameBeginRect = [keyboardFrameBegin CGRectValue];
//    keyboardSize=keyboardFrameBeginRect.size.height;
    CutWidth=FullWidth-keyboardSize;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void) setObject:(long) obj{
    _CompeventID=obj;
    NSLog(@"value came through!!! Yay===%lu",obj);
}

-(void)setEventDetailObject:(Dummy *) obj{
    objectEvent=obj;
    
    [_tblEventDetails reloadData];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(indexPath.row==0){
        EventDetailsImageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventImage" forIndexPath:indexPath];
         cell.tag=indexPath.row;
       
        Constants *cObj=[[Constants alloc]init];
        NSString *urlpath=[NSString stringWithFormat:@"http://%@/%@",cObj.endpoint,objectEvent.eventImageURL];
        NSURL *url = [NSURL URLWithString:urlpath];
        if(cell.tag==indexPath.row){
        [cell.ImgEventImage hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"LoadingImage.jpg"]];
        }
        return cell;
        
    }
    
    if(indexPath.row==1){
        EventDetailsNameTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventName" forIndexPath:indexPath];
         cell.tag=indexPath.row;
        if(cell.tag==indexPath.row){
            cell.lblEventName.text=[NSString stringWithFormat:@"%@",objectEvent.eventName];
        }
         return cell;
    }
    if(indexPath.row==2){
        EventDetailsDescTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventDesc" forIndexPath:indexPath];
         cell.tag=indexPath.row;
        if(cell.tag==indexPath.row){
        cell.lblEventDescription.text=[NSString stringWithFormat:@"%@",objectEvent.eventDesc];
            }
         return cell;
    }
    
    if(indexPath.row==3){
        EventDetailsDateTimeVenueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventDateTimeVenue" forIndexPath:indexPath];
         cell.tag=indexPath.row;
        if(cell.tag==indexPath.row){
        cell.lblContent.text=[NSString stringWithFormat:@"%@",objectEvent.startDate];
            cell.lblContentName.text = @"Date";
        }
         return cell;
    }
    
//    if(indexPath.row==4){
//        EventDetailsDateTimeVenueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventDateTimeVenue" forIndexPath:indexPath];
//
//        cell.lblContentName.text=@"Time :";
//        cell.lblContent.text=[NSString stringWithFormat:@"%@",objectEvent.startDate];
//         return cell;
//    }
    
    if(indexPath.row==4){
        EventDetailsDateTimeVenueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventDateTimeVenue" forIndexPath:indexPath];
         cell.tag=indexPath.row;
        if(cell.tag==indexPath.row){
        cell.lblContentName.text=@"Venue :";
        cell.lblContent.text=[NSString stringWithFormat:@"%@",objectEvent.venueID];
        }
         return cell;
    }
    
    if(indexPath.row==5){
        EventDetailsMapsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventMaps" forIndexPath:indexPath];
         cell.tag=indexPath.row;
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        f.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *LatNumber = [f numberFromString:objectEvent.latitude];

        NSNumberFormatter *f1 = [[NSNumberFormatter alloc] init];
        f1.numberStyle = NSNumberFormatterDecimalStyle;
        NSNumber *LongNumber = [f numberFromString:objectEvent.longtitude];


        
      
        CLLocation *location = [[CLLocation alloc] initWithLatitude:[LatNumber doubleValue] longitude:[LongNumber doubleValue]];
        MKPointAnnotation *mapPin = [[MKPointAnnotation alloc] init];
        
        mapPin.coordinate = location.coordinate;
                    mapPin.title = [NSString stringWithFormat:@"%@",objectEvent.venueID];
//                    houseCoord.subtitle = @"You are here";
        [cell.mapsEventLocation addAnnotation:mapPin];
        MKCoordinateRegion adjustedRegion = [cell.mapsEventLocation regionThatFits:MKCoordinateRegionMakeWithDistance(mapPin.coordinate, 250, 250)];
        
        [cell.mapsEventLocation setRegion:adjustedRegion animated:YES];
         return cell;
        
    }
    
    if(indexPath.row==6){
        EventRatingsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EventRatings" forIndexPath:indexPath];
//        cell.lblTitle.text=[NSString stringWithFormat:@"Session Rating-%@",objectEvent.eventName];
        eventID=[NSString stringWithFormat:@"%@", objectEvent.eventID];
        cell.tag=indexPath.row;
        NSString *temp=cell.txtComments.text;
        cell.txtComments.text=eventComments;
        HCSStarRatingView *starRatingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(0,0,cell.ratingsView.frame.size.width,cell.ratingsView.frame.size.height)];
        starRatingView.maximumValue = 5;
        starRatingView.minimumValue = 0;
        starRatingView.value = eventRatingsNum;
        starRatingView.allowsHalfStars = NO;
        starRatingView.tintColor = [UIColor colorWithRed:0.141 green:0.455 blue:0.663 alpha:1.00];       [starRatingView addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
        [cell.ratingsView addSubview:starRatingView];
        
        [cell.btnSubmit addTarget:self action:@selector(btnEventsClick:) forControlEvents:UIControlEventTouchUpInside];
        
        return cell;
        
    }
    
    if(indexPath.row==7){
        EventDetailsDateTimeVenueTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Subs" forIndexPath:indexPath];
        cell.tag=indexPath.row;
        [cell.btnSubsToEvent addTarget:self action:@selector(btnSubclicked) forControlEvents:UIControlEventTouchUpInside];
         NSString *Subskey =[[NSUserDefaults standardUserDefaults] stringForKey:[NSString stringWithFormat:@"Event%@",eventID]];

        if([Subskey isEqualToString:@"YES"] ){
            if(cell.tag==indexPath.row && cell.btnSubsToEvent.tag==69){
               
                //save indexPath.row inside this tag
                
//                cell.btnSubsToEvent.titleLabel.text=[[NSString alloc]init];
                cell.btnSubsToEvent.titleLabel.textColor=[UIColor whiteColor];
//                cell.btnSubsToEvent.titleLabel.text=[NSString stringWithFormat:@"Disable Notification"];
                 [cell.btnSubsToEvent setTitle:@"Disable Notification" forState:UIControlStateNormal];
                

            }
        }
        
        if(![Subskey isEqualToString:@"YES"] ){
            if(cell.tag==indexPath.row&& cell.btnSubsToEvent.tag==69){
                 cell.btnSubsToEvent.titleLabel.textColor=[UIColor whiteColor];
                [cell.btnSubsToEvent setTitle:@"Enable Notification" forState:UIControlStateNormal ];
            }
        }
        return cell;
    }
    
    return 0;
}

- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    NSLog(@"Session Changed rating to %.1f", sender.value);
    eventRatingsNum=sender.value;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==5){
        return 350;
    }
    if(indexPath.row==2){
        return UITableViewAutomaticDimension;
    }
    if(indexPath.row==6){
        return 240;
    }
    return UITableViewAutomaticDimension;
}

-(void)btnSubs{
    // add table in database to cater for subs as it will be enabled and disabled.
    
    NSString *Subskey =[[NSUserDefaults standardUserDefaults] stringForKey:[NSString stringWithFormat:@"Event%@",eventID]];
    
    NSString *topic=testTopic;
    if(![Subskey isEqualToString:@"YES"]){
   [[NSUserDefaults standardUserDefaults] setValue:@"YES" forKey:[NSString stringWithFormat:@"Event%@",eventID]];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[FIRMessaging messaging] subscribeToTopic:topic
                                    completion:^(NSError * _Nullable error) {
                                        NSLog(@"Subscribed to test topic");
                                    }];
        subsCheck=@"YES";
        [_tblEventDetails reloadData];
        NSIndexPath *index=[NSIndexPath indexPathForRow:7 inSection:0];
        [_tblEventDetails scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:[NSString stringWithFormat:@"Event%@",eventID]];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[FIRMessaging messaging] unsubscribeFromTopic:topic
                                        completion:^(NSError * _Nullable error) {
                                            NSLog(@"unSubscribed to test topic");
                                        }];
        subsCheck=@"NO";
        [_tblEventDetails reloadData];
        NSIndexPath *index=[NSIndexPath indexPathForRow:7 inSection:0];
        [_tblEventDetails scrollToRowAtIndexPath:index atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    [_actIndicatorTopicSet stopAnimating];
    _actIndicatorTopicSet.hidden=YES;
}

-(void)btnSubclicked{
    _actIndicatorTopicSet.hidden=NO;
    [_actIndicatorTopicSet startAnimating];
    NSDictionary *headers = @{ @"Authorization": @"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd",
                               @"Cache-Control": @"no-cache",
                               @"Postman-Token": @"c34fae8c-37d3-4413-a845-339961f62f9d" };
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://www.ashivanram.tk/apiTest/getNotificationTopics.php"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    [request setAllHTTPHeaderFields:headers];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        
                                                    NSDictionary *arrJSON = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                                                        
                                                        NSDictionary *topicJSON = [arrJSON objectForKey:@"PushNotify"];
                                                        for (NSDictionary *obj in topicJSON){
                                                        if([[obj objectForKey:@"EventID"] isEqualToString:[NSString stringWithFormat:@"%@",objectEvent.eventID]]){
                                                            testTopic=[obj objectForKey:@"PushTopic"];
                                                          //.............
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                 [self btnSubs];
                                                            });
                                                            //................
                                                           
                                                        }
                                                            
                                                        }
                                                        
                                                        
                                                        
                                                    }
                                                }];
    [dataTask resume];
}
-(void)btnEventsClick:(id)sender{
    //send all info about speaker
    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    if(![Username isEqualToString:@"Guest"]){
        long eventRatingNo =[[NSUserDefaults standardUserDefaults] integerForKey:@"EventRating"];
        NSIndexPath *nowIndex = [NSIndexPath indexPathForRow:6 inSection:0];
        EventRatingsTableViewCell*cell = [self.tblEventDetails cellForRowAtIndexPath:nowIndex];
        //[self LoginForUser:username andpassword:password];
        Constants *cObj=[[Constants alloc]init];
        NSLog(@"ip adddress is :%@",cObj.endpoint);
        
        NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/rest/rate",cObj.endpoint]]];
        
        NSString *temp=cell.txtComments.text;
        eventComments=temp;
        NSString *userUpdate =[NSString stringWithFormat:@"type=%@&userid=%@&eventid=%@&rate=%@&message=%@", @"event",userID,objectEvent.eventID,[NSString stringWithFormat:@"%ld", eventRatingNo],eventComments];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        //create the Method "GET" or "POST"
        [urlRequest setHTTPMethod:@"POST"];
        
        [urlRequest setValue:@"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd" forHTTPHeaderField:@"Authorization"];
        //Convert the String to Data
        NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
        
        //Apply the data to the body
        [urlRequest setHTTPBody:data1];
        
        NSURLSession *session = [NSURLSession sharedSession];
        NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
            NSLog(@"Response =%@",response);
            if(httpResponse.statusCode == 200)
            {
                NSError *parseError = nil;
                NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
                NSLog(@"The response is - %@",responseDictionary);
                NSDictionary *success = [responseDictionary objectForKey:@"data"];
                NSLog(@"The success is - %@",success);
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self insertIntoSQLite:userID andComments:eventComments andRatings:[NSString stringWithFormat:@"%ld", eventRatingsNum] andSessionID:@"" andEventID:[NSString stringWithFormat:@"%@",objectEvent.eventID] andtype:2];
                    NSLog(@"query executed");
                    
                });
            }
            else
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    NSLog(@"Error response is %@", response);
                    
                });
                
            }
        }];
        [dataTask resume];
        
        [self showsUIAlertWithMessage:@"Thank You for Your Submission" andTitle:@"Submitted-Event"];
        
    }
    else{
        [self showsUIAlertWithMessage:@"Sorry.Please Login to access this feature" andTitle:@"Information"];
    }
}

- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,FullWidth)];
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
    
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView{
    //    textView.text=@"";
  
    textView.textColor=[UIColor blackColor];
//    FullW;
     [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,FullWidth-49)];
    
    
    //     [self hideKeyboard];
    return YES;
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    [self.view.layer removeAllAnimations];
    //    textView.text=@"";
    textView.textColor=[UIColor blackColor];
//    keyboardSize=keyboardSize-20;
//    FullWidth =self.view.frame.size.height;
    [self.view setFrame:CGRectMake(0,-keyboardSize,self.view.frame.size.width,FullWidth)];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:6 inSection:0];
    [_tblEventDetails scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    
    return YES;
}

-(void)insertIntoSQLite:(NSString*)userid andComments:(NSString*)comments andRatings:(NSString*)ratings andSessionID:(NSString*)sessionid andEventID:(NSString*)eventid andtype:(int)typeNum{
    //  need a type colmn for type session/speaker/event in sqlite
    NSString *query;
    if([self getEventData]==true){
        query = [NSString stringWithFormat:@"UPDATE tblReview SET comments='%@' ,rating='%@' Where eventid='%@' and rate_type='%d'",comments,ratings,eventID,typeNum];
        
        //        UPDATE "main"."tblReview" SET "comments" = ?1, "rating" = ?2 WHERE  "id" = 2
    }
    else{
        query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblReview (user_id,comments,rating,sessionid,eventid,rate_type) Values ('%@','%@','%@','%@','%@','%d')",userid,comments,ratings,sessionid,eventid,typeNum];
    }
    //    NSString *clearquery2 = [NSString stringWithFormat:@"delete from tblReview"];
    
    [self.dbManager executeQuery:query];
    
    
    //    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
    }
    else{
        NSLog(@"Could not execute the query.");
    }
}

-(BOOL)getEventData{
    NSString *query = [NSString stringWithFormat:@"select * from tblReview where rate_type=2 and eventid=%@",eventID];
    
    //    //    // Execute the query.
    
    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
    if(dbresults.count>0){
        NSLog(@"%@",dbresults);
        //                for(int i=0;i<dbresults.count;i++){
        //                    sessionComments=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"comments"]];
        //                    NSString *ratings =[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"rating"]];
        //
        //                    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        //                    f.numberStyle = NSNumberFormatterDecimalStyle;
        //                    NSNumber *myNumber = [f numberFromString:ratings];
        //
        //                    sessionRatingsNum=[myNumber intValue];
        //
        //
        //
        //                }
        return true;
    }
    return false;
}

-(void)getEventDataForDisplay{
    NSString *query = [NSString stringWithFormat:@"select * from tblReview where rate_type=2 and eventid=%@",eventID];
    
    //    //    // Execute the query.
    
    NSArray *dbresults =[self.dbManager loadDataFromDB:query];
    if(dbresults.count>0){
        NSLog(@"%@",dbresults);
        for(int i=0;i<dbresults.count;i++){
            eventComments=[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"comments"]];
            NSString *ratings =[[dbresults objectAtIndex:i] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"rating"]];
            
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            f.numberStyle = NSNumberFormatterDecimalStyle;
            NSNumber *myNumber = [f numberFromString:ratings];
            
            eventRatingsNum=[myNumber intValue];
            
            
            
        }
    }
    
}

@end

