//
//  MyProfileViewController.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 23/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Users.h"

@interface MyProfileViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
-(void)setUserObject:(Users *)uObj;
@end
