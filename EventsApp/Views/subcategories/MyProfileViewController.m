//
//  MyProfileViewController.m
//  FIAEventsApp
//
//  Created by Ashivan Ram on 23/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "MyProfileViewController.h"
#import "SWRevealViewController.h"
#import "ProPicTableViewCell.h"
#import "BioProfileTableViewCell.h"
#import "ContactsTableViewCell.h"
#import "UserInfoDownload.h"
#import "Users.h"
#import "Constants.h"
#import "Haneke.h"
#import "DBManager.h"
#import "Reachability.h"

@interface MyProfileViewController (){
     UIView *overlayview;
    Users *userProfileObj;
    UIRefreshControl *refreshControl;
    
}
@property (weak, nonatomic) IBOutlet UITableView *tblProfile;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnBack;
@property (weak, nonatomic) IBOutlet UIView *overlayView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;
@property (nonatomic, strong) DBManager *dbManager;

@end

@implementation MyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    refreshControl = [[UIRefreshControl alloc]init];
    [self.tblProfile addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];

    Constants *obj=[[Constants alloc]init];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:obj.dbName];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
 */

-(void)refreshTable{
    
     [refreshControl endRefreshing];
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        [self showsUIAlertWithMessageWithButtons:@"Content cannot be Updated. No Internet Connection Avaliable" andTitle:@"Information"];
    }
    else
    {
        Reachability* reachability = [Reachability reachabilityWithHostName: @"www.google.com"];
        NetworkStatus netStatus = [reachability currentReachabilityStatus];
        NSLog(@"the nertwok is %ld", (long)netStatus);
        if((long)netStatus>0){
            [self UpdateUserData];
        }
        else{
            [self showsUIAlertWithMessageWithButtons :@"Oh!No.Your internet connection is not stable. Please Check Internet connection and Try Again." andTitle:@"Alert"];
        }
    }
   
}
- (void)showsUIAlertWithMessageWithButtons:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings",nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        NSLog(@"Cancel button clicked");
    }
    if (buttonIndex == 1){
        NSLog(@"settings button clicked");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-prefs:"]];
    }
}

- (IBAction)btnBack:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"its done");
    }];
//    [super viewWillAppear:animated=NO];
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    Constants *cObj=[[Constants alloc]init];
    if(indexPath.row==0){
        ProPicTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Pic" forIndexPath:indexPath];
        NSString *urlpath=[NSString stringWithFormat:@"http://%@/%@",cObj.endpoint,userProfileObj.Picture];
        NSURL *url = [NSURL URLWithString:urlpath];
        //            NSData *data = [NSData dataWithContentsOfURL:url];
        
        //                UIImage *img = [[UIImage alloc] initWithData:data];
        [cell.imgProPic hnk_setImageFromURL:url placeholder:[UIImage imageNamed:@"LoadingImage.jpg"]];
        cell.lblUserName.text=[[NSString stringWithFormat:@"%@ %@",userProfileObj.FirstName,userProfileObj.LastName] uppercaseString];
        cell.lblUserCompany.text=[NSString stringWithFormat:@"%@",userProfileObj.CompanyName];
        return cell;
        
    }
    
//    if(indexPath.row==1){
//        BioProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Bio" forIndexPath:indexPath];
//        cell.tag=indexPath.row;
//        return cell;
//    }
    if(indexPath.row==1){
        ContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Contact" forIndexPath:indexPath];
        cell.lblContactType.text=@"Phone";
        cell.lblContactContent.text=[NSString stringWithFormat:@"%@",userProfileObj.Phone];
        return cell;
    }
    
    if(indexPath.row==2){
        ContactsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Contact" forIndexPath:indexPath];
        cell.lblContactType.text=@"Email";
        cell.lblContactContent.text=[NSString stringWithFormat:@"%@",userProfileObj.Email];
        return cell;
    }
    
    
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row==0){
        return 200;
    }
   
    return 100;
}

-(void)setUserObject:(Users *)uObj{
    
    userProfileObj=uObj;
    [_tblProfile reloadData];
    
   
}
- (IBAction)btnUpdateProfile:(id)sender {
    
//    [self performSegueWithIdentifier:@"updatePro" sender:self];
}

-(void)UpdateUserData{

   
    NSString *username=[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    NSString *password=[[NSUserDefaults standardUserDefaults] stringForKey:@"Password"];
    
    //[self LoginForUser:username andpassword:password];
    Constants *cObj=[[Constants alloc]init];
    NSLog(@"ip adddress is :%@",cObj.endpoint);
    
    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/rest/checkuser",cObj.endpoint]]];
    
    NSString *userUpdate =[NSString stringWithFormat:@"username=%@&password=%@", username,password];
    [[NSUserDefaults standardUserDefaults] synchronize];
    //create the Method "GET" or "POST"
    [urlRequest setHTTPMethod:@"POST"];
    
    [urlRequest setValue:@"Bearer l4xL7hAoQD25SIz67d5jIZWfSJwIk2N1FRd" forHTTPHeaderField:@"Authorization"];
    //Convert the String to Data
    NSData *data1 = [userUpdate dataUsingEncoding:NSUTF8StringEncoding];
    
    //Apply the data to the body
    [urlRequest setHTTPBody:data1];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        NSLog(@"Response =%@",response);
        if(httpResponse.statusCode == 200)
        {
            NSError *parseError = nil;
            NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&parseError];
            NSLog(@"The response is - %@",responseDictionary);
            NSDictionary *success = [responseDictionary objectForKey:@"data"];
            NSString *isSuccess = [responseDictionary objectForKey:@"status"];
            NSLog(@"The success is - %@",success);
            if([isSuccess isEqualToString:@"true"])
            {
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                
                    for(NSDictionary *successObj in success){
                        Users *obj=[[Users alloc]init];
                        
                        obj.UserID=[successObj objectForKey:@"id"];
                        obj.FirstName=[successObj objectForKey:@"first_name"];
                        obj.LastName=[successObj objectForKey:@"last_name"];
                        obj.Email=[successObj objectForKey:@"email"];
                        obj.Phone=[successObj objectForKey:@"phone"];
                        obj.Picture=[successObj objectForKey:@"picture"];
                        obj.Gender=[successObj objectForKey:@"gender"];
                        obj.CompanyName=[successObj objectForKey:@"company_name"];
                        obj.Designation=[successObj objectForKey:@"designation"];
                        obj.Qualificaton=[successObj objectForKey:@"qualification"];
                        obj.Active=[successObj objectForKey:@"active"];
                        obj.LastModified=[successObj objectForKey:@"lastmodified"];
                        
                        [self getUserData:obj];
                        [self setUserObject:obj];
                    
                        
                    }
                    
                });
            }
        }
    }];
    [dataTask resume];
}

-(void)getUserData:(Users*)userData{
        NSString *clearquery = [NSString stringWithFormat:@"delete from tblCurrentUser"];
        
        [self.dbManager executeQuery:clearquery];
        
    
        if (self.dbManager.affectedRows != 0) {
            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
        }
        else{
            NSLog(@"Could not execute the query.");
        }
        NSString *query = [NSString stringWithFormat:@"INSERT or REPLACE INTO tblCurrentUser (first_name,last_name,email,phone,picture,gender,company_name,designation,qualification,active,lastmodified,id) Values (\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\",\"%@\")",userData.FirstName,userData.LastName,userData.Email,userData.Phone,userData.Picture,userData.Gender,userData.CompanyName,userData.Designation,userData.Qualificaton,userData.Active,userData.LastModified,userData.UserID];
        
        
        [self.dbManager executeQuery:query];
        
        
        //    // If the query was successfully executed then pop the view controller.
        if (self.dbManager.affectedRows != 0) {
            NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
        }
        else{
            NSLog(@"Could not execute the query.");
        }
        
        NSString *newquery = [NSString stringWithFormat:@"select * from tblCurrentUser where active=='1' order by lastmodified"];
        
        //    //    // Execute the query.
        
        NSArray *dbresults =[self.dbManager loadDataFromDB:newquery];
        if(dbresults.count>0){
            //    NSLog(@"db elements %@",dbresults);
            
            
            //            checkLastModifiedUser=[[dbresults objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"lastmodified"]];
            //            [[NSUserDefaults standardUserDefaults] setValue:checkLastModifiedUser forKey:@"LastModified-Userv1"];
            //            [[NSUserDefaults standardUserDefaults] synchronize];
        }
}


@end
