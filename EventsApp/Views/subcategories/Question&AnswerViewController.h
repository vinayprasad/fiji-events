//
//  Question&AnswerViewController.h
//  FIAEventsApp
//
//  Created by temp on 13/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Question_AnswerViewController : UIViewController<UIWebViewDelegate,UIAlertViewDelegate>
@property (strong, nonatomic) IBOutlet UIWebView *QuestionAnswerWebView;
-(void)SetSessionID:(NSString*)sessionID;

@end
