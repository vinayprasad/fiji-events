//
//  ReviewViewController.h
//  FIAEventsApp
//
//  Created by Ashivan Ram on 23/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Session Times.h"
#import "Events.h"

@interface ReviewViewController : UIViewController<UITabBarDelegate,UITableViewDataSource,UITextFieldDelegate,UIScrollViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *tblRatings;
-(void)setSessionDetails:(Session_Times *)sessionObj andCheck:(BOOL)isitEndOfEvent andEventID:(Events*)eventObj;
@end
