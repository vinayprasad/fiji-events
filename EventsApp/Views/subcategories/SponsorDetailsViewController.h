//
//  SponsorDetailsViewController.h
//  FIAEventsApp
//
//  Created by temp on 13/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sponsors.h"
@interface SponsorDetailsViewController : UIViewController<UIWebViewDelegate,UIAlertViewDelegate>
@property (strong,nonatomic) Sponsors *selectedSponsor;
@property (strong, nonatomic) IBOutlet UIWebView *SponsorWebView;
-(void)setSponsorWebUrl:(Sponsors*)obj;
@end
