//
//  EventDetailsViewController.h
//  FIAEventsApp
//
//  Created by temp on 19/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Dummy.h"

@interface EventDetailsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>

@property  long CompeventID;
-(void) setObject:(long) obj;
-(void)setEventDetailObject:(Dummy *) obj;
@end
