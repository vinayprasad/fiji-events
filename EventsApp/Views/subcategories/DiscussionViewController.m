//
//  DiscussionViewController.m
//  FIAEventsApp
//
//  Created by temp on 13/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "DiscussionViewController.h"
#import "ChatTableViewCell.h"
#import "YourChatTableViewCell.h"

@interface DiscussionViewController (){
    NSArray *ChatImages;
    NSArray *ChatName;
    NSArray *ChatMsg;
}
@property (strong, nonatomic) IBOutlet UILabel *lblBanner;

@end

@implementation DiscussionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    ChatImages=@[@"P1.png",@"P1.png",@"P1.png",@"P2.png",@"P1.png"];
    
    [_BtnSend.layer setCornerRadius:17.0f];
    
    // border
    [_BtnSend.layer setBorderColor:[UIColor blackColor].CGColor];
    [_BtnSend.layer setBorderWidth:0.9f];
    
 
    
    //----------------------------------------------
    [_lblBanner.layer setCornerRadius:17.0f];
    
    // border
    [_lblBanner.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_lblBanner.layer setBorderWidth:0.9f];
    _lblBanner.clipsToBounds=YES;
    
    
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   
    return ChatImages.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    if(indexPath.row==3){
        YourChatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"YourChatBox" forIndexPath:indexPath];
        NSString *imgname = [ChatImages objectAtIndex:indexPath.row];
        cell.ChatProPic.image = [UIImage imageNamed:imgname];
        cell.lblName.text=@"Amber McGreggor";
        cell.lblMessage.text=@"Yes! i am also in favour of the idea.";
        return cell;
    }else{
   ChatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatBox" forIndexPath:indexPath];
    NSString *imgname = [ChatImages objectAtIndex:indexPath.row];
    cell.ChatProImage.image = [UIImage imageNamed:imgname];
    cell.lblName.text=@"Walter Jenkins";
    cell.lblMessage.text=@"Energy is a universally used but limited in content as only much can be used or it might incurr demerit impacts on the surronding environment.";
        return cell;
    }
    
    return 0;
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [[event allTouches] anyObject];
    if([_txtComment isFirstResponder] && [touch view] != _txtComment){
        [_txtComment resignFirstResponder];
        
    }
    [super touchesBegan:touches withEvent:event];
}

@end
