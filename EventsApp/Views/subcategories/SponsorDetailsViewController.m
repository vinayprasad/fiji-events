//
//  SponsorDetailsViewController.m
//  FIAEventsApp
//
//  Created by temp on 13/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "SponsorDetailsViewController.h"
#import "Sponsors.h"
#import "Reachability.h"
@interface SponsorDetailsViewController (){
    NSString *SURL;
    NSString *SName;
    
}
@property (strong, nonatomic) IBOutlet UILabel *lblBanner;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *actIndLoadingImage;

    

@end

@implementation SponsorDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.SponsorWebView.delegate=self;
    NSURL *url = [NSURL URLWithString:SURL];
    NSURLRequest *request =  [NSURLRequest requestWithURL:url];
    [_SponsorWebView loadRequest:request];
    
    _lblBanner.text=SName;
//    [_lblBanner.layer setCornerRadius:17.0f];
//
//    // border
//    [_lblBanner.layer setBorderColor:[UIColor whiteColor].CGColor];
//    [_lblBanner.layer setBorderWidth:0.9f];
//    _lblBanner.clipsToBounds=YES;
    
    //[[self navigationController] setNavigationBarHidden:NO animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)setSponsorWebUrl:(Sponsors*)obj{
    SURL=obj.SponsorWebURL;
    SName=obj.SponsorCompanyName;
    
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
  
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        [self showsUIAlertWithMessageWithButtons:@"You need to be connected to the internet to view this content" andTitle:@"Alert"];
    }
    else
    {
          [self.actIndLoadingImage startAnimating];
    }

}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.actIndLoadingImage stopAnimating];
    self.actIndLoadingImage.hidesWhenStopped=YES;
}
- (void)showsUIAlertWithMessageWithButtons:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings",nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        NSLog(@"Cancel button clicked");
    }
    if (buttonIndex == 1){
        NSLog(@"settings button clicked");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-prefs:"]];
    }
}
@end
