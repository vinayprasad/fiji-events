//
//  PollingViewController.m
//  FIAEventsApp
//
//  Created by Bhawick on 5/4/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import "PollingViewController.h"
#import "Constants.h"

@interface PollingViewController (){
    NSString *objSessionID;
    NSString *objUSerID;
}
@property (strong, nonatomic) IBOutlet UILabel *lblBanner;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;
@end

@implementation PollingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    Constants *obj = [[Constants alloc ]init];
    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    if(![Username isEqualToString:@"Guest"]){
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/web/poll/question?session=%@&userid=%@",obj.endpoint,objSessionID, objUSerID]];
        NSURLRequest *request =  [NSURLRequest requestWithURL:url];
        [_PollingWebView loadRequest:request];
    }
    else{
        [self showsUIAlertWithMessage:@"Sorry. Please Login to access this feature" andTitle:@"Information"];
        [_actIndicator stopAnimating];
        _actIndicator.hidesWhenStopped=YES;
    }
    self.PollingWebView.delegate=self;
    //----------------------------------------------
    [_lblBanner.layer setCornerRadius:17.0f];
    
    // border
    [_lblBanner.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_lblBanner.layer setBorderWidth:0.9f];
    _lblBanner.clipsToBounds=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma -mark WebView Methods
-(void)webViewDidStartLoad:(UIWebView *)webView{
    [self.actIndicator startAnimating];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.actIndicator stopAnimating];
    self.actIndicator.hidesWhenStopped=YES;
}

- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}

-(void)SetSessionID:(NSString *)sessionID{
    objSessionID=sessionID;
}
-(void)SetUserID:(NSString *)userID{
    objUSerID=userID;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
