//
//  PollingViewController.h
//  FIAEventsApp
//
//  Created by Bhawick on 5/4/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PollingViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIWebView *PollingWebView;
-(void)SetSessionID:(NSString*)sessionID;
-(void)SetUserID:(NSString*)sessionID;

@end
