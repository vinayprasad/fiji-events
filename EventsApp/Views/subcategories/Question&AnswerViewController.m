//
//  Question&AnswerViewController.m
//  FIAEventsApp
//
//  Created by temp on 13/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#//
//  Question&AnswerViewController.m
//  FIAEventsApp
//
//  Created by temp on 13/02/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import "Question&AnswerViewController.h"
#import "Constants.h"
#import "Reachability.h"

@interface Question_AnswerViewController (){
    NSString *objSessionID;
}
@property (strong, nonatomic) IBOutlet UILabel *lblBanner;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *actIndicator;


@end

@implementation Question_AnswerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    Constants *obj = [[Constants alloc ]init];
    NSString *Username =[[NSUserDefaults standardUserDefaults] stringForKey:@"Username"];
    if(![Username isEqualToString:@"Guest"]){
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/FIA_PORTAL/api/v1/session/panels?sessionid=%@",obj.endpoint,objSessionID]];
        NSURLRequest *request =  [NSURLRequest requestWithURL:url];
        [_QuestionAnswerWebView loadRequest:request];
    }
    else{
        [self showsUIAlertWithMessage:@"Sorry. Please Login to access this feature" andTitle:@"Information"];
        [_actIndicator stopAnimating];
        _actIndicator.hidesWhenStopped=YES;
    }
    self.QuestionAnswerWebView.delegate=self;
    //----------------------------------------------
//    [_lblBanner.layer setCornerRadius:17.0f];
//
//    // border
//    [_lblBanner.layer setBorderColor:[UIColor whiteColor].CGColor];
//    [_lblBanner.layer setBorderWidth:0.9f];
//    _lblBanner.clipsToBounds=YES;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webViewDidStartLoad:(UIWebView *)webView{
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        [self showsUIAlertWithMessageWithButtons:@"You need to be connected to the internet to view this content" andTitle:@"Alert"];
    }
    else
    {
         [self.actIndicator startAnimating];
    }
  
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.actIndicator stopAnimating];
    self.actIndicator.hidesWhenStopped=YES;
}

- (void)showsUIAlertWithMessage:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
    
}
- (void)showsUIAlertWithMessageWithButtons:(NSString *) message andTitle:(NSString *)title{
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:title message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Settings",nil];
    [alert show];
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 0){
        NSLog(@"Cancel button clicked");
    }
    if (buttonIndex == 1){
        NSLog(@"settings button clicked");
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"App-prefs:"]];
    }
}

-(void)SetSessionID:(NSString *)sessionID{
    objSessionID=sessionID;
}
/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

