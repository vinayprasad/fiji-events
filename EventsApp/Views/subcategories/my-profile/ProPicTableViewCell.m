//
//  ProPicTableViewCell.m
//  FIAEventsApp
//
//  Created by Ashivan on 4/11/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import "ProPicTableViewCell.h"

@implementation ProPicTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
        [_imgProPic.layer setCornerRadius:_imgProPic.frame.size.height/2];
    
        // border
        [_imgProPic.layer setBorderColor:[UIColor whiteColor].CGColor];
        [_imgProPic.layer setBorderWidth:8.5f];
        _imgProPic.clipsToBounds=YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
