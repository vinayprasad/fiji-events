//
//  ProPicTableViewCell.h
//  FIAEventsApp
//
//  Created by Ashivan on 4/11/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProPicTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgProPic;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblUserCompany;

@end
