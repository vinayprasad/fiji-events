//
//  BioProfileTableViewCell.h
//  FIAEventsApp
//
//  Created by Ashivan on 4/11/18.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BioProfileTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;

@end
