//
//  MasterViewController.h
//  FIAEventsApp
//
//  Created by temp on 31/01/2018.
//  Copyright © 2018 temp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"

@class DetailViewController;

@interface MasterViewController : UITableViewController<UIGestureRecognizerDelegate,SWRevealViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *lblWelcomeNav;

@property (weak, nonatomic) IBOutlet UIImageView *imgProPic;
@property (strong, nonatomic) DetailViewController *detailViewController;
@property (strong, nonatomic) IBOutlet UITableView *tblMenu;


@end

